# sum-pred

Carlos M. Carrillo, 2019 
The Hydroinformatics and Integrated Research Group 
Biological Systems Engineering Department
University of Nebraska-Lincoln

Files are originally located at:

~/2019/sum-pred/ 


NOTE:
(1) https://developer.atlassian.com/blog/2016/04/different-ssh-keys-multiple-bitbucket-accounts/ 

ACKNOWLEDMENTS
This research was funded by the U.S. Geological Survey (USGS), the U.S. Department of Agriculture (USDA), the Daugherty Water for Food Global Institute (DWFI) at the University of Nebraska-Lincoln (UNL), and the UNL’s Layman Award. 
We thank the Earth System Research Laboratory at NOAA for providing the gridded data. 
Also, the Climate Forecast System (CFS) retrospective reanalysis and reforecast dataset was obtained from the National Centers for Environmental Prediction (NCEP).  
We are grateful to the UNL Holland Computer Center for access to their high-computing facilities to perform the analysis, which code is at http://bit.ly/GP-LLJ. 

