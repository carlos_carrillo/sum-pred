*---------------* 
* gradsnc -p 
*---------------* 
'reinit'
*---------------* 
pathref="/home/carlosc/2010/coff-var/gs/frac.usmx.gs" 
*---------------* 
'q gxinfo'
   say result
   xline=sublin(result,3);xmax=subwrd(xline,6);xmin=subwrd(xline,4)
   xline=sublin(result,4);ymax=subwrd(xline,6);ymin=subwrd(xline,4)
* 
   dx=(xmax-xmin)/2;dy=(ymax-ymin)/2
*
*  xl=xmin+0.8;xr=xl+dx*1.5;
*  yt=ymax-0.55;yb=yt-dy*0.962;
   xl=xmin+0.8;xr=xl+dx*1.75;
   yt=ymax-0.55;yb=yt-dy*0.962;
*  
*   case=1,2,3
   case=1
   case=2
*  case=3
*  case=4
*  case=5
*
   if(case=1) 
     epsfile = 'eof1.gph.eps'
     pngfile = 'eof1.gph.png'
     ncfile = './result/eof1.hgt.200.1993.nc'
*    ncfile2 = './result/ttest.pc3.gph.nc'
     titlel1 = ' `0EOF1(`3D`0HGT 200 mb:28%) JJA (daily) '; 
     titlel2 = '1993'
   endif 
   if(case=2) 
     epsfile = 'eof2.gph.gfs.eps'
     pngfile = 'eof2.gph.gfs.png'
     ncfile = './result/eof2.hgt.reco.1993.glb.nc'
     titlel1 = ' `0EOF2(`3D`0HGT 200 mb: 6%) MJJA (daily) '; 
     titlel2 = '1993'
   endif 
   if(case=3) 
     epsfile = 'eof3.gph.eps'
     pngfile = 'eof3.gph.png'
     ncfile = './result/eof3.hgt.200.1993.nc'
     titlel1 = ' `0EOF3(`3D`0HGT 200 mb: 9%) JJA (daily) '; 
     titlel2 = '1993'
   endif 
   if(case=4) 
     epsfile = 'eof4.gph.eps'
     pngfile = 'eof4.gph.png'
     ncfile = './result/eof4.hgt.200.1993.nc'
     titlel1 = ' `0EOF4(`3D`0HGT 200 mb: 8%) JJA (daily) '; 
     titlel2 = '1993'
   endif 
   if(case=5) 
     epsfile = 'eof5.gph.eps'
     pngfile = 'eof5.gph.png'
     ncfile = './result/eof5.hgt.200.1993.nc'
     titlel1 = ' `0EOF5(`3D`0HGT 200 mb: 7%) JJA (daily) '; 
     titlel2 = '1993'
     
   endif 



*  
   imo=imo1;xlab=on;smo=smo1

   xtitle = ' `0(1895~2008)'; 
*   xtitle = ' `0PRMladj vs. JJ LDEO SPI,1896~2005'; 
*  titlel1 = ' `0RCMladj vs. JA LDEO SPI'; 
*  titlel2 = ' `01895-2008'; 
   cbar='on'; 
   initime = 1jan1895;endtime = 1jan2008; 
   initime = 1;endtime = 111; 
   call = mkplot(ncfile,ncfile2,initime,endtime,xl,xr,yb,yt,imo,xlab,smo,titlel1,titlel2,cbar,case) 

* RCM  31.995  -109.33
if(1=0) 
   xlat =  31.995;
   xlon = -109.33;
  'q ll2xy 'xlon'  'xlat''
   x=subwrd(result,1)
   y=subwrd(result,2)
  
  'set line 1 1 4'
  'draw mark 9 'x' 'y' 0.11'

  'set strsiz 0.11 0.11'
  'set string 1 l'
  'draw string 1.20 5.89 RCM'
  'set line 1 1 4' 
  'draw mark 9 1.10 5.88 0.15'
endif 
    
   cbar='off'; 
*------------------------* 
'set strsiz 0.11 0.11'
'set string 1 l'
**'draw string 0.5 0.75 'pathref''
*------------------------* 
if (1=1) 
'enable print x.gm'
'print'
'disable print'
*'!gxps -c -i x.gm -o x.ps'
'!gxeps -c -i x.gm -o x.eps'
'!convert -density 200 x.eps 'pngfile''
'!mv x.eps 'epsfile''
*'!mv x.eps x.eps'
endif 
*-------------------------* 

*----------------------------* 
function mkplot(ncfile,ncfile2,initime,endtime,xl,xr,yb,yt,imo,xlab,smo,titlel1,titlel2,cbar,case) 


'set parea 'xl' 'xr' 'yb' 'yt''
*
*---------------* 
'sdfopen  'ncfile''
**'sdfopen  'ncfile2''
say ncfile 

'set grads off'
'set frame off'
'set mpdraw off'
'set xlab off'
'set ylab off'
*---------------* 

*-'set lat 15 70'
*-'set lon 160 340'
*'set lon 220 300'

*'define xvar = smth9(smth9(smth9(data)))'
* --- insert here 

*initime = 1jan1895;endtime = 1jan2008; 
*initime = 1jan1950;endtime = 1jan2008; 

itime = initime; 
etime = endtime; 

*say itime' 'etime 

'set gxout shaded' 

*'define xvar = data'
if(case=1);mapsign=1;endif

*if(case=2);mapsign=1;endif
if(case=2);mapsign=-1;endif

if(case=3);mapsign=1;endif
if(case=4);mapsign=1;endif
if(case=5);mapsign=1;endif
*'define xvar = -1*data'
*'define xvar = 'mapsign'*data'
*'define xvar = 'mapsign'*smth9(data)'

*-'define xvar = 'mapsign'*( smth9(data) - ave(data,lon=0,lon=360) )'

 'define xvar = 'mapsign'*( smth9(data) - 0.0*ave(data,lon=0,lon=360) )'
*'define xvar = 'mapsign'*( (data) - 0.0*ave(data,lon=0,lon=360) )'



'set gxout shaded'
'set grid off'


*'yred2blue.gs'
*'set clevs -0.2 -0.15 -0.1 -0.05 0.2 0.3 0.4 0.5' 
*'set ccols  21 23 26 29 0 31 33 35 37'

*'jaecol'
*'set clevs -0.2 -0.15 -0.1 -0.05 0.2 0.3 0.4 0.5' 
*'set ccols  28 26 24 22 0 34 36 38 39'

*'yred2blue.gs'
'blue_green_red.gs'

*'set clevs  -0.4 -0.3 -0.2 -0.1 0.1 0.2 0.3 0.4 ' 
*'set clevs  -25 -20 -15 -10 10 15 20 25' 
*'set clevs  -80 -40 -20 -10 10 20 40 80' 
'set clevs   -80 -60 -40 -20 -10 10 20 40 60 80' 
'set ccols  49 47 45 43 41 0 31 33 35 37 39'


*if(case=3);
* 'set clevs -0.3 -0.2 -0.1 -0.05 0.2 0.3 0.4 0.5' 
* 'set ccols  28 26 24 22 0 34 36 38 39'
*endif

*-'set mpdset mres' 
'd xvar'
*-'cbarv 'xr+0.142' 'yt-3.10' 0'
*'cbarh 'xr-1.300' 'yt-1.80' 0'
'cbarh.big.local1 'xr-1.582' 'yt-1.80' 0 '0.160' '0.140''
*'set clevs -1.65 0 1.65'
*'set ccols  4 0 0 2'

*'set gxout contour'
*'set clab off'; 'set cthick 2';'set cstyle 1'; 
*'set clevs -1.65  1.65' 

**'d 'mapsign'*data.2'
**'hatch.gs 'mapsign'*data.2 -min 1.65 -max 50 -angle 45 -density 0.01 -int 0.05'
**'hatch.gs 'mapsign'*data.2 -min -50 -max -1.65 -angle 45 -density 0.01 -int 0.05'



** contour lines 
'set gxout contour'
*'set clevs  -0.3 -0.2 -0.15 -0.1 0.1 0.15 0.2 0.3' 
*'set clevs  -20 -15 -10 -5 5 10 15 20' 
*-'set clevs  -25 -20 -15 -10 10 15 20 25' 
'set clevs   -80 -60 -40 -20 -10'
'set clab off';'set cthick 1';'set cstyle 1';'set ccolor 1';
'd xvar'
'set clevs    10 20 40 60 80' 
'set clab off';'set cthick 1';'set cstyle 1';'set ccolor 1';
'd xvar'
** 
**'basemap L 0 1 L' 
**

'set mpdraw on'
'jaecol.gs'
ccol = 39;*dark green

*'set map 1 1 3'
'set map 'ccol' 1 6'
'set mpdset lowres'
*'set mpdset mres'
'draw map'

*'set map 'ccol' 1 2'
*'set mpdset hires'
*'draw map'



'myframe 1 1 2'
*'set strsiz 0.17 0.17'
*'set string 1 l'
if(1=1) 
  'set strsiz 0.12 0.13'
  'set string 1 l'
*  'draw string 'xl+0.00' 'yt-2.05' 'xtitle' '
  if(cbar=on) 
  'yred2blue.gs'
*    'set strsiz 0.106 0.13'
    'set strsiz 0.11 0.15'
*    'draw string 'xl+0.28' 'yt+0.12' FRAC[JAS/ANNUAL](LDEO)'
    'draw string 'xl+0.00' 'yt-1.68' 'titlel1''

*-  'set line  0'      
*-  'draw recf 'xl+0.02' 'yb+1.53' 'xl+1.50' 'yb+1.75''    
*-  'set line  1'      

    'set string 1 l 5'
    'draw string 'xl+0.02' 'yb+1.50' 'titlel2''

*   'draw string 'xl+0.00' 'yt-1.42' 'titlel2''
*   'cbarh 'xr-1.12' 'yt-1.50' 0'
*   'cbarv 'xr+0.14' 'yt-3.15' 0'
  endif  
endif 

*'set strsiz 0.17 0.17';'set string 1 l'
*'draw string 'xr-0.59' 'yb+0.5' 'smo''


*xl2=xmin;xr2=xmax;yt2=ymax;yb2=ymin
* x-y label
 'q gxinfo';*the first parea changes when the plot is defined
   xline=sublin(result,3);xr2=subwrd(xline,6);xl2=subwrd(xline,4)
   xline=sublin(result,4);yt2=subwrd(xline,6);yb2=subwrd(xline,4)

*xlab=on
ylab=on
*'set lon -118 -101'
if(xlab=on);xvlf= 0;xvrg=360;xint=40.0;
            call=myxlabh(xl2,xr2,yt2,yb2,xvlf,xvrg,xint);endif
if(ylab=on);yvb= 20;yvt=60.0;yint=20.0;
            call=myylabh(xl2,xr2,yt2,yb2,yvb,yvt,yint);endif


*'close 2' 
*'close 1' 
return 

*---------------------------
function myxlabh(xl,xr,yt,yb,xvlf,xvrg,xint)
* xvlf=-120;xvrg=-20;xint=20 
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,2);x1=subwrd(xline,6);xn=subwrd(xline,8)
*
  nptos=(xvrg-xvlf)/xint + 1;
*
  it=1;while(it<=nptos)
          xx.it = xvlf + (it-1)*xint 
*          say xx.it 
       it=it+1
       endwhile
* lab 
  it=1;while(it<=nptos)
        xreal=xx.it
        say xreal 
        xabs=math_abs(xx.it)
        if (xreal<0);
           if (xreal>-180);
               xxlb.it=xabs%"`aO`nW";
           else;  xtmp = 360-xabs
               xxlb.it=xtmp%"`aO`nE";
           endif
        else
           if (xreal> 180);
               xtmp = 360-xabs
               xxlb.it=xtmp%"`aO`nW";
               if (xtmp = 0);
                  xxlb.it=xtmp%"`aO`n";
               endif 
           else;  
              xxlb.it=xabs%"`aO`nE";
           endif
            
        endif
        if(xreal=0|xreal=180);xxlb.it=xabs%"`aO`n";endif
*        xxlb.it=math_abs(xx.it) 
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say xxlb.it 
        it=it+1
       endwhile
*

*
  ip=1;while(ip<=nptos)
         xx = xx.ip
         xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         'set line 1 1 1'
         'draw line 'xs' 'yb' 'xs' 'yb-0.075''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
         y = yt
*        'draw line 'xs' 'yb' 'xs' 'yt''
         'set strsiz 0.09 0.11'
         'set string 1 c 3'
         'draw string 'xs'  'yb-0.175' 'xxlb.ip''
      ip=ip+1
      endwhile
return
*------------------------
function myylabh(xl,xr,yt,yb,yvb,yvt,yint)
* yvb=-40;yvt=40;yint=10;
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,3);y1=subwrd(xline,6);yn=subwrd(xline,8)
  nptos=(yvt-yvb)/yint + 1;
*
  it=1;while(it<=nptos)
          yy.it = yvb  + (it-1)*yint 
*          say yy.it 
       it=it+1
       endwhile
* lab 

  it=1;while(it<=nptos)
        yreal=yy.it
        yabs=math_abs(yy.it)
        if(yreal=0);yylb.it="EQ";endif
        if(yreal<0);yylb.it=yabs%"`aO`nS";endif
        if(yreal>0);yylb.it=yabs%"`aO`nN";endif
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say yylb.it 
        it=it+1
       endwhile
*
  ip=1;while(ip<=nptos)
         yy = yy.ip
*        xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         ys = yb+(yy-y1)*(yt-yb)/(yn-y1)
         'set line 1 1 1'
         'draw line 'xl-0.075' 'ys' 'xl' 'ys''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
*         'draw line 'xs' 'yb' 'xs' 'y''
*        'draw line 'xr' 'ys' 'xl' 'ys''
*        'set strsiz 0.12'
         'set strsiz 0.09 0.11'
         'set string 1 r 3'
*         'draw string 'xs'  'yb-0.2' 'xxlb.ip''
         'draw string 'xl-0.10'  'ys' 'yylb.ip''
      ip=ip+1
      endwhile
return
*----------------------------------------------

