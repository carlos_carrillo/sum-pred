*===================================================
*function cbar(x,y,k)
function cbar(args)
x=subwrd(args,1) 
y=subwrd(args,2) 
k=subwrd(args,3) 
dx=subwrd(args,4) 
dy=subwrd(args,5) 
*--------
 'query shades'
  shdinfo = result
  if (subwrd(shdinfo,1)='None')
    say 'Cannot plot color bar: No shading information'
    return
  endif
*
  cnum = subwrd(shdinfo,5)
*
*  Plot colorbar
*
*  x=1
*  y=0
*  k=2 
*
*  dx=0.24
*  dy=0.13

    x=x
   x0=x
    y=y
   y0=y
 
* 'set string 1 l 2 0' ; 'set strsiz 0.06 0.07'
*'set string 1 l 2 0' ; 'set strsiz 0.12 0.12'
 'set string 1 l 2 0' ; 'set strsiz 0.10 0.10'
  num = 0
  while (num<cnum)
 
    rec = sublin(shdinfo,num+2)
    col = subwrd(rec,1)
    hi  = subwrd(rec,3)
     'set line 'col' '1' '2''  
     'draw recf 'x-dx' 'y' 'x' 'y+dy
*    'set line 1  1  5'
*    'set line 1  1  6'
     'set line 1  1  2'
     'draw rec  'x-dx' 'y' 'x' 'y+dy
    if(num<cnum-1)
      'set strsiz  0.06' 
      'set strsiz  0.11 0.12' 
*     'set string 1 c  3 60' 
*     'set string 1 l 12 60' 
      'set string 1 l 3 60' 
*      'draw string 'x-dx*0' 'y+1.55*dy' 'hi
      'draw string 'x-dx*0' 'y+1.30*dy' 'hi
      'set string 1 c 3 0' 
    endif
    num = num + 1
*    y=y+dy
*   x=x+dy
    x=x+dx
  endwhile
  y=y0
  x=x0
 
  if(k=1)
           "draw string "x-0.12" "y+0.36" ms"
           'set strsiz 0.03 0.04'
           "draw string "x+0.00" "y+0.41" -1" 
  endif
  if(k=2); "draw string "x+0.25" "y+0.55" ww`30`1m"
           'set strsiz 0.03'
           "draw string "x+0.15" "y+0.62" -2" ;endif
  if(k=3); "draw string "x+0.25" "y+0.55" m`30`1s"
           'set strsiz 0.03'
           "draw string "x+0.25" "y+0.62" -1" ;endif
