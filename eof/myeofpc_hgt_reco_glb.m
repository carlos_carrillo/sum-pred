%--------% 
clear all
close all
%--------% 
% SST

% filesst = '/FoudreD0/data/sst/noaa/sst.1854-2010.jj.4-8.10-15.25-50.nc';
% filesst = '/FoudreD0/data/sst/noaa/sst.1854-2010.nov-apr.4-8.10-15.25-50.nc';
  filesst = '/home/carrillo/2015/Mazon/prog/hgt_narr.dys.nc';
  filesst = '/home/carrillo/2015/sum-pred/eof/hgt.200.wet.93.dy.nc';

  filesst = '/work/hydro/carrillo/2015/sum-pred/eof/hgt.200.wet.93.dy.nc';
  filesst = '/data2/cmc542/scratch/2019/DATA/CFSR/hgt200/hgt.200.mjja.93.dy.nc';
  filesst_clim = '/home/carrillo/2015/Mazon/prog/hgt_narr.jjas.79-14.nc';
%--------% 
% yrini = 1993; yrend = 2010;
  yrini = 1993; yrend = 1993;
% filespi = '/FoudreD0/data/ldeo/spi.pnoaa.1895-2010.nov-apr.mat'; 
% pref_file = 'sst.jj.1895-2010';
  pref_file = 'hgt.reco.2000.1993';
  pref_file = 'hgt.reco.1993.glb';
%----------------------------% 
%---- OUT FILES -------------% 
%----------------------------% 
   path_store ='result/';

   expl_eofr  = strcat(path_store,'expl_roteof.',pref_file,'.txt');
   expl_eof     = strcat(path_store,'expl_eof.',pref_file,'.txt');

   eof1file = strcat(path_store,'eof1.',pref_file,'.nc');
   eof2file = strcat(path_store,'eof2.',pref_file,'.nc');
   eof3file = strcat(path_store,'eof3.',pref_file,'.nc');
   eof4file = strcat(path_store,'eof4.',pref_file,'.nc');
   eof5file = strcat(path_store,'eof5.',pref_file,'.nc');

   pc1file = strcat(path_store,'pc1.',pref_file,'.txt');
   pc2file = strcat(path_store,'pc2.',pref_file,'.txt');
   pc3file = strcat(path_store,'pc3.',pref_file,'.txt');
   pc4file = strcat(path_store,'pc4.',pref_file,'.txt');
   pc5file = strcat(path_store,'pc5.',pref_file,'.txt');

   eofr1file = strcat(path_store,'eofr1.',pref_file,'.nc');
   eofr2file = strcat(path_store,'eofr2.',pref_file,'.nc');
   eofr3file = strcat(path_store,'eofr3.',pref_file,'.nc');
   eofr4file = strcat(path_store,'eofr4.',pref_file,'.nc');
   eofr5file = strcat(path_store,'eofr5.',pref_file,'.nc');

   pcr1file = strcat(path_store,'pcr1.',pref_file,'.txt');
   pcr2file = strcat(path_store,'pcr2.',pref_file,'.txt');
   pcr3file = strcat(path_store,'pcr3.',pref_file,'.txt');
   pcr4file = strcat(path_store,'pcr4.',pref_file,'.txt');
   pcr5file = strcat(path_store,'pcr5.',pref_file,'.txt');

%----------------------------------------------------% 
%----------------------------------------------------% 
% HGT clim 
% ncid = netcdf.open(filesst_clim,'NC_NOWRITE');
% rdataclim = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'hgt') );
% lat = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lat') );
% lon = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lon') );
% netcdf.close(ncid);
%----------------------------------------------------% 
%  HGT  

%  load(filespi);

  ncid = netcdf.open(filesst,'NC_NOWRITE');
% rdata = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'sst') );
  rdata = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'hgt200') );
  lat = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lat') );
  lon = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lon') );
  time = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'time') );
  time_unit = netcdf.getAtt(ncid, netcdf.inqVarID(ncid,'time'),'units' );

  data_fill = netcdf.getAtt(ncid,3,'_FillValue');
  netcdf.close(ncid);
%
% rdataclim (find(rdataclim == data_fill)) = NaN;
% 
  rdata (find(rdata == data_fill)) = NaN;
  [nrow ncol ntime]=size(rdata); 
   for it=1:ntime
    xdata(:,:,it)=rdata(:,:,it)'; 
   end 
% ydata=rdataclim'; 
  figure;contourf(lon,lat,xdata(:,:,1)); 
% figure;contourf(lon,lat,ydata); 
  clear rdata;rdata=xdata;
% clear rdataclim;rdataclim=ydata;
%
%
%  yrs_data = [1854:2010]; 

%  idx_ini = find (yrs_data == yrini);
%  idx_end = find (yrs_data == yrend);

%  clear xdata
%  xdata = rdata(:,:,idx_ini:idx_end); clear rdata ; rdata = xdata;
%  clear xtmp
%  xtmp = yrs_data(idx_ini:idx_end); clear yrs_data ; yrs_data = xtmp;

%  rdatamean = mean(rdata,3);
% [nrow ncol ntime] = size(rdata);
%  for it=1:ntime
%    rdata(:,:,it)  = rdata(:,:,it) - rdatamean;
%  end

  [nrow ncol ntime] = size(rdata);

%
%------------------
%
  contourf(lon,lat,rdata(:,:,1))


  gph = rdata; 

% gphmean = mean(gph,3);
%[nrow ncol ntime] = size(gph); 
% for it=1:ntime  
%    gph(:,:,it)  = gph(:,:,it) - gphmean; 
% end 
% figure(1); contourf(gph(:,:,1)); 
% 
% gph_raw = gph; 
% for irow=1:nrow 
%    gph(:,icol,:)  = gph(:,icol,:) * sqrt(cos(lat(icol)*pi/180.)); 
%    gph(irow,:,:)  = gph(irow,:,:) * sqrt(cos(lat(irow)*pi/180.)); 
% end 
% figure(2);contourf(gph(:,:,1)); 
% 
% reduce domain 
% 
if(1==1)
%  mnlat=-20; mxlat=65;
%  mnlat=-60; mxlat=65;
   mnlat= 10; mxlat=60;
   mnlat=  2; mxlat=70;
   mnlat= 10; mxlat=70;
%  mnlat=-40; mxlat=65;
   mnlon=150; mxlon=354;  % -140 -> -60 
%  mnlon=20; mxlon=350;  % -130 -> -60 
   mnlon=0; mxlon=359;  % -140 -> -60 

   idwlat = floor( (mnlat - lat(1))/(lat(2)-lat(1)) + 1 );
   iuplat = ceil ( (mxlat - lat(1))/(lat(2)-lat(1)) + 1 );

   ilflon = floor( (mnlon - lon(1))/(lon(2)-lon(1)) + 1 );
   irglon = ceil ( (mxlon - lon(1))/(lon(2)-lon(1)) + 1 );

  [nrow ncol ntime ] = size(gph);

   for it = 1:ntime
      nwgph(:,:,it) = gph(idwlat:iuplat,ilflon:irglon,it);
   end
%     nwrdataclim = rdataclim(idwlat:iuplat,ilflon:irglon);
   nwlon = lon(ilflon:irglon);
   nwlat = lat(idwlat:iuplat);

  figure;contourf(lon,lat,gph(:,:,1)) 
  figure;contourf(nwlon,nwlat,nwgph(:,:,1)) 
  clear gph lon lat; 
% clear rdataclim; 
% rdataclim = nwrdataclim; 
%--reduce resolution 
  [nx,ny,nt] = size(nwgph); 
   clear nwlon2 nwlat2 nwgph2  

   nwlon2 = nwlon(1:4:length(nwlon)); 
   nwlat2 = nwlat(1:4:length(nwlat)); 
   nwgph2 = nwgph(1:4:nx,1:4:ny,:) ; 
% 
%  gph = nwgph; 
%  lon = nwlon; 
%  lat = nwlat; 
%--
   gph = nwgph2; 
   lon = nwlon2; 
   lat = nwlat2; 
end

  clear gphmean;
  gphmean = mean(gph,3);

%  DETREND DATA important! 
%  we do not detrend this time 
% [nrow ncol ntime] = size(gph);
%  for irow=1:nrow 
%     for icol=1:ncol 
%         drdata(irow,icol,:) = detrend(gph(irow,icol,:)); 
%     end 
%  end 
%  gph=drdata;  
% 
%  REMOVE mean 
% 
%  gphmean = rdataclim;
  [nrow ncol ntime] = size(gph); 
   for it=1:ntime  
     gph(:,:,it)  = gph(:,:,it) - gphmean; 
   end 
%  figure(1); contourf(gph(:,:,1)); 

%  DETREND DATA important! 
%  we do not detrend this time 
% [nrow ncol ntime] = size(gph);
%  for irow=1:nrow 
%     for icol=1:ncol 
%         drdata(irow,icol,:) = detrend(gph(irow,icol,:)); 
%     end 
%  end 
%  gph=drdata;  
%  

% 
% 
SPI2D = gph; 
% 
%----------------------------------------------------% 
%----------------------------------------------------% 
PPmatI = SPI2D; 
[ny,nx,nt] = size(PPmatI);
for i=1:nx
    for j=1:ny 
        nnan = length( find ( isnan(PPmatI(j,i,:)) == 1) ); 
        if (nnan == 0) 
           xpp(j,i,:)=PPmatI(j,i,:); 
 	else 
           xpp(j,i,1:nt)=NaN; 
        end
    end 
end 
nmiss = length( find ( isnan(xpp(:,:,1)) == 1) ); 
nreal = nx*ny-nmiss;
%% 
%% Rarray
%% construct the reduced array
%% 
for it=1:nt
    xtmp = xpp(:,:,it);
%    it 
    idxpp = find(~isnan(xtmp)); % to reconstruct 2D map 
    Rarray(it,:) = xtmp(find(~isnan(xtmp)))';
end
%----------------------------------------------------% 
%%%%%%%%%%%%
% SVD
%%%%%%%%%%%%
disp('computing SVD ...') 


[U,S,EOFs] = svd(Rarray);

% 
% RECO without scaling 
% 
  RarrayF = U*S(:,1:5)*EOFs(:,1:5)';
  
  Rarray1 = U*S(:,1)*EOFs(:,1)';
  Rarray2 = U*S(:,2)*EOFs(:,2)';
  Rarray3 = U*S(:,3)*EOFs(:,3)';
  Rarray4 = U*S(:,4)*EOFs(:,4)';
  Rarray35 = U*S(:,3:5)*EOFs(:,3:5)';
  Rarray12 = U*S(:,1:2)*EOFs(:,1:2)';

  xPCmod1  = U*S(:,1);
  xPCmod2  = U*S(:,2); 
  xPCmod3  = U*S(:,3);
  xPCmod4  = U*S(:,4);
  xPCmod5  = U*S(:,5);
  
%RarrayF_3D = nan(size(gph)); 
%Rarray1_3D = nan(size(gph)); 
%Rarray2_3D = nan(size(gph)); 
%Rarray3_3D = nan(size(gph)); 
 ieof = nan(ny,nx);% initial value 
for it = 1:nt
    ieof(idxpp) = RarrayF(it,:); RarrayF_3D(:,:,it) = ieof;
    ieof(idxpp) = Rarray1(it,:); Rarray1_3D(:,:,it) = ieof;
    ieof(idxpp) = Rarray2(it,:); Rarray2_3D(:,:,it) = ieof;
    ieof(idxpp) = Rarray3(it,:); Rarray3_3D(:,:,it) = ieof;
    ieof(idxpp) = Rarray4(it,:); Rarray4_3D(:,:,it) = ieof;
    ieof(idxpp) = Rarray35(it,:); Rarray35_3D(:,:,it) = ieof;
    ieof(idxpp) = Rarray12(it,:); Rarray12_3D(:,:,it) = ieof;
end
save('mean-gph.mat','gphmean','lat','lon');
save('reco-gph.Full.mat','RarrayF_3D','lat','lon');
save('reco-gph.mod1.mat','Rarray1_3D','lat','lon');
save('reco-gph.mod2.mat','Rarray2_3D','lat','lon');
save('reco-gph.mod3.mat','Rarray3_3D','lat','lon');
save('reco-gph.mod4.mat','Rarray4_3D','lat','lon');
save('reco-gph.mod35.mat','Rarray35_3D','lat','lon');
save('reco-gph.mod12.mat','Rarray12_3D','lat','lon');

 [ntime nmodes]=size(S(:,1));yrs_data=[1:ntime]; fid=fopen('pc.mod1.txt','w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), xPCmod1(itime,1));fprintf(fid,'\n');
  end
  fclose(fid);

 [ntime nmodes]=size(S(:,1));yrs_data=[1:ntime]; fid=fopen('pc.mod2.txt','w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), xPCmod2(itime,1));fprintf(fid,'\n');
  end
  fclose(fid);

 [ntime nmodes]=size(S(:,1));yrs_data=[1:ntime]; fid=fopen('pc.mod3.txt','w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), xPCmod3(itime,1));fprintf(fid,'\n');
  end
  fclose(fid);

 [ntime nmodes]=size(S(:,1));yrs_data=[1:ntime]; fid=fopen('pc.mod4.txt','w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), xPCmod4(itime,1));fprintf(fid,'\n');
  end
  fclose(fid);

 [ntime nmodes]=size(S(:,1));yrs_data=[1:ntime]; fid=fopen('pc.mod5.txt','w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), xPCmod5(itime,1));fprintf(fid,'\n');
  end
  fclose(fid);

%----EOF rotation-------
% 
nmod = 5;  % number of modes retained 
%-nmod = 10; 

% meth-1  adding rotation 
 EOFr = rotatefactors(EOFs(:,1:nmod)); % Varimax Rotation: columns 1-5
 PCr = Rarray*EOFr;
%EOFr = EOFs(:,1:nmod); % Varimax Rotation: columns 1-5
%PCr = Rarray*EOFr;

% meth-2  non-rotated
 PCr2 = U(:,1:nmod);
 EOFr2 = (PCr2'*Rarray)';

%PCr2 = rotatefactors(U(:,1:nmod));
%EOFr2 = (PCr2'*Rarray)';

% Normalize 
  for imod = 1:nmod
      EOFr(:,imod) = EOFr(:,imod) .*  std(PCr(:,imod));
      PCr(:,imod) = PCr(:,imod) ./ std(PCr(:,imod));

      EOFr2(:,imod) = EOFr2(:,imod) .* std(PCr2(:,imod));
      PCr2(:,imod) = PCr2(:,imod) ./ std(PCr2(:,imod));
  end
% 
%-----------------% 
%  EOF variance 
%-----------------% 
% meth 2 not rotated  
 [nrow ncol] = size(S);
  N = min([nrow ncol]); 
  for im=1:N
      lamd(im) = S(im,im);
  end
  expvar = (lamd.^2) ./ sum(lamd.^2);
% for meth 1 rotated 
  Rarrayr = PCr* EOFr';
%[CC,LL] = eig(cov(Rarrayr));
% L = diag(LL)/trace(LL);
% L = flipud(L);
% Lcomp = sum(L(1:nmod))/sum(L)
% or 
  [Ux,Sx,EOFx] = svd(Rarrayr);
  [nrow ncol] = size(Sx);
  N = min([nrow ncol]); 
  for im=1:N
      lamd(im) = Sx(im,im);
  end
  rotexpvar = (lamd.^2) ./ sum(lamd.^2);
%
%-----------------% 
% EOFr3D (map) ---% 
%-----------------% 
%
year = yrini:yrend;
ieof = nan(ny,nx);% initial value 
for imod = 1:nmod
    ieof(idxpp) = EOFr(:,imod);
    EOFr3D(:,:,imod) = ieof;

    ieof(idxpp) = EOFr2(:,imod);
    EOFr3D2(:,:,imod) = ieof;
end 

 figure;contourf(EOFr3D2(:,:,1)) 
 figure;contourf(EOFr3D2(:,:,2)) 
 figure;contourf(EOFr3D2(:,:,3)) 
 figure;contourf(EOFr3D2(:,:,4)) 
 figure;contourf(EOFr3D2(:,:,5)) 
%figure;contourf(EOFr3D2(:,:,1)+gphmean) 

 wrnc2d(eofr1file,lat,lon,EOFr3D(:,:,1)');
 wrnc2d(eofr2file,lat,lon,-1*EOFr3D(:,:,2)');
 wrnc2d(eofr3file,lat,lon,-1*EOFr3D(:,:,3)');
 wrnc2d(eofr4file,lat,lon,EOFr3D(:,:,4)');
 wrnc2d(eofr5file,lat,lon,EOFr3D(:,:,5)');

%wrnc2d(eofr1file,lat,lon,EOFr3D2(:,:,1)' + gphmean');
%wrnc2d(eofr2file,lat,lon,EOFr3D2(:,:,2)' + gphmean');
%wrnc2d(eofr3file,lat,lon,EOFr3D2(:,:,3)' + gphmean');
%wrnc2d(eofr4file,lat,lon,EOFr3D2(:,:,4)' + gphmean');
%wrnc2d(eofr5file,lat,lon,EOFr3D2(:,:,5)' + gphmean');


 wrnc2d(eof1file,lat,lon,EOFr3D2(:,:,1)');
 wrnc2d(eof2file,lat,lon,-1*EOFr3D2(:,:,2)');
 wrnc2d(eof3file,lat,lon,-1*EOFr3D2(:,:,3)');
 wrnc2d(eof4file,lat,lon,EOFr3D2(:,:,4)');
 wrnc2d(eof5file,lat,lon,EOFr3D2(:,:,5)');


  fid = fopen(expl_eofr,'w');
  for itime=1:5
      fprintf(fid,'%9.4f ', rotexpvar(itime));fprintf(fid,'\n');
  end
  fclose(fid);

  fid = fopen(expl_eof,'w');
  for itime=1:5
      fprintf(fid,'%9.4f ', expvar(itime));fprintf(fid,'\n');
  end
  fclose(fid);

%------------------------
 [ntime nmodes] = size(PCr);
  yrs_data = [1:ntime]
  fid = fopen(pc1file,'w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), PCr(itime,1));fprintf(fid,'\n');
  end
  fclose(fid);

  fid = fopen(pc2file,'w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), -1*PCr(itime,2));fprintf(fid,'\n');
  end
  fclose(fid);

  fid = fopen(pc3file,'w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), -1*PCr(itime,3));fprintf(fid,'\n');
  end
  fclose(fid);

  fid = fopen(pc4file,'w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), PCr(itime,4));fprintf(fid,'\n');
  end
  fclose(fid);

  fid = fopen(pc5file,'w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), PCr(itime,5));fprintf(fid,'\n');
  end
  fclose(fid);
%------------------------
%------------------------
 [ntime nmodes] = size(PCr2);
  yrs_data = [1:ntime]
  fid = fopen(pcr1file,'w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), PCr2(itime,1));fprintf(fid,'\n');
  end
  fclose(fid);

  fid = fopen(pcr2file,'w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), -1*PCr2(itime,2));fprintf(fid,'\n');
  end
  fclose(fid);

  fid = fopen(pcr3file,'w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), -1*PCr2(itime,3));fprintf(fid,'\n');
  end
  fclose(fid);

  fid = fopen(pcr4file,'w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), PCr2(itime,4));fprintf(fid,'\n');
  end
  fclose(fid);

  fid = fopen(pcr5file,'w');
  for itime=1:ntime
      fprintf(fid,'%4i %9.4f ',yrs_data(itime), PCr2(itime,5));fprintf(fid,'\n');
  end
  fclose(fid);
%------------------------

%------------------% 
% tunit = 'years  ';
% ntime = length(PCr2);
% t = [0:ntime-1];
%time_ini = 'days since 1971-01-01 00:00:00';
%time_ini = 'years since 1981-01-01 00:00:00';
%xvar=PCr2(:,1);wrnc1d('pc1.gph.nc',t,xvar,tunit,time_ini);
%xvar=PCr2(:,2);wrnc1d('pc2.gph.nc',t,xvar,tunit,time_ini);
%xvar=PCr2(:,3);wrnc1d('pc3.gph.nc',t,xvar,tunit,time_ini);


if(1==0) 
 znor = icdf('normal',0.975,0,1.0)
%N = 300;  
%N = 1000;  
 N = 30;  
 xlamd = 14.02; 
 xlamd = 12.61; 
 xlamd = 10.67; 
 xlamd = 10.43; 
%dlamd = xlamd*znor*sqrt(2/N)
 dlamd = expvar*znor*sqrt(2/N)
 x=[1:10] 
 %figure;plot(x,expvar(1:10),'o','MarkerSize',10,x,expvar(1:10)+dlamd(1:10),'+r',x,expvar(1:10)-dlamd(1:10),'+r') 
 figure;
 plot(x,expvar(1:10),'o','MarkerSize',10,'MarkerFaceColor','b');
 hold on; 
 for ix=1:10
  xline = [x(ix)  x(ix)];  yline  = [expvar(ix)-dlamd(ix) , expvar(ix)+dlamd(ix)];
  line(xline,yline,'LineStyle','-','LineWidth',2,'Color','r')
 end 
 ix=1; 
 xline = [x(ix)  x(ix)];  yline  = [expvar(ix)-dlamd(ix) , expvar(ix)+dlamd(ix)];
 line(xline,yline,'LineStyle','-','LineWidth',3,'Color','r')

 plot(x,expvar(1:10),'o','MarkerSize',10,'MarkerFaceColor','b');

 
 title('Eigenvalue Spectrum: Significant EOFs at 95% Confidence Interval','FontSize',14,'FontWeight','bold');
 xlabel('(Eigenvalue Number)','FontSize',13);
 ylabel('(Variance Explained)','FontSize',13);

end 
% 
%
