%---
clear; 
%---
addpath('/home/cmc542/matlablib/cmc/');
%--
%--
%--
%----- 
% var12f = strcat('/home/cmc542/2019/sum-pred/eof/cfs/0.35.cases/LLJ-Z200.pcs.corr.narr.pdf.txt');
  var12f = strcat('/home/cmc542/2019/sum-pred/eof/cfs/0.00.cases/LLJ-Z200-PREC.corr.0.00.pdf.txt');

%--
%--
% outfile = 'pdf.tas.mrsos.historical.07.tas.MIROC5_r1i1p1_07_1950_2005_20C.nc';
% var1 = 'LLJ'; 
% var2 = 'CGT'; 

% outfile = strcat('pdf.',var1,'.',var2,'.1982-2009.May-Jul.dy.nc');

% outfile = strcat('pdf.','LLJ','.','Z200','.1982-2009.May-Jul.dy.nc');
% outfile = strcat('pdf.','LLJ.Z200.PREC','.1982-2009.May-Jul.dy.nc');
  xcase = 1; 
  xcase = 2; 
  xcase = 3; 
  if (xcase == 1 ) 
      outfile = strcat('pdf.2D.','LLJ.PREC','.1982-2009.May-Jul.dy.nc');
  end
  if (xcase == 2 ) 
      outfile = strcat('pdf.2D.','Z200.PREC','.1982-2009.May-Jul.dy.nc');
  end
  if (xcase == 3 ) 
      outfile = strcat('pdf.3D.','LLJ.Z200.PREC','.1982-2009.May-Jul.dy.nc');
  end
%--
  xdata = load(var12f); 
  var1_SW = xdata(:,4);  % LLJ  corr 
  var2_SW = xdata(:,8);  % CGT  corr   
  var3_SW = xdata(:,12); % PREC corr  
%-----
% 
  if (xcase == 1) 
    x1 = var3_SW;  % 
    x2 = var1_SW;  % 
  end
  if (xcase == 2) 
    x1 = var3_SW;  % 
    x2 = var2_SW;  % 
  end
  if (xcase == 3) 
    x1 = var3_SW;  % PREC
    x2 = var2_SW;  % Z200 
    x3 = var1_SW;  % LLJ 
  end

%
r12 = corr(x1,x2); 
mx1 = mean(x1);sx1 = std(x1);
mx2 = mean(x2);sx2 = std(x2);
%-the PDF 
 nx1=abs(-1.0 -  1.0)/30.0; 
 x1D=[-1.0:nx1:1.0]; 
%-
 nx2=abs(-1.0 - 1.0)/30.0; 
 x2D=[-1.0:nx2:1.0]; 
%-
 nx3=abs(-1.0 - 1.0)/30.0; 
 x3D=[-1.0:nx3:1.0]; 
%--------
%-------%
%-[x,y] = meshgrid(x1D,x2D);
%-zx = (x - mx1)./sx1;
%-zy = (y - mx2)./sx2;
%-zxy = ((x - mx1)./sx1).*((y - mx2)./sx2); 
%-z1 = (1.0/( 2*pi*sx1*sx2*sqrt(1-r12.^2))) * exp( -1*(1.0/(2*(1-r12.^2)))*(zx.^2 + zy.^2 - 2*r12*zxy) ); 
%--- 
%---meth2 
[x,y,z] = meshgrid(x1D,x2D,x3D);

x12(:,1) = x1;
x12(:,2) = x2;
x12(:,3) = x3;

mu = mean(x12); 
sigma = cov (x12); 

[xa xb xc] = size(x); 
x1d = reshape(x,1,xa*xb*xc); 
y1d = reshape(y,1,xa*xb*xc); 
z1d = reshape(z,1,xa*xb*xc); 

x1d_12(:,1) = x1d; 
x1d_12(:,2) = y1d; 
x1d_12(:,3) = z1d; 

z1d = mvnpdf (x1d_12, mu,sigma); 
z1d_cdf = mvncdf (x1d_12, mu,sigma); 

z2 = reshape(z1d,xa,xb,xc); 
z2_cdf = reshape(z1d_cdf,xa,xb,xc); 

%-1D unconditional PREC 
 
    pd = fitdist(x1, 'Normal');
    xin = [-1:0.05:1];
    xpdf = pdf(pd,x1D);
    xcdf = cdf(pd,x1D);

%-PDF
idx = find(x1D >= 0.4); % location of 0.4 correlation  
figure;h1a=plot(x1D,z2(:,idx(1),idx(1)),'k','LineWidth',2,'DisplayName','Conditional');  hold on;  % conditional 
       h1b=plot(x1D,xpdf,'r','LineWidth',2,'DisplayName','Unconditional'); hold on;  % unconditional 
       axis([-1 1 0 2.50]);
       set(gca,'XTick',(-1.0:0.2:1.0));
       legend('Location','northwest','Orientation','horizontal'); 

%--text 
       x_title = 'PREC_{CORR} PDF: 3D conditional and 1D unconditional';% ,'FontSize',16,'FontWeight','bold');
       x_ylabel = 'Probability density function'; % ,'FontSize',12);% ,'FontWeight','Bold');
       x_xlabel = 'PREC_{r}(OBS,MODEL)';           % ,'FontSize',12); % --,'FontWeight')%-,'Bold');

       title(x_title);% ,'FontSize',16,'FontWeight','bold');
       ylabel(x_ylabel,'FontSize',12);% ,'FontWeight','Bold');
       xlabel(x_xlabel,'FontSize',12); % --,'FontWeight')%-,'Bold');

       fig4doc('f4.5','PREC_condi_uncondi');

%-CDF
%figure;h1a=plot(x1D,z2_cdf(:,idx(1),idx(1)),'k');  hold on;  % conditional 
%      h1b=plot(x1D,xcdf,'r'); hold on;  % unconditional 
%      axis([-1 1 0 1.00]);
%      set(gca,'XTick',(-1.0:0.2:1.0));
%---
%-figure;contourf(x,y,z1); 
%---- 
%---- 
%   outfile = 'test.nc'; 
    varname='data'; 
    vunits='fraction'; 
    data_fill=-999.9;  
    yrcase = 1975;
    tunit = 'years  ';
    xtmp = strcat(num2str(yrcase),'-01-01 00:00:00');
    time_ini = ['days since ',xtmp];
   [nrow ncol ntime] = size(z2);
    t = [0:ntime-1];
%   wrnc3dll_x(outfile,x1D,x2D,t,z1,tunit,time_ini,data_fill,varname,vunits);
    wrnc3dll_x(outfile,x1D,x2D,x3D,z2,tunit,time_ini,data_fill,varname,vunits);

return 
