#- 
#- CMC Oct 4, 2019 
#-


STEPS:

1. fwr.v850.cfs.dy.csh 
   converts v850 from 6th to daily 
   input: /data2/cmc542/scratch/2019/DATA/CFS/9mo/v850/0.00.cases/wnd850.1983072012.ctl 
   output: /data2/cmc542/scratch/2019/DATA/CFS/9mo/v850/v850.1dy.0.00/wnd850.1983072012.dy.bin
   
   fwr.z200.cfs.dy.csh 
   converts v850 from 6th to daily 
   input: ctlfile: /data2/cmc542/scratch/2019/DATA/CFS/9mo/z200/0.00.cases/z200.1982050112.ctl
   outfile: /data2/cmc542/scratch/2019/DATA/CFS/9mo/z200/z200.1dy.0.00/z200.1982050112.dy.bin

   both scripts use fwr.z200.cfs.dy.gs  and fwr.v850.cfs.dy.gs 

2. Filter z200 using a 12-60 days bandpass 
   bandpass_v850.f1tof2.cfs.csh
   list files in  v850.files.0.00.dy.txt 
   set ifile = /data2/cmc542/scratch/2019/DATA/CFS/9mo/v850/v850.1dy.0.00/$file
   set ofile = /data2/cmc542/scratch/2019/DATA/CFS/9mo/v850/v850.1dy.0.00/$file:r.12-60.dy.bin

   bandpass_z200.f1tof2.cfs.csh
   list files in z200.files.0.00.dy.txt 
   set ifile = /data2/cmc542/scratch/2019/DATA/CFS/9mo/z200/z200.1dy.0.00/$file
   set ofile = /data2/cmc542/scratch/2019/DATA/CFS/9mo/z200/z200.1dy.0.00/$file:r.12-60.dy.bin

3. convert bin to nc 
   ctl2nc.v850.csh
   infiles = /data2/cmc542/scratch/2019/DATA/CFS/9mo/v850/v850.1dy.0.00/ 
   outfile = /data2/cmc542/scratch/2019/DATA/CFS/9mo/v850/v850.1dy.0.00/NC/ 

   ctl2nc.z200.csh
   outfile = /data2/cmc542/scratch/2019/DATA/CFS/9mo/z200/z200.1dy.0.00/NC/$prefix:r.nc
   infiles = /data2/cmc542/scratch/2019/DATA/CFS/9mo/z200/z200.1dy.0.00/*12-60*bin

4. Compute EOF, PCs, and reco filed with modes 1,2,and 3 
   myeofpc_v850_reco_yrs.csh
   input: /data2/cmc542/scratch/2019/DATA/CFS/9mo/v850/v850.1dy.0.00/NC/ 
   outputs:  /data2/cmc542/scratch/2019/DATA/CFS/9mo/v850/v850.1dy.0.00/NC/reco 
             /data2/cmc542/scratch/2019/DATA/CFS/9mo/v850/v850.1dy.0.00/NC/results 
 
   myeofpc_z200_reco_yrs.csh 
   input: /data2/cmc542/scratch/2019/DATA/CFS/9mo/z200/z200.1dy.0.00/NC/ 
   outputs:  /data2/cmc542/scratch/2019/DATA/CFS/9mo/z200/z200.1dy.0.00/NC/reco 
             /data2/cmc542/scratch/2019/DATA/CFS/9mo/z200/z200.1dy.0.00/NC/results 

5. Compute xt on Z200

   fwr.xt.z200.cfs.12-60.csh
      infiles = /data2/cmc542/scratch/2019/DATA/CFS/9mo/z200/z200.1dy.0.00/NC/reco/reco-z200.Full.123.z200.cfs.*.12-60.nc 
      outfile = /data2/cmc542/scratch/2019/DATA/CFS/9mo/z200/z200.1dy.0.00/NC/xt/xt-$prefix:r.bin
   ctl2nc.xt.cfs.csh 
      infile = /data2/cmc542/scratch/2019/DATA/CFS/9mo/z200/z200.1dy.0.00/NC/xt/
      outfile = /data2/cmc542/scratch/2019/DATA/CFS/9mo/z200/z200.1dy.0.00/NC/xt/

6. Compute corretaions and distribution 
       Z200_mode_corr_1D_pdf_full.m  
            infiles = v850.files.0.00.dy.txt 
       LLJ_pc_corr_1D_pdf_full.m 
            infiles = v850.files.0.00.dy.txt 

   for precipitation 
      prec_corr_cfs_1D_pdf_full.m 
      infiles= 'prec.files.dy.txt'; 
      obsfile = strcat('pr-tsrs/prec.cpc.MW.',yys,'.txt');
      outfile = 'PREC.NGP.corr.txt'

7. 2D-distribution
   make file of corelation values : llj, cgt, prec 
   cases.gt.prec_0.00.csh 
   output = LLJ-Z200-PREC.corr.0.00.pdf.txt 


   bi_pdf_3var_LLJ_CGT_PREC_v2_covar.m
   var12f = strcat('/home/cmc542/2019/sum-pred/eof/cfs/0.35.cases/LLJ-Z200.pcs.corr.narr.pdf.txt');
   pdf.2D.LLJ.PREC.1982-2009.May-Jul.dy.nc

8. plot 2D 
    bi_pdf_2var_LLJ_PREC.v1.gs
    bi_pdf_2var_Z200_PREC.v1.gs

9. plot 3D 
   tri_pdf_3var_LLJ_CGT_PREC_v2_covar.m 
   var12f = strcat('/home/cmc542/2019/sum-pred/eof/cfs/0.00.cases/LLJ-Z200-PREC.corr.0.00.pdf.txt');

   outfile = strcat('pdf.3D.','LLJ.Z200.PREC','.1982-2009.May-Jul.dy.nc');
   fig4doc('f4.5','PREC_condi_uncondi'); 


#----
