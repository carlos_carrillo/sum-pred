%---
clear; 
%---
addpath('/home/cmc542/matlablib/cmc/');
%--
%--
%--
  xpath = '/data2/cmc542/scratch/2019/cesm-DUST/';
%----- 
% VAR1
% var1f = strcat(xpath,'f.e12.FAMIPC5CN.f19_19.mvBtoga.001x.cam.h0.AODDUST1.085001-185912.nc'); var1='AODDUST1'; 
%-----
% VAR2
% var2f = strcat(xpath,'f.e12.FAMIPC5CN.f19_19.mvBtoga.001x.clm2.h0.SOILWATER_10CM.085001-185912.nc');var2='SOILWATER_10CM'; 

% var12f = strcat('/home/cmc542/2019/sum-pred/eof/cfs/0.35.cases/LLJ-Z200.pcs.corr.narr.pdf.txt');
  var12f = strcat('/home/cmc542/2019/sum-pred/eof/cfs/0.00.cases/LLJ-Z200-PREC.corr.0.00.pdf.txt');

%--
%--
% outfile = 'pdf.tas.mrsos.historical.07.tas.MIROC5_r1i1p1_07_1950_2005_20C.nc';
% var1 = 'LLJ'; 
% var2 = 'CGT'; 

% outfile = strcat('pdf.',var1,'.',var2,'.1982-2009.May-Jul.dy.nc');

% outfile = strcat('pdf.','LLJ','.','Z200','.1982-2009.May-Jul.dy.nc');
% outfile = strcat('pdf.','LLJ.Z200.PREC','.1982-2009.May-Jul.dy.nc');
  xcase = 1 
  xcase = 2 
  if (xcase == 1 ) 
      outfile = strcat('pdf.2D.','LLJ.PREC','.1982-2009.May-Jul.dy.nc');
  end
  if (xcase == 2 ) 
      outfile = strcat('pdf.2D.','Z200.PREC','.1982-2009.May-Jul.dy.nc');
  end
%--
  xdata = load(var12f); 
  var1_SW = xdata(:,4);  % LLJ  corr 
  var2_SW = xdata(:,8);  % CGT  corr   
  var3_SW = xdata(:,12); % PREC corr  
%-----
% 
  if (xcase == 1) 
    x1 = var3_SW;  % 
    x2 = var1_SW;  % 
  end
  if (xcase == 2) 
    x1 = var3_SW;  % 
    x2 = var2_SW;  % 
  end

%
r12 = corr(x1,x2); 
mx1 = mean(x1);sx1 = std(x1);
mx2 = mean(x2);sx2 = std(x2);
% the PDF 
%-x1D=[min(x1):max(x1)]; 
%nx1=(max(x1)-min(x1) )/20.0; 
%nx1=(max(x1)-min(x1) )/30.0; 
%x1D=[min(x1)-2*nx1:nx1:max(x1)]; 
 nx1=abs(-1.0 -  1.0)/30.0; 
 x1D=[-1.0:nx1:1.0]; 

%-x2D=[min(x2):max(x2)]; 
%nx2=(max(x2)-min(x2) )/20.0; 
 nx2=abs(-1.0 - 1.0)/30.0; 
 x2D=[-1.0:nx2:1.0]; 
%--------
%-------%
[x,y] = meshgrid(x1D,x2D);
% f(x,y) = 
zx = (x - mx1)./sx1;
zy = (y - mx2)./sx2;
zxy = ((x - mx1)./sx1).*((y - mx2)./sx2); 
%-z1 = (1.0/( 2*pi*sqrt(1-r12.^2))) * exp( -1*(1.0/(2*(1-r12.^2)))*(zx.^2 + zy.^2 - 2*r12*zxy) ); 
z1 = (1.0/( 2*pi*sx1*sx2*sqrt(1-r12.^2))) * exp( -1*(1.0/(2*(1-r12.^2)))*(zx.^2 + zy.^2 - 2*r12*zxy) ); 

%--- 
%---meth2 
x12(:,1) = x1;
x12(:,2) = x2;
mu = mean(x12); 
sigma = cov (x12); 

[xa xb] = size(x); 
x1d = reshape(x,1,xa*xb); 
y1d = reshape(y,1,xa*xb); 
x1d_12(:,1) = x1d; 
x1d_12(:,2) = y1d; 
z1d = mvnpdf (x1d_12, mu,sigma); 

z2 = reshape(z1d,xa,xb); 

figure;contourf(x,y,z2); 
%---

figure;contourf(x,y,z1); 

%---- 
%---- 
%   outfile = 'test.nc'; 
    varname='data'; 
    vunits='fraction'; 
    data_fill=-999.9;  
    yrcase = 1975;
    tunit = 'years  ';
    xtmp = strcat(num2str(yrcase),'-01-01 00:00:00');
    time_ini = ['days since ',xtmp];
   [nrow ncol ntime] = size(z1);
    t = [0:ntime-1];
    wrnc3dll_x(outfile,x1D,x2D,t,z1,tunit,time_ini,data_fill,varname,vunits);

return 


    z1nan = z1;
%   z1nan(find(z1<0.06)) = NaN; 
%   z1nan(find(z1<0.04)) = NaN; 
    z1nan(find(z1<0.03)) = NaN; 
    wrnc3dll_x(outfilenan,x1D,x2D,t,z1nan,tunit,time_ini,data_fill,varname,vunits);

    figure;contourf(x1D,x2D,z1nan); 
    xx =  x( find ( max(max(z1)) == z1) ); 
    yy =  y( find ( max(max(z1)) == z1) ); 
    ixo = find( x1D == xx ); 
    iyo = find( x2D == yy );  
    ixf = find( x1D == 305.5); 
    iyf = find( x2D == 20.0);  

    z1lag = nan(size(z1)); 
    for i=ixo-1:ixo+5
        for j=iyo-1:iyo+5 
           z1lag(ixf+i-ixo,iyf+i-ixo) = z1(i,j); 
        end 
    end 

    figure;contourf(x1D,x2D,z1lag); 
   
   

    wrnc3dll_x(outfilelag,x1D,x2D,t,z1nan,tunit,time_ini,data_fill,varname,vunits);
 
%---- 
%---- 
