
*-------------------------------*
'reinit'
*-------------------------------*
epsfile = 'narccap.xt.fig13.eps'
*-------------------------------*

'q gxinfo'

   xline=sublin(result,3);xmax=subwrd(xline,6);xmin=subwrd(xline,4)
   xline=sublin(result,4);ymax=subwrd(xline,6);ymin=subwrd(xline,4)

*  dx = 1.40*(xmax - xmin)/2;
*  dy = 1.25*(ymax - ymin)/4;
*  dx=(xmax-xmin)/2;dy=(ymax-ymin)/2
   dx=0.95*(xmax-xmin)/2;dy=(ymax-ymin)/2
*--------------------------------------
*-- COL1 ------------------------------
   xl=xmin+0.75;xr=xl+dx*0.8;
   yt=ymax-0.75;yb=yt-dy*1.60;

*  ncfile1 = '/FoudreD0/data/ccctus/prog/interann/prog/mtm-svd/NCEPBIG/RCM3/power.nc'
*  ncfile2 = '/FoudreD0/data/ccctus/prog/interann/prog/mtm-svd/NCEPBIG/RCM3/sglev90.ew.nc'
*  ncfile3 = '/FoudreD0/data/ccctus/prog/interann/prog/mtm-svd/NCEPBIG/RCM3/sglev99.ew.nc'

   model = 'RCM3'
* 'xt.ST4.dy.2007.gs 'xl' 'xr' 'yb' 'yt' 'dx' 'dy' '
* 'xt.cpc-hgt.dy.gs 'xl' 'xr' 'yb' 'yt' 'dx' 'dy' '
* 'xt.olam-hgt.dy.jul93.gs 'xl' 'xr' 'yb' 'yt' 'dx' 'dy' '
  'xt.cfs-hgt.dy.5jul93.gs 'xl' 'xr' 'yb' 'yt' 'dx' 'dy' '
* pull stop

*--------------------------------%
