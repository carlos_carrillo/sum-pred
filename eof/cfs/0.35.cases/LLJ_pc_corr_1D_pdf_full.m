%----------------% 
%clear all;

%---------------------------------------------------------------------------------% 
%txtfile = strcat('result/pc1.vwnd.narr.',strcat(num2str(xyear)),'.txt');xtmp = strcat(strcat(num2str(xyear)),'-05-01 00:00:00');
 addpath ~/matlablib/cmc/ 
 addpath ~/ClimX/
%--
 infiles= 'v850.files.dy.txt'; 
%--
 EXTFILES = importdata(infiles);
 ifile = 1; infile = char(EXTFILES(ifile));
 len = length(EXTFILES);
 ic=1;% for correlation index 
 icc=1;% for random correlation index 
 for ifile = 1:len
     infile = char(EXTFILES(ifile));
    [xn nn] = name_struct5(infile,'.'); 
     yymmdd = xn(2).f; 
     yr = str2num( yymmdd(1:4) );
     mo = str2num( yymmdd(5:6) );
     dd = str2num( yymmdd(7:8) );

%yr=2009;mo=07;dd=15;

     syr=num2str(yr);
     smo=num2str(mo); if (mo < 10) ; smo = strcat('0',num2str(mo));  end
     sdd=num2str(dd); if (dd < 10) ; sdd = strcat('0',num2str(dd));  end

%txtfile = strcat('/work/hydro/carrillo/DATA/CFS/9mo/z200/z200.1dy/NC/result/pc1.z200.cfs.',syr,smo,sdd,'.12-60.txt')
% 
% 94 times 
% pc is unrotated,
% init time is smo sdd, of that year (syr) 
% cfsfile = strcat('/work/hydro/carrillo/DATA/CFS/9mo/v850/v850.1dy/NC/result/pc1.v850.cfs.',syr,smo,sdd,'.dy.txt')
     cfsfile = strcat('/data2/cmc542/scratch/2019/DATA/CFS/9mo/v850/v850.1dy/NC/result/pc1.v850.cfs.',syr,smo,sdd,'.dy.txt')
     xtmp = strcat(syr,'-',smo,'-',sdd,' 00:00:00');

% 124 times 
% pcr is unrotated, 
% init time is May 1 of that year 
% narrfile = strcat('/home/hydro/carrillo/2015/sum-pred/eof/narr/allyears/result/pcr1.vwnd.narr.',syr,'.txt')
     narrfile = strcat('/home/cmc542/2019/sum-pred/eof/narr/allyears/result/pcr1.vwnd.narr.',syr,'.txt')
  
     topmo = [31,28,31,30,31,30,31,31,30,31,30,31]; 
     it=1;
     for imo=5:8 
         for idd=1:topmo(imo)
           ldd(it)= idd; 
           lmm(it)= imo;
           it=it+1; 
         end 
     end 
%
% Find index on narr list 

     xidx = find(lmm == mo & ldd == dd);
%lmm(xidx) 
%ldd(xidx) 

%---------------------------------------------------------------------------------% 
     xdata = load(cfsfile); 
     varcfs = xdata(:,2); 
     cfssample = varcfs(1:30); 

     clear xdata; 
     xdata = load(narrfile); 
     varnarr = xdata(:,2); 
%--to check time series is in the boundary 
     if (xidx+29 > length(varnarr)) 
%	 return 
         disp('SOMETHING wrong with the FILE');  
	 if (ifile == 1 ) 
             fid=fopen('LLJ.pcs.corr.txt','w');
	 end 
     else
%--
         narrsample = varnarr(xidx:xidx+29); 
         xcorr=corr(narrsample, cfssample)
         lencorr = length(narrsample); 
         corrs(ic) = xcorr; 
%---random 
         for iran = 1:100
             idx1 = randperm(lencorr, lencorr); 
             idx2 = randperm(lencorr, lencorr); 
             xcorr2 = corr(narrsample(idx1), cfssample(idx2))
             corrs_ran(icc) = xcorr2; 
	     icc=icc+1; 
         end 
%---random 
         ic=ic+1;
	 if (ifile == 1 ) 
             fid=fopen('LLJ.pcs.corr.txt','w');
	 end 
         fprintf(fid,'%4i %2i %2i   %9.4f ',yr,mo,dd, xcorr );fprintf(fid,'\n');
%--
     end
%--
 end 
 fclose(fid);
%-- histogram of ic --% 
 figure; 
 h2=histogram(corrs,'Normalization','probability');
 h2.BinWidth = 0.1; 
 h2.FaceColor = 'c';
 h2.EdgeColor = 'k';
    hold on; 
    h1=histogram(corrs_ran,'Normalization','probability');
    h1.BinWidth = 0.1; 
%   h1.FaceColor = [0.90 0.90 0.90];% 'k'
    h1.FaceColor = [0.25 0.25 0.25];% 'k'
    h1.EdgeColor = 'k';
%---title 
   x_title = 'LLJ_{CORR} PDF';% ,'FontSize',16,'FontWeight','bold');
   x_ylabel = 'Probability density function'; % ,'FontSize',12);% ,'FontWeight','Bold');
   x_xlabel = 'LLJ_{r}(OBS,MODEL)';           % ,'FontSize',12); % --,'FontWeight')%-,'Bold');

    title(x_title);% ,'FontSize',16,'FontWeight','bold');
    ylabel(x_ylabel,'FontSize',12);% ,'FontWeight','Bold');
    xlabel(x_xlabel,'FontSize',12); % --,'FontWeight')%-,'Bold');

    fig4doc('f4.5','LLJ_rand.png');
  

%txtfile = 'result/pc2.z200.narr.1993.txt';xtmp = strcat('1993','-05-01 00:00:00');
%---------------------------------------------------------------------------------% 
%end 
%xvar=x;wrnc1d(ncfile,xtime,xvar,tunit,time_ini);
