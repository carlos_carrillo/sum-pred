%----------------% 
%clear all;

%---------------------------------------------------------------------------------% 
%txtfile = strcat('result/pc1.vwnd.narr.',strcat(num2str(xyear)),'.txt');xtmp = strcat(strcat(num2str(xyear)),'-05-01 00:00:00');
%--
 addpath ~/matlablib/cmc/
 addpath ~/ClimX/
%--
 infiles= 'v850.files.dy.txt';

%yr=2009;mo=07;dd=15;

 EXTFILES = importdata(infiles);
 ifile = 1; infile = char(EXTFILES(ifile));
 len = length(EXTFILES);
 ic=1;% for correlation index 
 icc=1;% for random correlation index 
 for ifile = 1:len
     infile = char(EXTFILES(ifile));
    [xn nn] = name_struct5(infile,'.');
     yymmdd = xn(2).f;
     yr = str2num( yymmdd(1:4) );
     mo = str2num( yymmdd(5:6) );
     dd = str2num( yymmdd(7:8) );
%---  

     syr=num2str(yr);
     smo=num2str(mo); if (mo < 10) ; smo = strcat('0',num2str(mo));  end
     sdd=num2str(dd); if (dd < 10) ; sdd = strcat('0',num2str(dd));  end

%txtfile = strcat('/work/hydro/carrillo/DATA/CFS/9mo/z200/z200.1dy/NC/result/pc1.z200.cfs.',syr,smo,sdd,'.12-60.txt')
% 
% 94 times 
% pc is unrotated,
% init time is smo sdd, of that year (syr) 
% cfsfile = strcat('/work/hydro/carrillo/DATA/CFS/9mo/v850/v850.1dy/NC/result/pc1.v850.cfs.',syr,smo,sdd,'.dy.txt')
% xtmp = strcat(syr,'-',smo,'-',sdd,' 00:00:00');
%-cfsfile = strcat('/work/hydro/carrillo/DATA/CFS/9mo/z200/z200.1dy/NC/xt/xt-reco-z200.Full.z200.cfs.',syr,smo,sdd,'.12-60.nc')
     cfsfile = strcat('/data2/cmc542/scratch/2019/DATA/CFS/9mo/z200/z200.1dy/NC/xt/xt-reco-z200.Full.z200.cfs.',syr,smo,sdd,'.12-60.nc')
     xtmp = strcat(syr,'-',smo,'-',sdd,' 00:00:00');

% 124 times 
% pcr is unrotated, 
% init time is May 1 of that year 
%    narrfile = strcat('/home/hydro/carrillo/2015/sum-pred/eof/narr/allyears/xt/xt-reco-z200.Full.z200.narr.',syr,'.nc')
     narrfile = strcat('/home/cmc542/2019/sum-pred/eof/narr/allyears/xt/xt-reco-z200.Full.z200.narr.',syr,'.nc')
  
     topmo = [31,28,31,30,31,30,31,31,30,31,30,31]; 
     it=1;
     for imo=5:8 
         for idd=1:topmo(imo)
             ldd(it)= idd; 
             lmm(it)= imo;
             it=it+1; 
         end 
     end 
%
% Find index on narr list 

     xidx = find(lmm == mo & ldd == dd);
%lmm(xidx) 
%ldd(xidx) 

%---------------------------------------------------------------------------------% 
%   LOAD CFS FILES  
% xdata = load(cfsfile); 

  ncid = netcdf.open(cfsfile,'NC_NOWRITE');
  rdata = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'z200') );
% lat = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lat') );
  lon = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lon') );
  time = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'time') );
  time_unit = netcdf.getAtt(ncid, netcdf.inqVarID(ncid,'time'),'units' );
%
  data_fill = netcdf.getAtt(ncid,2,'_FillValue');
  netcdf.close(ncid);
%
  rdata (find(rdata == data_fill)) = NaN;

  varcfs = rdata; 
  cfssample = varcfs(:,1:30); 

%---------------
% LOAD NARR FILE
%---------------

  clear rdata; 
  ncid = netcdf.open(narrfile,'NC_NOWRITE');
  rdata = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'z200') );
% lat = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lat') );
  lon = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lon') );
  time = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'time') );
  time_unit = netcdf.getAtt(ncid, netcdf.inqVarID(ncid,'time'),'units' );
%
  data_fill = netcdf.getAtt(ncid,2,'_FillValue');
  netcdf.close(ncid);
%
  rdata (find(rdata == data_fill)) = NaN;
% xdata = load(narrfile); 
  varnarr = rdata; 
 [xx, lens] = size(varnarr); 

     if (xidx+29 > lens)
%        return 
         disp('SOMETHING wrong with the FILE');
         if (ifile == 1 )
%            fid=fopen('LLJ.pcs.corr.txt','w');
             fid=fopen(' Z200.mode.corr.txt','w');
         end
     else
%
          narrsample = varnarr(:,xidx:xidx+29); 
%--------------------
% COMPUTE correlation 
%--------------------
         [nrow ncol] = size(narrsample);
          xcorr=corr( reshape(narrsample,nrow*ncol,1), reshape(cfssample,nrow*ncol,1) )
	  lencorr = lens;
          corrs(ic) = xcorr;
          ic=ic+1;
%---random 
          for iran = 1:100
             idx1 = randperm(nrow, nrow);
             idy1 = randperm(ncol, ncol);
             idx2 = randperm(nrow, nrow);
             idy2 = randperm(ncol, ncol);
%            xcorr2 = corr(narrsample(idx1), cfssample(idx2))
             xcorr2=corr( reshape(narrsample(idx1,idy1),nrow*ncol,1), reshape(cfssample(idx2,idy2),nrow*ncol,1) ); 
             corrs_ran(icc) = xcorr2;
             icc=icc+1;
          end
%---random 
 
          if (ifile == 1 )
             fid=fopen('Z200.mode.corr.txt','w');
          end
          fprintf(fid,'%4i %2i %2i   %9.4f ',yr,mo,dd, xcorr );fprintf(fid,'\n');
     end 
  end 
  fclose(fid);
%
%---------------------------------------------------------------------------------% 
 figure;
 h2=histogram(corrs,'Normalization','probability');
 h2.BinWidth = 0.1;
 h2.FaceColor = 'c';
 h2.EdgeColor = 'k';
%---------------------------------------------------------------------------------% 
    hold on;
    h1=histogram(corrs_ran,'Normalization','probability');
    h1.BinWidth = 0.1;
%   h1.FaceColor = [0.90 0.90 0.90];% 'k'
    h1.FaceColor = [0.25 0.25 0.25];% 'k'
    h1.EdgeColor = 'k';
%---------------------------------------------------------------------------------% 
   x_title = 'CGT_{CORR} PDF';% ,'FontSize',16,'FontWeight','bold');
   x_ylabel = 'Probability density function'; % ,'FontSize',12);% ,'FontWeight','Bold');
   x_xlabel = 'CGT_{r}(OBS,MODEL)';           % ,'FontSize',12); % --,'FontWeight')%-,'Bold');

    title(x_title);% ,'FontSize',16,'FontWeight','bold');
    ylabel(x_ylabel,'FontSize',12);% ,'FontWeight','Bold');
    xlabel(x_xlabel,'FontSize',12); % --,'FontWeight')%-,'Bold');

    fig4doc('f4.5','CGT_rand.png');



