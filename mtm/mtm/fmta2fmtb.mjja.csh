#! /bin/csh 

set file = '/home/carrillo/2012/tree-ring/result/pc1.spi.pnoaa.ja.1895-2008.txt' 
set file = '../prec.cpc.MW.1950-2013.ja.txt'
#set file = '/home/carrillo/2012/tree-ring/result/pc1.spi.pnoaa.nov-apr.1895-2008.txt' 
#set file = '/home/carrillo/2012/tree-ring/result/pc1.EW_RES_100yr.1895-2008.txt' 
#set file = '/home/carrillo/2012/tree-ring/result/pc1.LWadj_RES_100yr.1895-2008.txt' 

set outfile = `echo $file | awk '{print substr($1,38,length($1))".c2.txt"}'` 
set outfile = 'prec.cpc.MW.1950-2013.ja.c2.txt'


set file = '../prec.cpc.MW.1950-2013.a-s.txt' 
set outfile  = 'prec.cpc.MW.1950-2013.a-s.c2.txt' 


set file = '/work/hydro/carrillo/2015/sum-pred/eof/result/pc1.hgt.200.1993.txt' 
set outfile = 'pc1.hgt.200.1993.c2.txt' 

#set file = '/work/hydro/carrillo/2015/sum-pred/eof/result/pc2.hgt.200.1993.txt'
#set outfile = 'pc2.hgt.200.1993.c2.txt'

#set file = '/work/hydro/carrillo/2015/sum-pred/eof/result/pc3.hgt.200.1993.txt' 
#set outfile = 'pc3.hgt.200.1993.c2.txt' 

#set file = '/work/hydro/carrillo/2015/sum-pred/eof/result/pc4.hgt.200.1993.txt'
#set outfile = 'pc4.hgt.200.1993.c2.txt'

#set file = '/work/hydro/carrillo/2015/sum-pred/eof/result/pc5.hgt.200.1993.txt'
#set outfile = 'pc5.hgt.200.1993.c2.txt'

set file = '../hgt.500.narr.MW.1993.a-s.txt'
set outfile = 'hgt.500.narr.MW.1993.a-s.c2.txt'

set file = '../hgt.200.narr.MW.1993.a-s.txt'
set outfile = 'hgt.200.narr.MW.1993.a-s.c2.txt'

 set file = hgt.200.narr.MW.1993.mjja.txt
 set file = hgt.200.narr.MW.1998.mjja.txt
 set file = hgt.200.narr.MW.2005.mjja.txt
 set file = hgt.200.narr.MW.2008.mjja.txt
 set file = hgt.200.narr.MW.2010.mjja.txt
 set file = prec.cpc.MW.1993.mjja.txt
 set file = prec.cpc.MW.1998.mjja.txt
 set file = prec.cpc.MW.2005.mjja.txt
 set file = prec.cpc.MW.2008.mjja.txt
 set file = prec.cpc.MW.2010.mjja.txt
 set file = v.900.narr.MW.1993.mjja.txt
 set file = v.900.narr.MW.1998.mjja.txt
 set file = v.900.narr.MW.2005.mjja.txt
 set file = v.900.narr.MW.2008.mjja.txt
 set file = v.900.narr.MW.2010.mjja.txt


set infile = ../$file
set ofile = $file:r.c2.txt 


echo $infile
echo $ofile

awk '{print $2}' $infile > $ofile

exit 
