*---------------------------* 
'reinit'
*-
ipc = '1' 
*------------------------------*
pngfile = 'power.prec.mjja.all.png'
*------------------------------*
v1.1=0.0
vn.1=2.0
*------------------------------* 
'q gxinfo'
   xline=sublin(result,3);xmax=subwrd(xline,6);xmin=subwrd(xline,4)
   xline=sublin(result,4);ymax=subwrd(xline,6);ymin=subwrd(xline,4)

   xl=xmin+1.5;xr=xmax-4.5;
   yt=ymax-0.5;yb=ymin+8.0;
*--    
   xcase = 'prec2010'; scale = 1; 
   xtit='on';xlab='off';ylab='off'; 
   yt=yt-0.5;yb=yb-0.5; tit2='2010'; 
   call = mkplot(xcase, scale, xl, xr, yt, yb, xtit, xlab, ylab,tit2)
    
   yt=yt-1.5;yb=yb-1.5;
   xtit='on2';xlab='off';ylab='off'; 
   xcase = 'prec1993'; scale = 1; tit2='1993'; 
   call = mkplot(xcase, scale, xl, xr, yt, yb, xtit, xlab, ylab,tit2)
*--
   yt=yt-1.5;yb=yb-1.5;
   xtit='on2';xlab='off';ylab='off'; 
   xcase = 'prec1998'; scale = 1; tit2='1998'; 
   call = mkplot(xcase, scale, xl, xr, yt, yb, xtit, xlab, ylab,tit2)
*--
   yt=yt-1.5;yb=yb-1.5;
*  xtit='off';xlab='on';ylab='off'; 
   xtit='on2';xlab='off';ylab='off'; 
   xcase = 'prec2008'; scale = 1; tit2='2008'; 
   call = mkplot(xcase, scale, xl, xr, yt, yb, xtit, xlab, ylab,tit2)
*--
   yt=yt-1.5;yb=yb-1.5;
   xtit='on2';xlab='on';ylab='off'; 
   xcase = 'prec2010'; scale = 1; tit2='2010'; 
   call = mkplot(xcase, scale, xl, xr, yt, yb, xtit, xlab, ylab,tit2)
*--
  'myframe2 1 1 2 0 0 0 'yt+6.03''
*--
*-------------------* 
*-'mkgm x.gm'
*-'!gm2ps x.gm'
*-'!convert -density 200 x.eps 'pngfile''
*-'!mv x.eps 'epsfile''
*-------------------* 
 'enable print x.gm'
 'print'
 'disable print'
 '!gxeps -c -i x.gm -o x.eps'
 '!convert -density 200 x.eps 'pngfile''
*  xres=2000*0.85
*  yres=2000*1.10
* 'printim 'pngfile' x'xres' y'yres' white'
*-------------------* 
function mkplot(xcase, scale, xl, xr, yt, yb, xtit, xlab, ylab, tit2)
*---------------------------* 
* xtit='on2';xlab='off';ylab='off'; 
* xtit='on2';xlab='on';ylab='off'; 
* xcase = 'prec1993'
* scale = 1; 
* xl=xmin+1.5;xr=xmax-3.5;
* yt=ymax-1.5;yb=ymin+5.0;
*---------------------------* 
ipc = '1' 
*------------------------------*
ncfile = xcase'/power.nc'
*--
ncfile2 = xcase'/sglev90.nc' 
ncfile3 = xcase'/sglev95.nc' 
say ncfile
*------------------------------*
xtitle = 'b) MTM Spectrum: `3D`0PREC'
xtitle2 = '1993: Jun1-Sep1'
*xtitle = ' '
xtitle2 = 'Area: 37-43`aO`nN; 102-90`aO`nW'
*---------------------------* 
v1.1=0.0

vn.1=120
*vn.2=120
*vn.3=120
*vn.4=120
*vn.5=120

*vn.2=330
*
*
if(1=1) 
  'set parea 'xl' 'xr' 'yb' 'yt''

'set grads off'
'set frame off'
'set grid off' 
*-- 
if xlab = 'off'
   'set xlab off'
else 
   if xlab = 'on'
     'set xlab on'
   endif 
endif 
*--
if ylab = 'off'
  'set ylab off'
else 
   if ylab = 'on'
     'set ylab on'
   endif 
endif 
*-----------------------*
*-----------------------*
*'sdfopen  power_dou.nc' 
'sdfopen 'ncfile''

'set xyrev on' 
'set zlog on' 

'set xlopts 1 3 0.12 0.12'
'set ylopts 1 3 0.12 0.12'

'set xflip on' 
'set ylint 10' 
*'set ylint 10000' 
*--scale = 1; 00; 

*'set z 1 2048' 
zlev=495 
'set z 1 'zlev'' 
*
*
*-'set xlevs 2 4 6 10 15 25 50 60'
zlevs = '2 4 6 10 15 30 60'
'set xlevs 'zlevs''

'set vrange 'v1.ipc' 'vn.ipc'' 

'set cmark 0'
'set line 1 1 5' 
*'set line 3 1 5' 
'set cthick 6' 
'set ccolor 8' 

*'d data' 
'd data/'scale'' 
*'close 1'
*-----------------
*'sdfopen  power_dou.nc' 
'sdfopen 'ncfile2''
'sdfopen 'ncfile3''
*
*

'set xyrev on' 
'set zlog on' 

'set xlopts 1 3 0.12 0.12'
'set ylopts 1 3 0.12 0.12'

'set xflip on' 


*-'set z 1 492' 
'set z 1 'zlev'' 
'set xlevs 'zlevs''

'set vrange 'v1.ipc' 'vn.ipc'' 
* 95% CI  
'set cmark 0';'set line 1 1 5';'set cthick 3';'set ccolor 5' 
'd data.3/'scale'' 
*---------------
*---------------
if xtit = 'off' 
else 
   if xtit = 'on' 
     'set strsiz 0.11 0.14'
     'set string 1 l'

     'draw string  'xl+0.02' 'yt+0.14' 'xtitle''
     'draw string  'xl+0.02' 'yt-0.14' 'xtitle2''
   endif 
endif 

*'draw string  'xl' 'yt'         `3D`0ENSO positivo(Nino-Normal)'

'set strsiz 0.13 0.13'
'set string 1 c 3'
 x = (xl+xr)/2
if xlab = 'off'
else 
   'draw string  'x+0.0' 'yb-0.36' frequency(days)'
endif 
*  'draw string  'xr+0.38' 'yb-0.00' ['tit2']'
   'set strsiz 0.12 0.12'
*  'draw string  'xr-0.28' 'yb+0.28' 'tit2''
   'draw string  'xr-0.28' 'yb+0.14' 'tit2''
*----
'close 3'
'close 2'
'close 1'

*----------------* 
endif 
*-------------------* 
return 
