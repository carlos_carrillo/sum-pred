 
'reinit'

*infile = '/home/carrillo/2015/Azar/prog/clim/wrf.R20C-R1.prec.1870-2010.ja.ctl'
*infile = '/work/hydro/carrillo/DATA/prec-temp/prec.livneh.1950-2013.ja.nc'

*infile = '/work/hydro/carrillo/DATA/prec/cpc_us/dy/precip.V1.0.1993.nc'

cyr=1993 
*cyr=2010 
*cyr=1998
*cyr=2008
*cyr=2005
*infile = '/work/hydro/carrillo/DATA/narr/dy/hgt/hgt_narr.ctl'
*infile = '/data2/cmc542/scratch/2019/DATA/CFSR/hgt200/hgt.200.cfsrr.mjja.'cyr'.dy.ctl'
infile = '/data2/cmc542/scratch/2019/DATA/CFSR/vwnd900/v.900.cfsrr.mjja.'cyr'.dy.ctl'

*'sdfopen 'infile''
'open 'infile''

* jan 1854 
* feb 2014 

*------WRITE FILE--------
*  outfile = 'sst.nino3.1854-2013.na.AA.txt'
*  outfile = 'prec.SE-MX.1854-2013.na.AA.txt'
*--SE-MX (Southeast Mexico) 
*  outfile = 'prec.livneh.SE-MX.1950-2013.ja.txt'
*  ltdw = 15.0; ltup = 22.5;
*  lnlf = -105; lnrg = -90.0; 
*--SE-MX (North MidWest) 
*  outfile = 'hgt.500.narr.MW.1993.a-s.txt'
*  outfile = 'hgt.200.narr.MW.1993.a-s.txt'
   outfile = 'v.900.narr.MW.'cyr'.mjja.txt'
*  ltdw = 37.0; ltup = 43.0;
*  lnlf = -102.0; lnrg = -90.0; 
*  lnlf = 258; lnrg = 270; 
*  ltdw = 35.0; ltup = 50.0;
*  lnlf = 270; lnrg = 280; 
*- ltdw = 40.0; ltup = 60.0;
*- lnlf = 270; lnrg = 300; 
   ltdw = 25.0; ltup = 40.0;
   lnlf = 258; lnrg = 265; 
*----ENP (eastern North Pacific) 
*  outfile = 'sst.ENP.1854-2013.na.AA.txt'
*  ltdw = 35.0; ltup = 50.0;
*  lnlf = -150.0; lnrg = -125.0; 
*----CNP (central North Pacific) 
*  outfile = 'sst.CNP.1854-2013.na.AA.txt'
*  ltdw = 26.0; ltup = 36.0;
*  lnlf = 177.0; lnrg = 196.0; 

*  yrini=1854;
*  tn = 160; 
*  yrini=1870;
*  tn = 141; 
   yrini=1950;
   yrini=1993;
*  yr=1993;
   yr=cyr;
*  initime=00Z01Jun%yr;endtime=00Z01Sep%yr
   initime=00Z01Apr%yr;endtime=00Z01Oct%yr
   initime=00Z01May%yr;endtime=00Z01Sept%yr
  'set time 'initime'';'q dim';xline=sublin(result,5);itime=subwrd(xline,9);
  'set time 'endtime'';'q dim';xline=sublin(result,5);etime=subwrd(xline,9);
   say initime":("itime")  "  endtime":("etime")"

*  tn = 93; 
  '!rm -rf 'outfile''
   it=itime;
* 'set lev 500' 
*-'set lev 200';* already 200 lev 
* 'define xmean = ave( ave(ave(hgt,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg'),t='itime',t='etime')'
* 'define xmean = ave( ave(ave(hgt200,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg'),t='itime',t='etime')'
  'define xmean = ave( ave(ave(v900,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg'),t='itime',t='etime')'
   while (it <= etime) 
        'set t 'it''
*       'd ave(ave(hgt,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg')'
*-      'd ave(ave(hgt-xmean,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg')'
*-      'd ave(ave(hgt200-xmean,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg')'
        'd ave(ave(v900-xmean,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg')'

*        say result
         flag = 'off';count=1
         while (flag != 'Result')
             xline = sublin(result,count);xvar=subwrd(xline,4);flag = subwrd(xline,1);
             count=count+1; 
         endwhile
*        pull stop 
*        xline = sublin(result,14);xvar=subwrd(xline,4) 
*        yr = yrini+it-1 
*        say yr'  'xvar
*        tofile = yr'  'xvar
         say it'  'xvar
         tofile = it'  'xvar

*        pull stop 
         call = write(outfile,tofile) 
       it=it+1 
   endwhile 
* 'set t 1 'tn''
*--------------
