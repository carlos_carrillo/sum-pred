'reinit'
pathref="/FoudreD0/data/ccctus/prog/interann/prog/clim/var.iann.ja.gs" 
*----------------------* 
*title1a = 'VAR(P[JA])'; 
 title1a = 'VAR(P)'; 
*----------------------* 
*case=1;* JA  2002 Livneh
case=2;* JA  1999 Livneh 
*case=3;* JA  2010 Livneh 
*case=4;* JA  2012 Livneh 
*case=2;* 
*----------------------* 
*----------------------* 
*----------------------* 
*----------------------* 
*if(case=2) 
    ncfile = '/work/hydro/carrillo/DATA/prec-temp/prec.livneh.1950-2013.ja.ctl' 
    ncfile = '/work/hydro/carrillo/DATA/prec-temp/prec.livneh.1950-2013.mm.ctl'
    ncfile = '/work/hydro/carrillo/DATA/prec-temp/perc.weibull.1950-2013.mm.nc' 


    epsfile = 'pr.mean.ano.livneh.93.ja.eps'

    smo.1='Jan';smo.2='Feb';smo.3='Mar';smo.4='Apr';smo.5='May';smo.6='Jun';
    smo.7='Jul';smo.8='Aug';smo.9='Sep';smo.10='Oct';smo.11='Nov';smo.12='Dec';

    mm.1='01';mm.2='02';mm.3='03';mm.4='04';mm.5='05';mm.6='06';
    mm.7='07';mm.8='08';mm.9='09';mm.10='10';mm.11='11';mm.12='12';

*  'open 'ncfile''
*  'sdfopen 'ncfile''
*  'set display color white'
*  'set background color white'

   'q gxinfo'
    say result
    xline=sublin(result,3);xmax=subwrd(xline,6);xmin=subwrd(xline,4)
    xline=sublin(result,4);ymax=subwrd(xline,6);ymin=subwrd(xline,4)
    dx=(xmax-xmin)/2;dy=(ymax-ymin)/2

    xl=xmin+0.7;xr=xl+dx*1.65;
    yt=ymax-0.5;yb=yt-dy*0.80;

   'set parea 'xl' 'xr' 'yb' 'yt''

    mo=12; moe=12  
    mo=4; moe=12  
    mo=1; moe=12  
    mo=1; moe=11  
*   mo=2; moe=2
*   mo=7; moe=7
    while (mo<=moe) 

    yr = 2010
    yr = 2011
    yr = 2012
    yr = 2013

    ncfile = '/work/hydro/carrillo/DATA/prec-temp/per-weibull.1950-2013.'mm.mo'.nc' 
   'sdfopen 'ncfile''
   'set display color white'
   'set background color white'

*   pngfile = 'pr.mean.ano.livneh.'yr'.'mo'.png'
    pngfile = 'percen.ano.livneh.'yr'.'mo'.gif'

*   title1a = '`0Precipitation Anomaly ('smo.mo') 'yr'  '; 
    title1a = '`0Percentile ('smo.mo') 'yr'  '; 
    initime = '00Z01'smo.mo%yr; 
    title2 = 'Livneh' 
    title1b = '1993: Wet';
*endif 
*--------------------------------------------------------------------------------* 
*--------------------------------------------------------------------------------* 
*epsfile = 'pr.var.var.'%models%'.'%substr(title1b,2,math_strlen(title1b)-2)%'.ja.eps'
 say substr(title1b,2,math_strlen(title1b)-2) 
*----------------------* 
*'open 'ncfile''
*'set display color white'
*'set background color white'

*'q gxinfo'
* say result
* xline=sublin(result,3);xmax=subwrd(xline,6);xmin=subwrd(xline,4)
* xline=sublin(result,4);ymax=subwrd(xline,6);ymin=subwrd(xline,4)
* dx=(xmax-xmin)/2;dy=(ymax-ymin)/2

* xl=xmin+0.7;xr=xl+dx*1.65;
* yt=ymax-0.5;yb=yt-dy*0.80;

* 'set parea 'xl' 'xr' 'yb' 'yt''
*---------
'c'

'set grads off'
'set frame off'
'set xlab off'
'set ylab off'
*---------------* 
* the NAM region 
*'set lat 25 40'
*'set lon -120 -100'
* US region 
*'set lat 15 50'
*'set lat 37 44'
*'set lon -105 -85'

'set lat 25 50'
'set lon -125 -80'

*   var   * 
ntime=12*116
ntime=140
*ntime=12*20
*ntime=12*5
 scale = 1.0/30;* mm/day
 scale = 1.0;* mm/day
*scale = 86400;* mm/day 
**scale = 1.0;* mm/day 
**'define xvar=ave(pow('scale'*data-xave,2),t=1,t='ntime')'
**'define xtmp = 'ntime'*xvar/('ntime-1')'
* initime=00Z1jun%yr;
 'set time 'initime'';'q dim';
  xline=sublin(result,5);itime=subwrd(xline,9);

*'define xvar = smth9('scale'*( data.1 + data.2 ) )'

**'define xmean = ave('scale'*prec,t=1,t=64)'

**'define xmean = ave('scale'*prec,t='mo',t=768,12)'

*'define xvar = smth9(smth9(smth9('scale' * prec(t='itime') ))) - xmean'
'define xvar = smth9(smth9(smth9('scale' * data(t='itime') )))'

'set gxout shaded'
'set grid off'

'set ylopts 1 3 0.15'
'set xlopts 1 3 0.15'

*'set ylint 15'
'redblue'

'set clevs 10 20 30 40 60 70 80 90'
'set ccols 30 28 27 26 0 25 24 23 21'

'd xvar'
'cbarh.2 'xr-1.80' 'yb-0.84' 0'
'set strsiz 0.11 0.12'

*'set strsiz 0.07 0.09'
'set strsiz 0.16 0.18'
'set string 1 l 5 0'
 'draw string 'xr-1.49' 'yt+0.18' [mm/day]'
*'set string 1 l 2 0'
*----------
*
'set gxout contour'
*'set clevs 2 4 6 8 10'
*'set clevs -3 -2 -1 1 2 3'
'set clevs 10 20 30 40 60 70 80 90'
'set ccolor 1'
'set clab off'
'set cthick 1'
'd xvar'
*

*'basemap O 0 1 L' 

'draw map'
'set strsiz 0.16 0.18'
'set string 1 l 5'
'draw string 'xl+0.22' 'yt+0.18' 'title1a''
'set strsiz 0.16 0.18'
*'draw string 'xl+0.0' 'yt-0.68' 'title1b''
*'draw string 'xl+0.0' 'yb+0.86' 'title1b''



'set strsiz 0.16 0.18'
'set string 1 l 5'
'draw string 'xl+0.23' 'yb+0.14' 'title2''


'myframe 1 1 2'

* x-y label 
 'q gxinfo';*the first parea changes when the plot is defined
   xline=sublin(result,3);xr2=subwrd(xline,6);xl2=subwrd(xline,4)
   xline=sublin(result,4);yt2=subwrd(xline,6);yb2=subwrd(xline,4)

xlab=on
ylab=on
*if(xlab=on);xvlf=0;xvrg=180;xint=60;
*            call=myxlabh(xl2,xr2,yt2,yb2,xvlf,xvrg,xint);endif
if(xlab=on);xvlf=-125;xvrg=-80;xint=5;
            call=myxlabh(xl2,xr2,yt2,yb2,xvlf,xvrg,xint);endif
if(ylab=on);yvb=25.0;yvt=50.0;yint=5.0;
            call=myylabh(xl2,xr2,yt2,yb2,yvb,yvt,yint);endif
* 
*------------------------

'set mpdraw on'
*'set mpdset mres'
'set map 1 1 2'
'set mpdset hires'
*'set mpdset hires'
'draw map'

'set mpdset lowres'
'set map 1 1 5'
'draw map'


*-----------
'set strsiz 0.10 0.12'
'set string 1 l'
*'draw string 0.5 2.40 'pathref''

*------------------------* 
if (1=1) 
'enable print x.gm'
'print'
'disable print'
*'!gxeps -r -c -i x.gm -o x.eps'
'!gxps -c -i x.gm -o x.eps'
'!convert -density 200 x.eps 'pngfile''

*'printim 'pngfile''
*'!mv x.eps 'epsfile''
endif 
*-------------------------* 
      'close 1'
       mo=mo+1;
    endwhile 

*---------------------------
function myxlabh(xl,xr,yt,yb,xvlf,xvrg,xint)
* xvlf=-120;xvrg=-20;xint=20 
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,2);x1=subwrd(xline,6);xn=subwrd(xline,8)
*
  nptos=(xvrg-xvlf)/xint + 1;
*
  it=1;while(it<=nptos)
          xx.it = xvlf + (it-1)*xint 
*          say xx.it 
       it=it+1
       endwhile
* lab 
  it=1;while(it<=nptos)
        xreal=xx.it
        xabs=math_abs(xx.it)
        say xreal 
        if (xreal<0);
           if (xreal>-180);
               xxlb.it=xabs%"`aO`nW";
           else; 
                xtmp = 360-xabs
                xxlb.it=xtmp%"`aO`nE";
           endif
        else
           if(xreal<=180) 
            xxlb.it=xabs%"`aO`nE";
           else 
              xtmp = 360-xabs
              xxlb.it=xtmp%"`aO`nW";
           endif 
           if(xreal=0|xreal=360);xxlb.it="0`aO`n";endif

        endif
        if(xreal=0|xreal=180);xxlb.it=xabs%"`aO`n";endif
*        xxlb.it=math_abs(xx.it) 
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say xxlb.it 
        it=it+1
       endwhile
*

*
  ip=1;while(ip<=nptos)
         xx = xx.ip
         xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         'set line 1 1 1'
         'draw line 'xs' 'yb' 'xs' 'yb-0.075''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
         y = yt
*        'draw line 'xs' 'yb' 'xs' 'yt''
*        'set strsiz 0.14'
         'set strsiz 0.14 0.15'
         'set string 1 c 3'
         'draw string 'xs'  'yb-0.175' 'xxlb.ip''
      ip=ip+1
      endwhile
return
*------------------------
function myylabh(xl,xr,yt,yb,yvb,yvt,yint)
* yvb=-40;yvt=40;yint=10;
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,3);y1=subwrd(xline,6);yn=subwrd(xline,8)
  nptos=(yvt-yvb)/yint + 1;
*
  it=1;while(it<=nptos)
          yy.it = yvb  + (it-1)*yint 
*          say yy.it 
       it=it+1
       endwhile
* lab 

  it=1;while(it<=nptos)
        yreal=yy.it
        yabs=math_abs(yy.it)
        if(yreal=0);yylb.it="EQ";endif
        if(yreal<0);yylb.it=yabs%"`aO`nS";endif
        if(yreal>0);yylb.it=yabs%"`aO`nN";endif
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say yylb.it 
        it=it+1
       endwhile
*
  ip=1;while(ip<=nptos)
         yy = yy.ip
*        xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         ys = yb+(yy-y1)*(yt-yb)/(yn-y1)
         'set line 1 1 1'
         'draw line 'xl-0.075' 'ys' 'xl' 'ys''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
*         'draw line 'xs' 'yb' 'xs' 'y''
*        'draw line 'xr' 'ys' 'xl' 'ys''
*        'set strsiz 0.14'
*        'set strsiz 0.08 0.09'
         'set strsiz 0.14 0.15'
         'set string 1 r 3'
*         'draw string 'xs'  'yb-0.2' 'xxlb.ip''
         'draw string 'xl-0.10'  'ys' 'yylb.ip''
      ip=ip+1
      endwhile
return
*----------------------------------------------

