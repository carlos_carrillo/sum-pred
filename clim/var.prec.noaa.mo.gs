'reinit'
pathref="/FoudreD0/data/ccctus/prog/interann/prog/clim/var.iann.ja.gs" 
*----------------------* 
*title1a = 'VAR(P[JA])'; 
 title1a = 'VAR(P)'; 
*----------------------* 
case=1;* WRF-20CR 
*case=2;* 
*----------------------* 
*----------------------* 
*----------------------* 
 if(case=1) 
*ncfile = '/FoudreD0/data/ldeo/prec.1895-2010.mm.nc'
*ncfile = '/FoudreD0/data/rean20C/mo/prate.mon.mean.nc'
*ncfile = '/home/carrillo/2014/rean20c/wrfR20C/iann/wrf.R20C-R1.prec.1870-2010.mm.ctl'
 ncfile = '/work/hydro/carrillo/DATA/prec/ldeo/prec.1895-2010.mm.nc'
 epsfile = 'pr.var.noaa.mo.eps'
 pngfile = 'pr.var.noaa.mo.png'
 models = 'PNOAA' 
 title2 = 'NOAA[mo]' 
 title1b = '(1895-2010)';endif 
*----------------------* 
 if(case=2) 
 ncfile = '/FoudreD0/data/ccctus/prog/interann/data/pr_CRCM_ccsm.01-2038.to.11-2070.ja.0.5x0.5.nc'
 models = 'CRCM_ccsm' 
 title2 = 'CRCM[ccsm]' 
 title1b = '(2038-2070)';endif 
*----------------------* 
*--------------------------------------------------------------------------------* 
*--------------------------------------------------------------------------------* 
*epsfile = 'pr.var.'%models%'.'%substr(title1b,2,math_strlen(title1b)-2)%'.ja.eps'
 say substr(title1b,2,math_strlen(title1b)-2) 
*----------------------* 
'sdfopen 'ncfile''
*'open 'ncfile''
*
 'q gxinfo'
  say result
  xline=sublin(result,3);xmax=subwrd(xline,6);xmin=subwrd(xline,4)
  xline=sublin(result,4);ymax=subwrd(xline,6);ymin=subwrd(xline,4)
  dx=(xmax-xmin)/2;dy=(ymax-ymin)/2

  xl=xmin+0.7;xr=xl+dx*0.65;
  yt=ymax-0.1;yb=yt-dy*0.65;

 'set parea 'xl' 'xr' 'yb' 'yt''
*---------

'set grads off'
'set frame off'
'set xlab off'
'set ylab off'
*---------------* 
* the NAM region 
*'set lat 25 40'
*'set lon -120 -100'
* US region 
*'set lat 15 49'
*'set lon -125 -80'
* MX region 
*'set lat 15 38'
*'set lon -124 -80'
* MX region 
 'set lat 15 48'
 'set lon -124 -80'

*   var   * 
ntime=12*116
ntime=12*142
ntime=12*2
ntime=12*63
ntime=12*116
*ntime=12*20
*ntime=12*5

 scale = 86400;* mm/day 
 scale = 1.0/30;* mm/month to mm/day
*scale = 1.0;* mm/day
*'define xave=ave('scale'*prec,t=1,t='ntime')'
*'define xvar=ave(pow('scale'*prec-xave,2),t=1,t='ntime')'
*'define xtmp = xvar/('ntime-1')'

*'define xave=ave('scale'*prate,t=1,t='ntime')'
*'define xvar=ave(pow('scale'*prate-xave,2),t=1,t='ntime')'
*'define xtmp = 'ntime'*xvar/('ntime-1')'

*'define xvar=ave(pow('scale'*prec-xave,2),t=1,t='ntime')'
*'define xtmp = 'ntime'*xvar/('ntime-1')'
*'define xvar =sum(pow('scale'*prec-xave,2),t=1,t='ntime')'
*'define xtmp = xvar/('ntime-1')'
'define xave=ave('scale'*prec,t=1,t='ntime')'
 'define xvar=ave(pow('scale'*prec-xave,2),t=1,t='ntime')'
 'define xtmp = 'ntime'*xvar/('ntime-1')'


*'define xtmp = 'ntime'*xvar/('ntime-1')'

*'define xvar=ave('scale'*prec,t=1,t='ntime')'
*'define xvar = sqrt(xtmp)'
'define xvar = smth9(xtmp/10)'
*'define xvar = 100*smth9(xvar)'
*'define xvar = 1*smth9(xvar)'

*'define xvar = smth9(xave)'

'set gxout shaded'
'set grid off'

'set ylopts 1 3 0.15'
'set xlopts 1 3 0.15'
*--------------  
*'set ylint 15'
'redblue'
*'set clevs 1.0 2.0 3.0 4.0 5.0'
'set clevs 0.5 1.0 1.5 2.0 2.5'
'set ccols 0 25 24 23 22 21'
'd xvar'
'cbarh 'xr-0.65' 'yt-0.52' 0'
'set strsiz 0.12 0.12'

'set strsiz 0.07 0.09'
'set string 1 l 2 0'
*'draw string 'xr-1.64' 'yt-0.450' 10`a-2`n(mm/day)`a2`n'
'draw string 'xr-1.62' 'yt-0.450' 10*(mm/day)`a2`n'
*'set string 1 l 2 0'
*----------
*
'set gxout contour'
*  if (case=1|case=2|case=3|case=4|case=13|case=14|case=15|case=16) 
*    'set cint 0.5';'set cmin 0.5';'set cmax 2.5';
*  else 
*    'set cint 1.0';'set cmin 1.0';'set cmax 5.0';
*  endif 
*'set clevs 1.0 2.0 3.0 4.0 5.0'
*'set clevs 0.5 1.0 1.5 2.0 2.5'
*'set clevs 1.0 2.0 3.0 4.0 5.0'
'set clevs 0.5 1.0 1.5 2.0 2.5'
*'set clevs 1.0 1.5 2.0 2.5 3.0'
'set ccolor 1'
'set clab off'
'set cthick 1'
'd xvar'
*
'draw map'
'set strsiz 0.13 0.14'
'set string 1 l 4'
'draw string 'xl+0.0' 'yt-0.40' 'title1a''
'set string 1 l 4'
'set strsiz 0.12 0.12'
'draw string 'xl+0.0' 'yt-0.68' 'title1b''
*'draw string 'xl+0.0' 'yb+0.74' 'title1b''

'basemap O 0 1 L' 

'set strsiz 0.12 0.13'
'set string 1 l 4'
'draw string 'xl+0.02' 'yb+0.68' 'title2''


'myframe 1 1 2'

* x-y label 
 'q gxinfo';*the first parea changes when the plot is defined
   xline=sublin(result,3);xr2=subwrd(xline,6);xl2=subwrd(xline,4)
   xline=sublin(result,4);yt2=subwrd(xline,6);yb2=subwrd(xline,4)

xlab=on
ylab=on
*if(xlab=on);xvlf=0;xvrg=180;xint=60;
*            call=myxlabh(xl2,xr2,yt2,yb2,xvlf,xvrg,xint);endif
if(xlab=on);xvlf=-120;xvrg=-80;xint=10;
            call=myxlabh(xl2,xr2,yt2,yb2,xvlf,xvrg,xint);endif
if(ylab=on);yvb=15.0;yvt=35.0;yint=5.0;
            call=myylabh(xl2,xr2,yt2,yb2,yvb,yvt,yint);endif
* 
*------------------------

'set mpdraw on'
*'set mpdset mres'
'set map 1 1 2'
'set mpdset hires'
'draw map'

'set mpdset lowres'
'set map 1 1 5'
'draw map'


*-----------
'set strsiz 0.10 0.12'
'set string 1 l'
'draw string 0.5 2.40 'pathref''

*------------------------* 
if (1=1) 
'enable print x.gm'
'print'
'disable print'
'!gxeps -c -i x.gm -o x.eps'
'!convert x.eps 'pngfile''
'!mv x.eps 'epsfile''
endif 
*-------------------------* 



*---------------------------
function myxlabh(xl,xr,yt,yb,xvlf,xvrg,xint)
* xvlf=-120;xvrg=-20;xint=20 
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,2);x1=subwrd(xline,6);xn=subwrd(xline,8)
*
  nptos=(xvrg-xvlf)/xint + 1;
*
  it=1;while(it<=nptos)
          xx.it = xvlf + (it-1)*xint 
*          say xx.it 
       it=it+1
       endwhile
* lab 
  it=1;while(it<=nptos)
        xreal=xx.it
        xabs=math_abs(xx.it)
        say xreal 
        if (xreal<0);
           if (xreal>-180);
               xxlb.it=xabs%"`aO`nW";
           else; 
                xtmp = 360-xabs
                xxlb.it=xtmp%"`aO`nE";
           endif
        else
           if(xreal<=180) 
            xxlb.it=xabs%"`aO`nE";
           else 
              xtmp = 360-xabs
              xxlb.it=xtmp%"`aO`nW";
           endif 
           if(xreal=0|xreal=360);xxlb.it="0`aO`n";endif

        endif
        if(xreal=0|xreal=180);xxlb.it=xabs%"`aO`n";endif
*        xxlb.it=math_abs(xx.it) 
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say xxlb.it 
        it=it+1
       endwhile
*

*
  ip=1;while(ip<=nptos)
         xx = xx.ip
         xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         'set line 1 1 1'
         'draw line 'xs' 'yb' 'xs' 'yb-0.075''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
         y = yt
*        'draw line 'xs' 'yb' 'xs' 'yt''
*        'set strsiz 0.14'
         'set strsiz 0.08 0.09'
         'set string 1 c 3'
         'draw string 'xs'  'yb-0.175' 'xxlb.ip''
      ip=ip+1
      endwhile
return
*------------------------
function myylabh(xl,xr,yt,yb,yvb,yvt,yint)
* yvb=-40;yvt=40;yint=10;
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,3);y1=subwrd(xline,6);yn=subwrd(xline,8)
  nptos=(yvt-yvb)/yint + 1;
*
  it=1;while(it<=nptos)
          yy.it = yvb  + (it-1)*yint 
*          say yy.it 
       it=it+1
       endwhile
* lab 

  it=1;while(it<=nptos)
        yreal=yy.it
        yabs=math_abs(yy.it)
        if(yreal=0);yylb.it="EQ";endif
        if(yreal<0);yylb.it=yabs%"`aO`nS";endif
        if(yreal>0);yylb.it=yabs%"`aO`nN";endif
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say yylb.it 
        it=it+1
       endwhile
*
  ip=1;while(ip<=nptos)
         yy = yy.ip
*        xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         ys = yb+(yy-y1)*(yt-yb)/(yn-y1)
         'set line 1 1 1'
         'draw line 'xl-0.075' 'ys' 'xl' 'ys''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
*         'draw line 'xs' 'yb' 'xs' 'y''
*        'draw line 'xr' 'ys' 'xl' 'ys''
*        'set strsiz 0.14'
         'set strsiz 0.08 0.09'
         'set string 1 r 3'
*         'draw string 'xs'  'yb-0.2' 'xxlb.ip''
         'draw string 'xl-0.10'  'ys' 'yylb.ip''
      ip=ip+1
      endwhile
return
*----------------------------------------------

