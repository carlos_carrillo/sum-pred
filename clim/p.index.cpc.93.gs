 
'reinit'

 OLAM = R1F0.I8E1
*OLAM = R2F1.I1E1
*OLAM = R2F1.I2E1
*OLAM = R2F1.I3E1
*OLAM = R2F1.I4E1
*OLAM = R2F1.I5E1
*OLAM = R2F1.I6E1
*OLAM = R2F1.I7E1
*OLAM = R2F1.I8E1
 OLAM = R2F1.I9E1
*OLAM = R1F1.I10E1

infile = '/work/hydro/carrillo/2015/backup_tusker/work/MODELS/OLAM/runs/'OLAM'/NC/pr_olam.ctl'
outfile = 'pr-tsrs/prec.olam.MW.'OLAM'.93.txt'

infile = '/work/hydro/carrillo/DATA/prec/cpc_us/dy/precip.V1.0.1993.nc'
outfile = 'pr-tsrs/prec.olam.MW.cpc.93.txt'


 'sdfopen 'infile''
*'open 'infile''

* jan 1854 
* feb 2014 

*------WRITE FILE--------
*  outfile = 'sst.nino3.1854-2013.na.AA.txt'
*  outfile = 'prec.SE-MX.1854-2013.na.AA.txt'
*--SE-MX (MidWest) 
   ltdw = 37.0; ltup = 43.0;
*  lnlf = -102; lnrg = -90.0; 
   lnlf = 258.0; lnrg = 270.0;*for   lnlf = -102; lnrg = -90.0; 
*----ENP (eastern North Pacific) 
*  outfile = 'sst.ENP.1854-2013.na.AA.txt'
*  ltdw = 35.0; ltup = 50.0;
*  lnlf = -150.0; lnrg = -125.0; 
*----CNP (central North Pacific) 
*  outfile = 'sst.CNP.1854-2013.na.AA.txt'
*  ltdw = 26.0; ltup = 36.0;
*  lnlf = 177.0; lnrg = 196.0; 

*  yrini=1854;
*  tn = 160; 
*  yrini=1870;
*  tn = 141; 
*  yrini=1950;
   tn = 21; 
  'set time 00Z1Mar1993'
  'q dim';xline = sublin(result,5);t1=subwrd(xline,9);
  'set time 00Z1Nov1993'
  'q dim';xline = sublin(result,5);tn=subwrd(xline,9);
*  say t1' 'tn 
  '!rm -rf 'outfile''
*  it=1;
   it=t1;
   while (it <= tn) 
        'set t 'it''
        'q dims';xline = sublin(result,5);xtime=subwrd(xline,6);
*       'd ave(ave(data,lat=-5.0,lat=5.0),lon=-150.0,lon=-90.0)'
*       'd ave(ave(data,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg')'
*       'd ave(ave(data,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg')'
        'd ave(ave(precip,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg')'
*        say result
         flag = 'off';count=1
         while (flag != 'Result')
             xline = sublin(result,count);xvar=subwrd(xline,4);flag = subwrd(xline,1);
             count=count+1; 
         endwhile
*        pull stop 
*        xline = sublin(result,14);xvar=subwrd(xline,4) 
*        yr = yrini+it-1 
         say xtime'  'xvar
         tofile = xtime'  'xvar
*        tofile = yr'  'xvar
*        pull stop 
         call = write(outfile,tofile) 
       it=it+1 
   endwhile 
* 'set t 1 'tn''
*--------------
