#! /bin/csh 


#----------------------

# WET years 

set yy  = 1982
set yy  = 1991
set yy  = 1995
set yy  = 1999
set yy  = 2005 
set yy  = 2008 

# DRY YEARS
# #
set yy = 1985
set yy = 1989
set yy = 2002
#set yy = 2006
#

# NORMAL YEARS
# 
# 
set yy = 1983
set yy = 1984
set yy = 1986
set yy = 1987
set yy = 1988
set yy = 1990
set yy = 1992
set yy = 1993
set yy = 1994
#
set yy = 1996
set yy = 1997
set yy = 1998
set yy = 2000
set yy = 2001
set yy = 2003
set yy = 2004
set yy = 2007
set yy = 2009
#
#

set infiles = `cat cfs.$yy.cases.txt`
set outfile = "corr.cfs.cpc.cases.$yy.txt" 
rm -rf $outfile 

foreach infile ($infiles) 
#  set infile = 'pr-tsrs/prec.cfs.MW.1982050112.txt'
# set obsfile = "pr-tsrs/prec.cpc.MW.1982.txt";
  set obsfile = "pr-tsrs/prec.cpc.MW.$yy.txt";

# set  initime = "00Z02MAY$yy";
# set  initime = "00Z02MAY1982";
  set  initime = `head -1 $infile | awk '{print $1}'`;
#-------------------------------
# set  endtime = 00Z02JUN1982;

  ln -sf $infile  prec.cfs.txt 
  ln -sf $obsfile prec.cpc.txt 

#matlab -nodisplay -r "infile=$infile,obsfile=$obsfile,initime='$initime',corr_cfs_yr"
  matlab -nodisplay -r "initime='$initime',corr_cfs_yr,quit"
  echo "OUTPUT file : xcorr.txt "
  set xcorr = `cat xcorr.txt`
  echo $infile $xcorr  >> $outfile 
# exit 
end 

