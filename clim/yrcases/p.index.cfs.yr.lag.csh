#! /bin/csh 


#set infile = '/work/hydro/carrillo/DATA/CFS/9mo/prate/1982/prate.1993092312.ctl'

 set yy = 1982 
 set yy = 1991
 set yy = 1995
 set yy = 2005
 set yy = 2008
 set yy = 1999
 set yy = 1993

# DRY YEARS
# 
 set yy = 1985
 set yy = 1989
 set yy = 2002
 set yy = 2006
#

# NORMAL YEARS
#
#
  set yy = 1983
  set yy = 1984
  set yy = 1986
  set yy = 1987
  set yy = 1988
  set yy = 1990
  set yy = 1992
  set yy = 1994
#
  set yy = 1996
  set yy = 1997
  set yy = 1998
  set yy = 2000
  set yy = 2001
  set yy = 2003
  set yy = 2004
  set yy = 2007
  set yy = 2009
#

set files = `ls /work/hydro/carrillo/DATA/CFS/9mo/prate/${yy}/*.ctl` 

foreach file ($files) 

# set infile = '/work/hydro/carrillo/DATA/CFS/9mo/prate.1993080412.ctl'
  set infile = $file
  set xdate = `echo $file | awk '{a=split($1,b,"."); print b[a-1]}'` 
# set outfile = "pr-tsrs/prec.cfs.MW.${xdate}.txt" 
  set outfile = "pr-tsrs.lag15/prec.cfs.MW.${xdate}.txt" 
  echo $xdate
  echo $outfile
# grads -blc "run p.index.cfs.yr.gs $infile $outfile"
  grads -blc "run p.index.cfs.yr.lag15.gs $infile $outfile"
# exit

# exit 
end 

