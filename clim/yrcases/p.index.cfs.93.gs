 
'reinit'

 
 CFS = 1982050612
*CFS = 1993061012
*CFS = 1993061512
*CFS = 1993062012
*CFS = 1993062512
*CFS = 1993063012
*CFS = 1993070512
*CFS = 1993071012
*CFS = 1993071512
*CFS = 1993072012
*CFS = 1993072512
*CFS = 1993073012
*CFS = 1993080412
*CFS = 1993080912
*CFS = 1993081412
*CFS = 1993081912
*CFS = 1993082412
*CFS = 1993082912
*CFS = 1993090312
*CFS = 1993090812
*CFS = 1993091312
*CFS = 1993091812
*CFS = 1993092312
*CFS = 1993092812

*infile = '/work/hydro/carrillo/2015/backup_tusker/work/MODELS/OLAM/runs/'OLAM'/NC/pr_olam.ctl'
*outfile = 'pr-tsrs/prec.olam.MW.'OLAM'.93.txt'

infile = '/work/hydro/carrillo/DATA/CFS/9mo/prate/1982/prate.'CFS'.ctl'
outfile = 'pr-tsrs/prec.cfs.MW.'CFS'.txt'


*'sdfopen 'infile''
 'open 'infile''

* jan 1854 
* feb 2014 

*------WRITE FILE--------
*  outfile = 'sst.nino3.1854-2013.na.AA.txt'
*  outfile = 'prec.SE-MX.1854-2013.na.AA.txt'
*--SE-MX (MidWest) 
   ltdw = 37.0; ltup = 43.0;
   lnlf = -102; lnrg = -90.0; 
*----ENP (eastern North Pacific) 
*  outfile = 'sst.ENP.1854-2013.na.AA.txt'
*  ltdw = 35.0; ltup = 50.0;
*  lnlf = -150.0; lnrg = -125.0; 
*----CNP (central North Pacific) 
*  outfile = 'sst.CNP.1854-2013.na.AA.txt'
*  ltdw = 26.0; ltup = 36.0;
*  lnlf = 177.0; lnrg = 196.0; 

*  yrini=1854;
*  tn = 160; 
*  yrini=1870;
*  tn = 141; 
*  yrini=1950;
*  tn = 21; 
* 'q file';xline = sublin(result,5);tn=subwrd(xline,12);
  '!rm -rf 'outfile''
*-----
  'set t 1';'q dim';xline=sublin(result,5);xtime=subwrd(xline,6);initime=00Z%substr(xtime,4,12);
* initime=00Z015Jun1993;
  endtime=00Z01Oct1993;
 'set time 'initime'';'q dim';xline=sublin(result,5);itime=subwrd(xline,9);
 'set time 'endtime'';'q dim';xline=sublin(result,5);etime=subwrd(xline,9);
 
  say initime"("itime") "endtime"("etime") "
*
   it=itime;
   it=1;
   it=2;
   tn=etime;
   tn=60*4;
   scale = 86400; 
   while (it <= tn) 
        et=it+3;
        'set t 'it''
        'q dims';xline = sublin(result,5);xtime=subwrd(xline,6);
*       'd ave(ave(data,lat=-5.0,lat=5.0),lon=-150.0,lon=-90.0)'
*       'd ave(ave(data,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg')'
*       'd ave(ave(data,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg')'

*       'd ave(ave('scale'*PRATEsfc,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg')'
        'd ave( ave(ave('scale'*PRATEsfc,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg'), t='it',t='et')'

*        say result
         flag = 'off';count=1
         while (flag != 'Result')
             xline = sublin(result,count);xvar=subwrd(xline,4);flag = subwrd(xline,1);
             count=count+1; 
         endwhile
*        pull stop 
*        xline = sublin(result,14);xvar=subwrd(xline,4) 
*        yr = yrini+it-1 
         say xtime'  'xvar
         tofile = xtime'  'xvar
*        tofile = yr'  'xvar
*        pull stop 
         call = write(outfile,tofile) 
*      it=it+1 
       it=it+4 
   endwhile 
* 'set t 1 'tn''
*--------------
