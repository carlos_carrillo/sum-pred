
function powerlfv(args)
  xl=subwrd(args,1)
  xr=subwrd(args,2)
  yb=subwrd(args,3)
  yt=subwrd(args,4)

  dx=subwrd(args,5)
  dy=subwrd(args,6)


  'set parea 'xl' 'xr' 'yb' 'yt''
*---------
* wet model 
  'set grads off'
  'set grid off'
  'set frame off'
* 'set xlab on'
  'set xlab off' 
  'set xlint 5'
  'set ylopts 1 2 0.09 0.11' 
  'set xlopts 1 4 0.08 0.11' 

*'open pr_CRCM_cgcm3.81.ctl'
 epsfile='hgt_narr_m2.93.eps'
 pngfile='hgt_narr_m2.93.png'
*pngfile='hgt_narr_m1.93.png'
*'open pr_RCM3_cgcm3.81.ctl'
*'open /FoudreD0/data/prec/radars4/prad.24hr.ctl' 
*'open /work/hydro/carrillo/DATA/prec-temp/prec.livneh.1950-2013.dd.ctl' 

* gphfile='/work/hydro/carrillo/2015/sum-pred/eof/reco-gph.mod35.nc'
  gphfile='/home/hydro/carrillo/2015/sum-pred/eof/reco-gph.mod35.nc'
* gphfile='/work/hydro/carrillo/2015/sum-pred/eof/reco-gph.mod12.nc'

 'sdfopen /work/hydro/carrillo/DATA/prec/cpc_us/dy/precip.V1.0.1993.nc' 
*'sdfopen /work/hydro/carrillo/2015/sum-pred/eof/reco-gph.mod35.nc' 

*xtitle = 'P[NW:37`aO`nN-43`aO`nN]   CPC-US   mm/day'
 xtitle = '`3D`0HGT (MODE 2: EOFs 3,4,5) NARR [m]'
 xtitle = 'P[CPC], `3D`0HGT (EOFs 3,4,5) NARR [m]'
*xtitle = '`3D`0HGT (MODE 2: EOFs 1,2) NARR [m]'
*xtitle2 = '30`aO`nN 37.5`aO`nN'
 xtitle2 = ' '
 xtitle2 = '1993: [NW:40`aO`nN-50`aO`nN]'

* dry model 
*open pr_RCM3_cgcm3.81.ctl';
*'open pr_RCM3_cgcm3.86.ctl';
*'set lat 34'
*'set lon 240 260'
*'set lon 245 260'

*'set lat 32'

*'set lon -118 -108'
 'set lat 40'
 'set lon 258  270'

  initime=00Z01Jun2007;endtime=00Z01Oct2007;
  initime=00Z01Jun1993;endtime=00Z01Aug1993;
  initime=00Z01Jul1993;endtime=00Z01Aug1993;
  initime=00Z01Jun1993;endtime=00Z01Aug1993;
  initime=00Z01Jun1993;endtime=00Z01Sep1993;

* initime=00Z01Mar1993;endtime=00Z01Nov1993;
  initime=00Z015Jun1993;endtime=00Z15Sep1993;

 'set time 'initime'';'q dim';
  xline=sublin(result,5);itime=subwrd(xline,9);
 'set time 'endtime'';'q dim';
  xline=sublin(result,5);etime=subwrd(xline,9);
  say initime"("itime") "endtime"("etime") "

 'set t 'itime' 'etime''

 'set yflip on'
*'set ylevs 1  2  3  4  5  6  7' 
*'set ylab 1 | 2| 3| 4| 5| 6| 7' 
'set gxout shaded' 

'yred2blue.gs'

'set clevs 2 4 6 8 10 12 14 16'
'set clevs  8 10 12 14 16 18 20 22'
'set ccols  0 29 27 25 23 21 33 35 37'

* to convert kg/(m2s) to mm/day 
*scale = 86400

* 'define xvar = ave(apcpsfc,lat=31.5,lat=37)'
* 'define xvar = ave(prec,lat=31.5,lat=37)'
* 'define xvar = ave(prec,lat=37.0,lat=43.0)'
  'define xvar = ave(precip,lat=37.0,lat=43.0)'
* 'define xvar = ave(data,lat=37.0,lat=43.0)'
  'd xvar' 

  'cbarv 'xr+0.14' 'yt-1.63' 0'

  'set strsiz 0.08 0.10'
  'set gxout contour' 
  'set clevs  8 10 12 14 16 18 20 22'
  'set clab off';'set cthick 1';'set ccolor 1';
  'd xvar'


* 'sdfopen /work/hydro/carrillo/2015/sum-pred/eof/reco-gph.mod35.nc' 
* gphfile = /work/hydro/carrillo/2015/sum-pred/eof/reco-gph.mod12.nc
* 'sdfopen /work/hydro/carrillo/2015/sum-pred/eof/reco-gph.mod12.nc' 

* 'set cint 1.0'; 'set cmin 2.0';'set cmax 9.0';
* 'set clevs 1 2 3 4 5 6 7 8'; 
* 'set clevs 2 4 6 8 10 12 14 16'

* 'set clevs 2 4 6 8 10 12 14 16'
* 'set ccols  0 29 27 25 23 21 33 35 37'

* 'jaecol.gs'
* 'set clevs -120 -100 -80 -60 -40 -20 20 40 60 80 100 120'
* 'set ccols  59 58 57 55 53 51 0 31 33 35 37 38 39'

* 'set cint 10' 
* 'set clab off';'set cthick 1';'set cstyle 1';'set ccolor 1';
* 'set clab off';'set cthick 1';'set ccolor 1';
* 'd 'scale'*smth9(smth9(tloop(ave(ave(pr,lat=30,lat=37.5),t-4,t+4))))'
* 'd ave(apcpsfc,lat=31.5,lat=37)'
* 'close 2'
* 'define xvar2 = ave(data,lat=35.0,lat=50.0)'
**  'define xvar2 = ave(data,lat=40.0,lat=50.0)'

** 'd xvar2' 
**  'cbarv 'xr+0.15' 'yt-2.34' 0'

* 'set cint 20' 
* 'set clevs -120 -100 -80 -60 -40 -20 20 40 60 80 100 120'
* 'set clab off';'set cthick 1';'set cstyle 1';'set ccolor 1';
* 'd 'scale'*smth9(smth9(tloop(ave(ave(pr,lat=30,lat=37.5),t-4,t+4))))'
* 'd ave(apcpsfc,lat=31.5,lat=37)'
* 'close 2'
* 'define xvar2 = ave(data,lat=35.0,lat=50.0)'

**  'd xvar2' 

* 'd xvar' 

* x-y label
 'q gxinfo';*the first parea changes when the plot is defined
   xline=sublin(result,3);xr2=subwrd(xline,6);xl2=subwrd(xline,4)
   xline=sublin(result,4);yt2=subwrd(xline,6);yb2=subwrd(xline,4)

   xlab=on
*  if(xlab=on);xvlf=-120;xvrg=-100;xint=5.0;
*  if(xlab=on);xvlf=258;xvrg=270;xint=2.0;
   if(xlab=on);xvlf=258;xvrg=270;xint=2;
             call=myxlabh(xl2,xr2,yt2,yb2,xvlf,xvrg,xint);endif
*  ylab=on
*  if(ylab=on);yvb=-40;yvt=60.0;yint=20.0;
*           call=myylabh(xl2,xr2,yt2,yb2,yvb,yvt,yint);endif

  'close 1'
  'sdfopen 'gphfile''
  'set lat 40'
  'set time 'initime'';'q dim';
   xline=sublin(result,5);itime=subwrd(xline,9);
  'set time 'endtime'';'q dim';
   xline=sublin(result,5);etime=subwrd(xline,9);
   say initime"("itime") "endtime"("etime") "
  'set t 'itime' 'etime''
  'set yflip on'


  'set gxout contour'
  'set clab off';'set cthick 1';'set ccolor 1';
*  pull stop

*  'set yflip on'
  'jaecol.gs'
* 'set clevs -120 -100 -80 -60 -40 -20 20 40 60 80 100 120'
* 'set ccols  59 58 57 55 53 51 0 31 33 35 37 38 39'
   'set xyrev on'
   'set lon 260' 
*  'set clab off';'set cthick 1';'set cstyle 1';'set ccolor 2';
*  'set cmark 0';'set cthick 8' 
   'set cmark 0';'set clab off';'set cthick 8';'set ccolor 34';
   'define xvar3 = ave(ave(data,lat=40.0,lat=50.0),lon=260,lon=280)'
*  'd xvar3' 
   'd 1.2*tloop(ave(xvar3,t-1,t+1))' 


   'set string 1 l 4'
   'set strsiz 0.10 0.14'
   'draw string 'xl+0.02' 'yt+0.12' 'xtitle''

   'set string 1 l 3'
   'set strsiz 0.10 0.15'
   'draw string 'xl+0.02' 'yb+0.12' 'xtitle2''

*----
   'myframe 1 1 2'
*--------------

   'q dim'
    xline=sublin(result,5);t1=subwrd(xline,11);tn=subwrd(xline,13)
    xline=sublin(result,5);time1=subwrd(xline,6);timen=subwrd(xline,8)

    say result 
    y1=t1;yn=tn

    r=1.0
    np = (tn - t1)/r + 1
    np = math_int(np)

    ry=1.0; npy=(yn-y1)/ry+1; npy=math_int(npy)
    dy = (yt-yb)/(np-1)
*   dx = (xr-xl)/(np-1)
*-------------------
*   plot CAPE-PW days 

    smo.1 ='Jan';smo.2='Feb';smo.3='Mar';smo.4 ='Apr';smo.5='May';smo.6='Jun';
    smo.7 ='Jul';smo.8='Aug';smo.9='Sep';smo.10 ='Oct';smo.11='Nov';smo.12='Dec';

file_in = "pw_cape_dys.2007.txt"
status = 0
while (status = 0)
record = read(file_in)
status = sublin(record,1)
if ( status  = 0)
    infolin  = sublin(record,2)
    stanum  = subwrd(infolin,3)
*
    fyr   = subwrd(infolin,1)
    fmo   = subwrd(infolin,2)
    fdy   = subwrd(infolin,3)
    fcape = subwrd(infolin,4)
    fpw   = subwrd(infolin,5)
    say  fyr"  "fmo"  "fdy

        initime=12Z%fdy%smo.fmo%fyr;
        say initime
       'set time 'initime'';'q dim';
        xline=sublin(result,5);itime=subwrd(xline,9);

        say initime":("itime")"

*       ylab.1=2000 
        ylab.1=itime
*   yy = yb + (yvar.ip-y1)*(yt-yb)/(yn-y1)
*   yy = yb + (ylab.1-y1)*(yt-yb)/(yn-y1)
    yy = yt - (ylab.1-y1)*(yt-yb)/(yn-y1)
 
    'set strsiz 0.10 0.12'
    'set string 1 l 3'
*   'draw string 'xl'  'yy-0.09' 'ylab.1''
*  
    'set line 1 1 2'
    'draw line 'xl' 'yy' 'xr' 'yy''
endif
endwhile 
*-------------------



**  'close 1'
*----
'enable print x.gm'
'print'
'disable print'
'!gxeps -c -i x.gm -o x.eps'
'!convert -density 200  x.eps 'pngfile''
'!mv x.eps 'epsfile''
*------------------
*------------------
*------------------
*------------------

function myxlabh(xl,xr,yt,yb,xvlf,xvrg,xint)
* xvlf=-120;xvrg=-20;xint=20 
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,2);x1=subwrd(xline,6);xn=subwrd(xline,8)
*
  nptos=(xvrg-xvlf)/xint + 1;
* say x1' 'xn 
*
  it=1;while(it<=nptos)
          xx.it = xvlf + (it-1)*xint 
*          say xx.it 
       it=it+1
       endwhile
* lab 
 it=1;while(it<=nptos)
        xreal=xx.it
        say xreal
        xabs=math_abs(xx.it)
        if (xreal<0);
           if (xreal>-180);
               xxlb.it=xabs%"`aO`nW";
           else;  xtmp = 360-xabs
               xxlb.it=xtmp%"`aO`nE";
           endif
        else
           if (xreal> 180);
               xtmp = 360-xabs
               xxlb.it=xtmp%"`aO`nW";
               if (xtmp = 0);
                  xxlb.it=xtmp%"`aO`n";
               endif
           else;
              xxlb.it=xabs%"`aO`nE";
           endif

        endif
       if(xreal=0|xreal=180);xxlb.it=xabs%"`aO`n";endif
*        xxlb.it=math_abs(xx.it) 
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say xxlb.it 
        it=it+1
       endwhile
*

*
  ip=1;while(ip<=nptos)
         xx = xx.ip
         xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         'set line 1 1 1'
         'draw line 'xs' 'yb' 'xs' 'yb-0.075''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
         y = yt
*        'draw line 'xs' 'yb' 'xs' 'yt''
         'set strsiz 0.080 0.10'
         'set string 1 c 3'
         'draw string 'xs'  'yb-0.175' 'xxlb.ip''
*         say xs' 'xx 
      ip=ip+1
      endwhile
return
*----------


*------------------------
function myylabh(xl,xr,yt,yb,yvb,yvt,yint)
* yvb=-40;yvt=40;yint=10;
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,3);y1=subwrd(xline,6);yn=subwrd(xline,8)
  nptos=(yvt-yvb)/yint + 1;
*
  it=1;while(it<=nptos)
          yy.it = yvb  + (it-1)*yint 
*          say yy.it 
       it=it+1
       endwhile
* lab 

  it=1;while(it<=nptos)
        yreal=yy.it
        yabs=math_abs(yy.it)
        if(yreal=0);yylb.it="EQ";endif
        if(yreal<0);yylb.it=yabs%"`aO`nS";endif
        if(yreal>0);yylb.it=yabs%"`aO`nN";endif
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say yylb.it 
        it=it+1
       endwhile
*
  ip=1;while(ip<=nptos)
         yy = yy.ip
*        xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         ys = yb+(yy-y1)*(yt-yb)/(yn-y1)
         'set line 1 1 1'
         'draw line 'xl-0.075' 'ys' 'xl' 'ys''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
*         'draw line 'xs' 'yb' 'xs' 'y''
*        'draw line 'xr' 'ys' 'xl' 'ys''
*        'set strsiz 0.12'
         'set strsiz 0.09 0.11'
         'set string 1 r 3'
*         'draw string 'xs'  'yb-0.2' 'xxlb.ip''
         'draw string 'xl-0.10'  'ys' 'yylb.ip''
      ip=ip+1
      endwhile
return
*----------------------------------------------



