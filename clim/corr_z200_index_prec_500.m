% 
% this version do not need a reference site (511) because it uses the dominant mode.  
% 
 clear all; 
%---------------------------------------- 
%---------------------------------------- 
% filespi = '/FoudreD0/data/ccctus/prog/interann/data/spi/spi_CRCM_cgcm3.01-1971.to.11-2000.ja.0.5x0.5.mat';
% yrini = 1895; yrend = 2010;
% filespi = '/FoudreD0/data/ldeo/spi.pnoaa.1895-2010.ja.mat';
% 
  yrini = 1870; yrend = 2010;
  yrini = 1871; yrend = 2010;
  yrini = 1871; yrend = 2009;
  yrini = 1993; yrend = 1993;
% filespi = '/home/carrillo/2015/Azar/prog/clim/wrf.R20C-R1.prec.1870-2010.ja.nc';
%  filespi = '/FoudreD0/data/sst/noaa/sst.1854-2010.jj.nc';
%  filespi = '/FoudreD0/data/sst/noaa/sst.1854-2010.ja.nc';
%  source from 20CR SST 
%  filespi = '/FoudreD0/data/rean20C/mo/air.sfc.rean20.ja.1871-2012.nc' 

   filespi = '/work/hydro/carrillo/DATA/prec/cpc_us/dy/precip.V1.0.1993.nc';

%---------------------------------------- 
%  pc1file = 'prec.SE-MX.1854-2013.na.AA.txt';
%  rfile = 'r.prec.SE-MX.prec.WRF-20CR.JA.nc';ttestf = 'ttest.prec.SE-MX.prec.WRF-20CR.JA.nc';
%  bootstraptxt = 'bookstrap.prec.SE-MX.prec.WRF-20CR.JA.txt';
%---------------------------------------- 
%  pc1file = 'prec.SE-MX.1854-2013.na.AA.txt';
%  rfile = 'r.prec.SE-MX.sst.JA.nc';ttestf = 'ttest.prec.SE-MX.sst.JA.nc';
%   bootstraptxt = 'bookstrap.prec.SE-MX.sst.JA.txt';

   pc1file = 'pr-tsrs/z200.narr.MW.1993.v2.txt';
   rfile = 'corrf/r.z200.index.prec.1993.nc';ttestf = 'corrf/ttest.z200.index.prec.1993.nc';
   bootstraptxt = 'corrf/bookstrap.z200.index.prec.1993.txt';

%---------------------------------------- 
% yrini = 1871;   yrend = 2012;
% filesst = '/FoudreD0/data/rean20C/mo/uqvq.chi.rean20.ja.1871-2012.nc';
%----------------------------------------------------------------------
% sitefile = 'cron-list.spi.v2.txt'; 
%----------------------------------------------------------------------
%[freqnum xtmp ytmp ztmp] = textread(freqfile,'%s %s %s %s');
%[x1 x2 x3 x4 x5 x6 x7 lat lon x10] = textread(sitefile,'%s %s %s %s %s %s %s %f %f %s');
%----------------------------------------------------------------------
%------------------------------
%  EOF/PC1 RECO MODE
%------------------------------
% activate to save 
  pc1x = load(pc1file); 
  reco_mode = pc1x(:,2); 
% reco_mode
  yrreco = pc1x(:,1); 

% idx_ini = find (yrreco == yrini);
% idx_end = find (yrreco == yrend);

% clear xdata
% xdata = reco_mode(idx_ini:idx_end); clear reco_mode; reco_mode = xdata;
% clear xtmp
% xtmp = yrreco(idx_ini:idx_end); clear yrreco; yrreco = xtmp;

%------------------------ 
%  figure;plot(xdata(:,1),reco_mode/nsta,'r',xdata(:,1),xdata(:,3),'b')
%  figure;plot(xdata(:,1),4.0*reco_mode/nsta,'r',xdata(:,1),xdata(:,3),'b')
% 
%------------------------------
%  DATA SPI  
%------------------------------
%  clear lat lon
%  load(filespi);
%  rdata  = dataspi;
%  yrs_data = time;

%  idx_ini = find (yrs_data == yrini);
%  idx_end = find (yrs_data == yrend);

%  clear xdata
%  xdata = rdata(:,:,idx_ini:idx_end); clear rdata; rdata = xdata;
%  clear xtmp
%  xtmp = yrs_data(idx_ini:idx_end); clear yrs_data; yrs_data = xtmp;
%--------------------------------------------
%  DATA SST  
%--------------------------------------------
% ncid = netcdf.open(filesst,'NC_NOWRITE');
  ncid = netcdf.open(filespi,'NC_NOWRITE');
% rdata = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'sst') );
% rdata = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'prec') );
  rdata = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'precip') );
% rdata = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'uqvqchi') );
  lat = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lat') );
  lon = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'lon') );
  time = netcdf.getVar(ncid, netcdf.inqVarID(ncid,'time') );
  time_unit = netcdf.getAtt(ncid, netcdf.inqVarID(ncid,'time'),'units' );

% data_fill = netcdf.getAtt(ncid,3,'_FillValue');
  netcdf.close(ncid);

% rdata (find(rdata == data_fill)) = NaN;


  ij=find(time_unit=='-');iniyr_s=time_unit(ij(1)-4:ij(1)-1);iniyr=str2num(iniyr_s);
   tlen = length(time);
   yrs_data = [0:tlen-1]+iniyr;

   idx_ini = find (yrs_data == yrini);
   idx_end = find (yrs_data == yrend);

   idx_ini = 152; 
   idx_end = 244; 

   clear xdata
   xdata = rdata(:,:,idx_ini:idx_end); clear rdata ; rdata = xdata;
   clear xtmp
   xtmp = yrs_data(idx_ini:idx_end); clear yrs_data ; yrs_data = xtmp;

%  rdatamean = mean(rdata,3);
% [nrow ncol ntime] = size(rdata);
%  for it=1:ntime
%    rdata(:,:,it)  = rdata(:,:,it) - rdatamean;
%  end
% 
% reduce domain 
% 
if(1==0)
%  mnlat=-20; mxlat=60;
%  mnlon=300; mxlon=358;  % -130 -> -60 

   mnlat=-20.0; mxlat=65;
   mnlon=  0.9375; mxlon= 359.0625;  % -130 -> -60 

% 
%  meth #1 
%  idwlat = floor( (mnlat - lat(1))/(lat(2)-lat(1)) + 1 );
%  iuplat = ceil ( (mxlat - lat(1))/(lat(2)-lat(1)) + 1 );

%  ilflon = floor( (mnlon - lon(1))/(lon(2)-lon(1)) + 1 );
%  irglon = ceil ( (mxlon - lon(1))/(lon(2)-lon(1)) + 1 );
%  meth #2 

   nlat = length(lat);
      xlen = 600000000.0; 
      for i=1:nlat
          if( xlen > abs(mnlat - lat(i)) ) 
              xlen =  abs(mnlat - lat(i)); idwlat = i; 
          end 
      end 
      xlen = 600000000.0; 
      for i=1:nlat
          if( xlen > abs(mxlat - lat(i)) ) 
              xlen =  abs(mxlat - lat(i)); iuplat = i; 
          end 
      end 
   nlon = length(lon);
      xlen = 600000000.0; 
      for i=1:nlon 
          if( xlen > abs(mnlon - lon(i)) ) 
              xlen =  abs(mnlon - lon(i)); ilflon = i; 
          end 
      end 
      xlen = 600000000.0; 
      for i=1:nlon 
          if( xlen > abs(mxlon - lon(i)) ) 
              xlen =  abs(mxlon - lon(i)); irglon = i; 
          end 
      end 

%-----------------------------


  [nrow ncol ntime ] = size(rdata);

   for it = 1:ntime
%     nwrdata(:,:,it) = rdata(idwlat:iuplat,ilflon:irglon,it);
      nwrdata(:,:,it) = rdata(ilflon:irglon,idwlat:iuplat,it);
   end
   nwlon = lon(ilflon:irglon);
   nwlat = lat(idwlat:iuplat);

  figure;contourf(lon,lat,rdata(:,:,1)')
  figure;contourf(nwlon,nwlat,nwrdata(:,:,1)')
  clear rdata lon lat;

  rdata = nwrdata;
  lon = nwlon;
  lat = nwlat;

end
% 
% correlation  and t-test 
% 
  [nrow ncol ntime] = size(rdata);
   r2d = NaN(nrow,ncol);
   rtest = NaN(nrow,ncol);


 for irow=1:nrow
     for icol=1:ncol
         y = squeeze(reco_mode)'; 
         x = squeeze(rdata(irow,icol,:))';
         if (length(x) == length( find(isnan(x)==1)))
             r2d(irow,icol)=NaN;
             rtest(irow,icol) = NaN;
         else

             xcov = cov([x' y']);covxy = xcov(2,1);
             varx = xcov(1,1);vary = xcov(2,2);

             a1 = covxy/varx;  % regression  coefficient 
             r = covxy/( sqrt(varx) * sqrt(vary) ) ;  % regression  coefficient 
             r2d(irow,icol)=r;
%              [rdw  rup]=corrtest(23,r,0.90,'twotails'); 
            [tcrit t]=corrttest(ntime,r,0.95,'twotails');
             rtest(irow,icol) = t;

%            corr1(irow,icol) = corr(x,y);
          end 
%        xx=squeeze(pc5); yy = squeeze(rdata(i,j,:));corr5(i,j) = corr(xx,yy);
     end
 end
   tcrit

  [nrow ncol ntime] = size(rdata);
   n_nan = length(find(isnan(rdata(:,:,1))==1)); 
   tsize = nrow*ncol-n_nan;

   orig_lenlocsig = length(find(rtest> tcrit | rtest < -1*tcrit));
   orig_percent = orig_lenlocsig * 100 / tsize;
% 
%------------field significance 
%

  [nrow ncol ntime] = size(rdata);
   n_nan = length(find(isnan(rdata(:,:,1))==1)); 
   tsize = nrow*ncol-n_nan;

if(1==0) 

for ifield=1:500
%for ifield=1:50
    ifield 
    idx = randperm(ntime); clear rrdata; 
    for it=1:ntime 
        rrdata(:,:,it) = rdata(:,:,idx(it)); 
    end 

 for irow=1:nrow
     for icol=1:ncol
         y = squeeze(reco_mode)'; 
%        x = squeeze(rdata(irow,icol,:))';
         x = squeeze(rrdata(irow,icol,:))';
         if (length(x) == length( find(isnan(x)==1)))
             r2d(irow,icol)=NaN;
             rtest(irow,icol) = NaN;
         else

             xcov = cov([x' y']);covxy = xcov(2,1);
             varx = xcov(1,1);vary = xcov(2,2);

             a1 = covxy/varx;  % regression  coefficient 
             r = covxy/( sqrt(varx) * sqrt(vary) ) ;  % regression  coefficient 
             r2d(irow,icol)=r;
%              [rdw  rup]=corrtest(23,r,0.90,'twotails'); 
            [tcrit t]=corrttest(ntime,r,0.95,'twotails');
             rtest(irow,icol) = t;

%            corr1(irow,icol) = corr(x,y);
          end 
%        xx=squeeze(pc5); yy = squeeze(rdata(i,j,:));corr5(i,j) = corr(xx,yy);
     end
 end
% r2d and rtest 
%  ifield = 1; 
%  [nrow ncol] = size(rtest);
%  tsize = nrow*ncol;
   lenlocsig = length(find(rtest> tcrit | rtest < -1*tcrit));
   percent(ifield) = lenlocsig * 100 / tsize;
   ngrids(ifield) = lenlocsig;
end 
% 
% plot histogram for field significant 
% 
%dx = 100;
 dx = 5;
%bins = [0:dx:40]; 
%bins = [0:dx:4000];
 bins = [0:dx:100];
 len = length(bins);
 for ilen=1:len-1
     xhist2(ilen) = length(find(percent>=bins(ilen) & percent < bins(ilen+1))); 
%    xhist2(ilen) = length(find(ngrids>=bins(ilen) & ngrids < bins(ilen+1)));
     bins_m(ilen) = (bins(ilen)+bins(ilen+1))/2;
 end
 xhist3 = xhist2;
%q90 = quantile(ngrids,0.90);
 q90 = quantile(percent,0.90);
%xper = 0.45; 
 xper = 0.80; 
%xper = 0.15; 
%xper = 0.09; 
 q90 = quantile(percent,xper);
 xhist3(find(bins_m < q90)) = NaN;

%figure(2); bar(bins_m,xhist,'c')
 figure; bar(bins_m,xhist2,'c')
 hold on  ; bar(bins_m,xhist3,'b')
     title('P: Null Distribution for %grids > 90% local significant local t-test','FontSize',13,'FontWeight','bold');

%    xlabel('percentage','FontSize',13); 
     xlabel('# of locally significant points','FontSize',13);
     ylabel('frequency','FontSize',13);
% save 

% bootstraptxt = 'bootstrap.band1.txt' 
 fid = fopen(bootstraptxt, 'w');
 fprintf(fid,'     t-orig  tcrit after n-realizations \n');
 fprintf(fid,'%9.4f %9.4f %9.4f ', orig_percent, q90, xper);
 fprintf(fid,'\n');
 fclose(fid);
%
end % field significant
%----------
%  tcrit 
% tcrit is the threshold for confident correlation at 90% level 
  wrnc2d(rfile,lat,lon,r2d);
  wrnc2d(ttestf,lat,lon,rtest);
  figure;contourf(lon,lat,r2d');
  figure;contourf(lon,lat,rtest');
%
%
%
%-----------------------
%  correlation SPI  
%-----------------------
%  clear lat lon
%  load(filespi);
%  rdataspi  = dataspi;
%  yrs_dataspi = time;

%  idx_ini = find (yrs_dataspi == yrini);
%  idx_end = find (yrs_dataspi == yrend);

%  clear xdata
%  xdata = rdataspi(:,:,idx_ini:idx_end); clear rdataspi ; rdataspi = xdata;
%  clear xtmp
%  xtmp = yrs_data(idx_ini:idx_end); clear yrs_dataspi ; yrs_dataspi = xtmp;

% [nrow ncol nt] = size(rdataspi);
%  r2dspi = NaN(nrow,ncol);
%  for irow=1:nrow
%    for icol=1:ncol
%        y = squeeze(reco_mode)';
%        x = squeeze(rdataspi(irow,icol,:))';
%        if (length(x) == length( find(isnan(x)==1)))
%            r2dspi(irow,icol)=NaN;
%        else
%            xcov = cov([x' y']);covxy = xcov(2,1);
%            varx = xcov(1,1);vary = xcov(2,2);

%            a1 = covxy/varx;  % regression  coefficient 
%            r = covxy/( sqrt(varx) * sqrt(vary) ) ;  % regression  coefficient 
%            r2dspi(irow,icol)=r;
%         end
%    end
%end
%wrnc2d(rspifile,lat,lon,r2dspi');
%figure;contourf(lon,lat,r2dspi); 
