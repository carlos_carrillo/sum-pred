%----------------% 
clear all;
%close all;
%----------------% 
%----------------% 
%----------------% 

 txtfile = 'prb.pr.jul93.txt';
%txtfile = 'prb.run.jul93.txt';
%txtfile = 'prb.soil.jul93.txt';
 txtfile = '/work/hydro/ksmith/POST/wet.dry.norm/meanMODIS/output_data/Mm.SP_DRY_final.txt' 
 xtmp = strcat('1993','-07-01 00:00:00');
%------------------------------------

 ncfile = strcat(txtfile(1:length(txtfile)-4),'.nc');
 ncfile = strcat('xyz.nc');
 time_ini = 'days since 1993-03-01 00:00:00';

  
 time_ini = ['days since ',xtmp];
%   xtmp = strcat(num2str(yrcase),'-01-01 00:00:00');

 xdata =load(txtfile);
 yrs = xdata(:,1);
 rdata = xdata(:,3);
%
 x=rdata;
 t=yrs;
 
 tunit = 'years  '
 syrini = num2str(t(1)); 
%time_ini = 'years since 1895-01-01 00:00:00';
%time_ini = 'years since 1896-01-01 00:00:00';
 ntime = length(t);
% xtime = [0:2:ntime-1];
 for i=1:ntime
    xtime(i) = 0 + (i-1)*1; 
 end 
 xvar=x;wrnc1d(ncfile,xtime,xvar,tunit,time_ini);

