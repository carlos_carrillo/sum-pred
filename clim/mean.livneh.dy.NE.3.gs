'reinit'
pathref="/FoudreD0/data/ccctus/prog/interann/prog/clim/var.iann.ja.gs" 
*----------------------* 
*title1a = 'VAR(P[JA])'; 
 title1a = 'VAR(P)'; 
*----------------------* 
*case=1;* JA  2002 Livneh
case=2;* JA  1999 Livneh 
*case=3;* JA  2010 Livneh 
*case=4;* JA  2012 Livneh 
*case=2;* 
*----------------------* 
*----------------------* 
* if(case=2) 
    ncfile =  '/lustre/work/hydro/carrillo/DATA/prec-temp/prec.livneh.1950-2013.dd.ctl' 
    ncfilePr = ' /home/hydro/carrillo/2015/sum-pred/clim/prb.pr.jul93.nc'
    ncfileRun = '/home/hydro/carrillo/2015/sum-pred/clim/prb.run.jul93.nc'
    ncfileSoil = '/home/hydro/carrillo/2015/sum-pred/clim/prb.soil.jul93.nc'

    epsfile = 'pr.mean.ano.livneh.93.ja.eps'
    pngfile = 'pr.mean.ano.livneh.93.ja.png'
    title1a = '`3D`0P(JA)'; 

    title2 = 'Livneh' 
    smo.1='Jan';smo.2='Feb';smo.3='Mar';smo.4='Apr';smo.5='May';smo.6='Jun';
    smo.7='Jul';smo.8='Aug';smo.9='Sep';smo.10='Oct';smo.11='Nov';smo.12='Dec';

    mo = 6;
    mo = 7;

    yr = 1993

*  'open 'ncfile''

*  'sdfopen  'ncfilePr''
*  'sdfopen  'ncfileRun''
*  'sdfopen  'ncfileSoil''

   'q gxinfo'
    say result
    xline=sublin(result,3);xmax=subwrd(xline,6);xmin=subwrd(xline,4)
    xline=sublin(result,4);ymax=subwrd(xline,6);ymin=subwrd(xline,4)
    dx=(xmax-xmin)/2;dy=(ymax-ymin)/2

    xl=xmin+0.7;xr=xl+dx*1.65;
    yt=ymax-0.1;yb=yt-dy*0.74;

    xl2=xmin+0.7;xr2=xl2+dx*1.65;
    yt2=yb;yb2=yt2-dy*0.74;


*  'set parea 'xl' 'xr' 'yb' 'yt''

    dd=1; ddn=30; 
    dd=1; ddn=1; 
    dd=1; ddn=31; 
    dd=21; ddn=21; 
*   dd=1; ddn=1; 
    while (dd<=ddn) 
      'c'
      'open 'ncfile''
   'set parea 'xl' 'xr' 'yb' 'yt''
*   initime = 00Z01Jun1993; 
    initime = '00Z'dd%smo.mo%yr;

    pngfile = 'pr.mean.livneh.'yr'.'mo'.'dd'.dy.gif'
    title1a = '`0Precipitation ('smo.mo' 'dd', 'yr')  ';
    title1b = '   ';

*endif 
*--------------------------------------------------------------------------------* 
*--------------------------------------------------------------------------------* 

'set grads off'
'set frame off'
'set xlab off'
'set ylab off'
*---------------* 
*'set lat 37 44'
*'set lon -105 -85'
* 
'set lat 38 45'
'set lon -110.0 -90.0'

*   var   * 
*ntime=12*116
ntime=140
*ntime=12*20
*ntime=12*5
 scale = 1.0/30;* mm/day
 scale = 1.0;* mm/day
**'define xtmp = 'ntime'*xvar/('ntime-1')'
 'set time 'initime'';'q dim';
  xline=sublin(result,5);itime=subwrd(xline,9);

*'define xvar = smth9(smth9(smth9('scale' * prec(t='itime') )))'
'define xvar = ((('scale' * prec(t='itime') )))'

'set gxout shaded'
'set grid off'

'set ylopts 1 3 0.15'
'set xlopts 1 3 0.15'


'yred2blue.gs'
*'set clevs 2 3 4 5 6 7 8 9 10'
'set clevs 2 4 6 8 10 12 14 16 18'
'set ccols  0 29 28 27 26 25 24 23 22 21'


**'d xvar'
*'cbarh.2 'xr-1.80' 'yb-0.24' 0'
'set strsiz 0.11 0.12'

*'set strsiz 0.07 0.09'
'set strsiz 0.16 0.18'
'set string 1 l 2 0'
*'draw string 'xr-1.64' 'yt-0.450' 10`a-2`n(mm/day)`a2`n'
*'draw string 'xr-2.42' 'yt-0.430' [mm/day]'
*'draw string 'xr-1.30' 'yt-0.390' [mm/day]'
*'set string 1 l 2 0'
*----------
*
'set gxout contour'
*'set clevs 2 3 4 5 6 7 8 9 10'
'set clevs 2 4 6 8 10 12 14 16 18'
'set ccolor 1'
'set clab off'
'set cthick 1'
*'d xvar'
*

*'basemap O 0 1 L' 

'jaecol.gs' 
'set line 39 1 6'
*'set line 35'

*'draw shp PlatteRiverBasin'


*'draw map'
'set strsiz 0.16 0.18'
'set string 1 l 5'
*'draw string 'xl+0.0' 'yt-0.40' 'title1a''
'set strsiz 0.16 0.18'
*'draw string 'xl+0.0' 'yt-0.68' 'title1b''
*'draw string 'xl+0.0' 'yb+0.86' 'title1b''



'set strsiz 0.16 0.18'
'set string 1 l 4'
*'draw string 'xl+0.02' 'yb+0.68' 'title2''


*'myframe 1 1 2'

* x-y label 
 'q gxinfo';*the first parea changes when the plot is defined
   xline=sublin(result,3);xr2=subwrd(xline,6);xl2=subwrd(xline,4)
   xline=sublin(result,4);yt2=subwrd(xline,6);yb2=subwrd(xline,4)

xlab=on
ylab=on
*if(xlab=on);xvlf=0;xvrg=180;xint=60;
*            call=myxlabh(xl2,xr2,yt2,yb2,xvlf,xvrg,xint);endif
*if(xlab=on);xvlf=-110;xvrg=-90;xint=2;
*            call=myxlabh(xl2,xr2,yt2,yb2,xvlf,xvrg,xint);endif
*if(ylab=on);yvb=38.0;yvt=44.0;yint=2.0;
*            call=myylabh(xl2,xr2,yt2,yb2,yvb,yvt,yint);endif
* 
*------------------------

'set mpdraw on'
*'set mpdset mres'
'set map 1 1 2'
'set mpdset hires'
*'set mpdset hires'
*'draw map'

'set mpdset lowres'
'set map 1 1 5'
*'draw map'

*-----------
'set strsiz 0.10 0.12'
'set string 1 l'
*'draw string 0.5 2.40 'pathref''
*------------------------* 
*     'enable print x.gm'
*     'print'
*     'disable print'
*     '!gxps -c -i x.gm -o x.eps'
*     '!convert -density 200 x.eps 'pngfile''
*----SOIL MOISTURE-----------------
   xdw = 4.70 
  'close 1'
* 'set parea 'xl' 'xr' 'yb-xdw 4.5' 'yt-4.5''
  'set parea 'xl' 'xr' 'yb-xdw' 'yt-xdw''
  'set ylab on'
  'sdfopen prb.soil.jul93.nc'
  'set t 1 31';
   v1=140.0;vn = 180.0;'set vrange 'v1' 'vn''
*
  'set grads off';
* 'set grid on 3 1'
  'set grid off'
  'set frame off'
* 'set xlab off'
  'set xlab on'
  'set ylopts 1 2 0.14 0.17'
* 'set xlopts 1 2 0.11 0.12'
*
  'set ccolor 0';'set cmark 0';'set cthick 8'
  'set ccolor 1';'set cmark 0';'set cthick 8'
  'd data'
* 'set gxout linefill' 
* 'set lfcols 8 4'
* 'd data;const(data,0,-a)'
* 'set gxout contour' 
*-------------
*---RUNOFF------------------
  'close 1'
* 'set parea 'xl' 'xr' 'yb-4.5' 'yt-4.5''
* 'set parea 'xl' 'xr' 'yb-xdw' 'yt-xdw''
  'sdfopen prb.run.jul93.nc'
  'set grid off'
  'set t 1 31';
   v1=0.0;vn = 1.5;'set vrange 'v1' 'vn''
  'set gxout contour' 
*
  'set grads off';
* 'set grid on 3 1'
  'set grid off'
  'set frame off'
* 'set xlab off'
  'set xlab on'
  'set ylpos 0.0 r'
  'set ylopts 2 2 0.14 0.17'
* 'set xlopts 1 2 0.11 0.12'
*
* 'set ccolor 0';'set cmark 0';'set cthick 8'
  'set ccolor 2';'set cmark 0';'set cthick 8'
* 
* 'set gxout linefill' 
* 'set lfcols 2 4'
  'd data'
* 'd data;const(data,0,-a)'
* 'set gxout contour' 
*------PRECP-------------------* 
  'close 1'
   ybb=yb-4.5 
   ytt=yt-4.5 
*  ybb=yb-xdw
*  ytt=yt-xdw

* 'set parea 'xl' 'xr' 'yb-4.5' 'yt-4.5''
* 'set parea 'xl' 'xr' 'ybb' 'ytt''
  'sdfopen prb.pr.jul93.nc'
  'set grid off'
  'set t 1 31'
   v1=0.0;vn = 15.0;
  'set vrange 'v1' 'vn''
*
  'set gxout contour' 
  'set grads off'
* 'set grid on 3 1'
  'set grid off'
  'set frame off'
* 'set xlab off'
  'set xlab on'
  'set ylab on'
  'set yflip on'
  'set ylpos 0.45 r'
* 'set ylab off'

  'set ylopts 5 2 0.14 0.17'
* 'set xlopts 1 2 0.11 0.12'
*
  'set ccolor 0';'set cmark 0';'set cthick 8'
* 'set ccolor 1';'set cmark 0';'set cthick 8'
* 
   scale = 1
  'd data'
   scale = 1

   ybb=yb-4.7 
   ytt=yt-4.7 
  rc=histo(xl,xr,ytt,ybb,xarea,v1,vn,scale,dd)

  'myframe 1 1 2'

  'set strsiz 0.16 0.18'
  'set string 1 l 3'
  'draw string 'xl'  'ytt+0.41' Platte River Basin'

  'set strsiz 0.15 0.16'
  'set string 5 l 3'
  'draw string 'xl'  'ytt+0.14' Precipitation [mm],'

  'set strsiz 0.15 0.16'
  'set string 2 l 3'
  'draw string 'xl+2.4'  'ytt+0.14' Runoff [mm],'

  'set strsiz 0.15 0.16'
  'set string 1 l 3'
  'draw string 'xl+4.1'  'ytt+0.14' Soil moisture [mm]'

*-------------------------* 
      'enable print x.gm'
      'print'
      'disable print'
      '!gxps -c -i x.gm -o x.eps'
      '!convert -density 200 x.eps 'pngfile''
       pull  stop 
       dd=dd+1
       'close 1'
       'reset' 
    endwhile 
*---------------------------
*---------------------------
*---------------------------
function myxlabh(xl,xr,yt,yb,xvlf,xvrg,xint)
* xvlf=-120;xvrg=-20;xint=20 
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,2);x1=subwrd(xline,6);xn=subwrd(xline,8)
*
  nptos=(xvrg-xvlf)/xint + 1;
*
  it=1;while(it<=nptos)
          xx.it = xvlf + (it-1)*xint 
*          say xx.it 
       it=it+1
       endwhile
* lab 
  it=1;while(it<=nptos)
        xreal=xx.it
        xabs=math_abs(xx.it)
        say xreal 
        if (xreal<0);
           if (xreal>-180);
               xxlb.it=xabs%"`aO`nW";
           else; 
                xtmp = 360-xabs
                xxlb.it=xtmp%"`aO`nE";
           endif
        else
           if(xreal<=180) 
            xxlb.it=xabs%"`aO`nE";
           else 
              xtmp = 360-xabs
              xxlb.it=xtmp%"`aO`nW";
           endif 
           if(xreal=0|xreal=360);xxlb.it="0`aO`n";endif

        endif
        if(xreal=0|xreal=180);xxlb.it=xabs%"`aO`n";endif
*        xxlb.it=math_abs(xx.it) 
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say xxlb.it 
        it=it+1
       endwhile
*

*
  ip=1;while(ip<=nptos)
         xx = xx.ip
         xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         'set line 1 1 1'
         'draw line 'xs' 'yb' 'xs' 'yb-0.075''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
         y = yt
*        'draw line 'xs' 'yb' 'xs' 'yt''
*        'set strsiz 0.14'
         'set strsiz 0.14 0.15'
         'set string 1 c 3'
         'draw string 'xs'  'yb-0.175' 'xxlb.ip''
      ip=ip+1
      endwhile
return
*------------------------
function myylabh(xl,xr,yt,yb,yvb,yvt,yint)
* yvb=-40;yvt=40;yint=10;
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,3);y1=subwrd(xline,6);yn=subwrd(xline,8)
  nptos=(yvt-yvb)/yint + 1;
*
  it=1;while(it<=nptos)
          yy.it = yvb  + (it-1)*yint 
*          say yy.it 
       it=it+1
       endwhile
* lab 

  it=1;while(it<=nptos)
        yreal=yy.it
        yabs=math_abs(yy.it)
        if(yreal=0);yylb.it="EQ";endif
        if(yreal<0);yylb.it=yabs%"`aO`nS";endif
        if(yreal>0);yylb.it=yabs%"`aO`nN";endif
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say yylb.it 
        it=it+1
       endwhile
*
  ip=1;while(ip<=nptos)
         yy = yy.ip
*        xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         ys = yb+(yy-y1)*(yt-yb)/(yn-y1)
         'set line 1 1 1'
         'draw line 'xl-0.075' 'ys' 'xl' 'ys''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
*         'draw line 'xs' 'yb' 'xs' 'y''
*        'draw line 'xr' 'ys' 'xl' 'ys''
*        'set strsiz 0.14'
*        'set strsiz 0.08 0.09'
         'set strsiz 0.14 0.15'
         'set string 1 r 3'
*         'draw string 'xs'  'yb-0.2' 'xxlb.ip''
         'draw string 'xl-0.10'  'ys' 'yylb.ip''
      ip=ip+1
      endwhile
return
*----------------------------------------------
function histo(xl,xr,yt,yb,xarea,v1,vn,scale,day)
  'q dim'
  xline=sublin(result,5);t1=subwrd(xline,11);tn=subwrd(xline,13)
  xline=sublin(result,5);time1=subwrd(xline,6);timen=subwrd(xline,8)
*
*  say t1' 'tn
*  pull stop
  x1=t1;xn=tn
  y1=0;yn=3.0
  y1=v1;yn=vn
* v1=0.0;vn = 3.0;'set vrange 'v1' 'vn''
*
  r=1.0
  np = (tn - t1)/r + 1
  np = math_int(np)

  ry=1.0; npy=(yn-y1)/ry+1; npy=math_int(npy)
  dy = (yt-yb)/(np-1)
  dx = (xr-xl)/(np-1)
**areas**
*  xarea=1
   if(xarea=1);
      latdw=15;latup=22.5;lonlf=-105;lonrg=-90;endif
   if(xarea=2);
      latdw=37.0;latup=43.0;lonlf=-102;lonrg=-90.0;endif
   if(xarea=3);
      latdw=20;latup=30;lonlf=-110;lonrg=-102.5;endif
   if(xarea=4);
      latdw=32.5;latup=42.5;lonlf=-97.5;lonrg=-87.5;endif
*
* y-values / ylab
* scale = 86400;
* scale = 1;
  it=1;while(it<=np)
      xtime = t1+(it-1)*r
      'set t 'xtime''
*     'd ave(ave(data,lat=5,lat=15),lon=-105,lon=-95)'
*     'd ave( ave(prec,lat='latdw',lat='latup'), lon='lonlf',lon='lonrg')/30'
*     'd ave( ave('scale'*data,lat='latdw',lat='latup'), lon='lonlf',lon='lonrg')/30'
*     'd ave( ave('scale'*data,lat='latdw',lat='latup'), lon='lonlf',lon='lonrg')/1.0'

*     'd ave( ave('scale'*prec,lat='latdw',lat='latup'), lon='lonlf',lon='lonrg')/1.0'
*     'd ave( ave( 'scale' *prec,lat='latdw',lat='latup'), lon='lonlf',lon='lonrg')'
      'd 'scale'*data'

*      say result
*      get end of line
       len=0;ilen=1;while(len=0)
                      xlen=sublin(result,ilen);xxlen=subwrd(xlen,1);
*                     say xxlen
                      if(xxlen='Result');len=1;endif
                      ilen=ilen+1;
*                     pull stop
                   endwhile
       len=ilen-1;
*      say len
*      pull stop
*      xline=sublin(result,17);
       xline=sublin(result,len);
       yvar.it=subwrd(xline,4);
       say yvar.it
*      pull stop
      it=it+1
      endwhile

* x-labels
  it=1;while(it<=np)
      xtime = t1+(it-1)*r
      'set t 'xtime''
      'q dim'; xline=sublin(result,5);xwrd=subwrd(xline,6)
      dd=substr(xwrd,4,2);dd=dd+0
      mon=substr(xwrd,6,3);
*
      if(mon=JAN);mon=1;endif;if(mon=FEB);mon=2;endif;
      if(mon=MAR);mon=3;endif;if(mon=APR);mon=4;endif;
      if(mon=MAY);mon=5;endif;if(mon=JUN);mon=6;endif;
      if(mon=JUL);mon=7;endif;if(mon=AUG);mon=8;endif;
      if(mon=SEP);mon=9;endif;if(mon=OCT);mon=10;endif;
      if(mon=NOV);mon=11;endif;if(mon=DEC);mon=12;endif;
*
      say mon
      if(mon=1);smon=Jan;endif;if(mon=2);smon=Feb;endif;
      if(mon=3);smon=Mar;endif;if(mon=4);smon=Apr;endif;
      if(mon=5);smon=May;endif;if(mon=6);smon=Jun;endif;
      if(mon=7);smon=Jul;endif;if(mon=8);smon=Aug;endif;
      if(mon=9);smon=Sep;endif;if(mon=10);smon=Oct;endif;
      if(mon=11);smon=Nov;endif;if(mon=12);smon=Dec;endif;
*
*     xlab.it= mon%'/'%dd
      xlab.it= smon
*     say xlab.it
*     pull stop
      it=it+1
      endwhile



*
* y-labels
*
  it=1;while(it<=npy)
           xtmp = y1+(it-1)*ry
           ylab.it= xtmp
           yy = yb + (xtmp-y1)*(yt-yb)/(yn-y1)
          'set strsiz 0.14';'set string 1 r 2'
*         'draw string 'xl-0.3'  'yy' 'ylab.it''
*         'draw line 'xl-0.2' 'yy' 'xl' 'yy''
           it=it+1
       endwhile
*---------------------------*
  ip=1;while(ip<=np)
      nn=ip
      nnp=ip-1
      xx=x1+(nn-1)*dx
      xx=x1+(nn-2)*dx
*     xx=x1+(nn-3)*dx
*     xx=x1+(nn-4)*dx
*
     'set line 1 1 1'
      yy = yb + (yvar.ip-y1)*(yt-yb)/(yn-y1)
*    'draw line 'xx' 'yb' 'xx' 'yy''
      dxrec=0.07
      dxrec=0.10
      dxrec=0.075
      dxrec=0.078
*     dxrec=0.040
     'jaecol'
*    'set line 79'
     'set line 5'
*here
**    'draw recf  'xx-dxrec' 'yb' 'xx+dxrec' 'yy''
*    'draw recf   'xx-2*dxrec' 'yb' 'xx+0*dxrec' 'yy''

*    'draw recf   'xx-2*dxrec' 'yb' 'xx+0*dxrec' 'yy''

      yyrev = yt-(yy-yb) 
*    'draw recf   'xx-2*dxrec' 'yb' 'xx+0*dxrec' 'yy''
*     say ip' 'day
*     pull stop 
      if (ip=day) 
       'set line 4'
       'draw recf   'xx-2*dxrec' 'yyrev' 'xx+0*dxrec' 'yt''
      else
       'draw recf   'xx-2*dxrec' 'yyrev' 'xx+0*dxrec' 'yt''
      endif 

*    'draw rec   'xx-2*dxrec' 'yb' 'xx+0*dxrec' 'yy''
     'set ccolor 1'

*   x-label
      'set strsiz 0.15'
      'set string 1 c 3'
*     'draw string 'xx'  'yb-0.25' 'xlab.ip''
*--------------------------------------------*

*--------------------------------------------*
** lines
       if(math_mod(nnp,12) = 0 & nnp < 15)
         'set line 1 3 1'
**         'draw line 'xl' 'yy' 'xr' 'yy''
       endif
       if(math_mod(nnp,30) = 0 & nnp < 37)
         'set line 1 3 1'
**       'draw line 'xl' 'yy' 'xr' 'yy''
       endif
*
      ip=ip+1
      endwhile
return
****

