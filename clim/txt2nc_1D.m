%----------------% 
clear all;
%close all;
%----------------% 
%----------------% 
%----------------% 

 txtfile = 'fmt.pnoaa.20-50.2.txt'
 ncfile = 'fmt.pnoaa.20-50.2.nc'
 time_ini = 'years since 1895-01-01 00:00:00';

 txtfile = 'fmt.chi.20-50.2.txt' 
 ncfile = 'fmt.chi.20-50.2.nc' 
 time_ini = 'years since 1871-01-01 00:00:00';
%----------------%

 txtfile = 'pr-tsrs/prec.olam.MW.R1F0.I8E1.93.txt'
 ncfile = 'pr-tsrs/prec.olam.MW.R1F0.I8E1.93.nc'
time_ini = 'days since 1993-07-01 00:00:00';

%txtfile = 'pr-tsrs/prec.olam.MW.R1F1.I10E1.93.txt'
%ncfile = 'pr-tsrs/prec.olam.MW.R1F1.I10E1.93.nc'
%time_ini = 'days since 1993-07-01 00:00:00';

%txtfile = 'pr-tsrs/prec.olam.MW.R2F1.I1E1.93.txt'
%ncfile = 'pr-tsrs/prec.olam.MW.R2F1.I1E1.93.nc'
%time_ini = 'days since 1993-07-21 00:00:00';

%txtfile = 'pr-tsrs/prec.olam.MW.R2F1.I2E1.93.txt'
%ncfile = 'pr-tsrs/prec.olam.MW.R2F1.I2E1.93.nc'
%time_ini = 'days since 1993-07-20 00:00:00';

%txtfile = 'pr-tsrs/prec.olam.MW.R2F1.I3E1.93.txt'
%ncfile = 'pr-tsrs/prec.olam.MW.R2F1.I3E1.93.nc'
%time_ini = 'days since 1993-07-17 00:00:00';

%txtfile = 'pr-tsrs/prec.olam.MW.R2F1.I4E1.93.txt'
%ncfile = 'pr-tsrs/prec.olam.MW.R2F1.I4E1.93.nc'
%time_ini = 'days since 1993-07-15 00:00:00';

%txtfile = 'pr-tsrs/prec.olam.MW.R2F1.I5E1.93.txt'
%ncfile = 'pr-tsrs/prec.olam.MW.R2F1.I5E1.93.nc'
%time_ini = 'days since 1993-07-12 00:00:00';

%txtfile = 'pr-tsrs/prec.olam.MW.R2F1.I6E1.93.txt'
%ncfile = 'pr-tsrs/prec.olam.MW.R2F1.I6E1.93.nc'
%time_ini = 'days since 1993-07-10 00:00:00';

%txtfile = 'pr-tsrs/prec.olam.MW.R2F1.I7E1.93.txt'
%ncfile = 'pr-tsrs/prec.olam.MW.R2F1.I7E1.93.nc'
%time_ini = 'days since 1993-07-07 00:00:00';

%txtfile = 'pr-tsrs/prec.olam.MW.R2F1.I8E1.93.txt'
%ncfile = 'pr-tsrs/prec.olam.MW.R2F1.I8E1.93.nc'
%time_ini = 'days since 1993-07-05 00:00:00';

%txtfile = 'pr-tsrs/prec.olam.MW.R2F1.I9E1.93.txt'
%ncfile = 'pr-tsrs/prec.olam.MW.R2F1.I9E1.93.nc'
%time_ini = 'days since 1993-07-03 00:00:00';

 txtfile = 'pr-tsrs/prec.olam.MW.cpc.93.txt'
 ncfile = 'pr-tsrs/prec.olam.MW.cpc.93.nc'
 time_ini = 'days since 1993-03-01 00:00:00';

%------------------------------------

 txtfile='pr-tsrs/prec.cfs.MW.1993060512.txt';xtmp = strcat('1993','-06-06 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993061012.txt';xtmp = strcat('1993','-06-11 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993061512.txt';xtmp = strcat('1993','-06-16 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993062012.txt';xtmp = strcat('1993','-06-21 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993062512.txt';xtmp = strcat('1993','-06-26 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993063012.txt';xtmp = strcat('1993','-07-01 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993070512.txt';xtmp = strcat('1993','-07-06 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993071012.txt';xtmp = strcat('1993','-07-11 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993071512.txt';xtmp = strcat('1993','-07-16 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993072012.txt';xtmp = strcat('1993','-07-21 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993072512.txt';xtmp = strcat('1993','-07-26 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993073012.txt';xtmp = strcat('1993','-07-31 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993080412.txt';xtmp = strcat('1993','-08-05 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993080912.txt';xtmp = strcat('1993','-08-10 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993081412.txt';xtmp = strcat('1993','-08-15 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993081912.txt';xtmp = strcat('1993','-08-20 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993082412.txt';xtmp = strcat('1993','-08-25 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993082912.txt';xtmp = strcat('1993','-08-30 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993090312.txt';xtmp = strcat('1993','-09-04 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993090812.txt';xtmp = strcat('1993','-09-09 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993091312.txt';xtmp = strcat('1993','-09-14 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993091812.txt';xtmp = strcat('1993','-09-19 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993092312.txt';xtmp = strcat('1993','-09-24 00:00:00');
%txtfile='pr-tsrs/prec.cfs.MW.1993092812.txt';xtmp = strcat('1993','-09-29 00:00:00');


txtfile='pr-tsrs/prec.cfs.MW.1993070512.2.txt';xtmp = strcat('1993','-07-06 00:00:00');
txtfile='pr-tsrs/prec.cfs.MW.1993071012.2.txt';xtmp = strcat('1993','-07-11 00:00:00');



 ncfile = strcat(txtfile(1:length(txtfile)-4),'.nc');
 time_ini = 'days since 1993-03-01 00:00:00';

  
  time_ini = ['days since ',xtmp];
%   xtmp = strcat(num2str(yrcase),'-01-01 00:00:00');

xdata =load(txtfile);
yrs = xdata(:,1);
rdata = xdata(:,2);
%
 x=rdata;
 t=yrs;
 
 tunit = 'years  '
 syrini = num2str(t(1)); 
%time_ini = 'years since 1895-01-01 00:00:00';
%time_ini = 'years since 1896-01-01 00:00:00';
 ntime = length(t);
% xtime = [0:2:ntime-1];
 for i=1:ntime
    xtime(i) = 0 + (i-1)*1; 
 end 
 xvar=x;wrnc1d(ncfile,xtime,xvar,tunit,time_ini);

