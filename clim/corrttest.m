%-----------------------------------------------% 
function [tcrit t] = corrttest(N,r,level,tail) 
%-----------------------------------------------% 
%N = 21; 
%r=0.8; 
%level= 0.95; 
%tail = 'twotails'; 
%tail = 'onetails'; 
%--------------------------% 
if(tail == 'twotails')
   ttail = level + (1-level)/2;  
end 
if(tail == 'onetails')
   ttail = level;
end 

%Z = 0.5 * log ( (1+r)/(1-r) ); 

t = r * sqrt(N-2) / sqrt(1-r^2);

tcrit = icdf('t',ttail,N-2); 


%uz_up = Z+znor*stdz; 
%uz_dw = Z-znor*stdz; 

% convert to true correlation 

%phoup = (exp(2*uz_up) -1 ) /(exp(2*uz_up)+1 );
%phodw = (exp(2*uz_dw) -1 ) /(exp(2*uz_dw)+1 );

%pho_up 
%pho_dw





