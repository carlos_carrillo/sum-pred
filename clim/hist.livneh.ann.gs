
'reinit'

  xarea  = 1
  xarea  = 2;* Only valid for area 2 
* xarea  = 3
* xarea  = 4

*---------------------------------------* 
 'q gxinfo'
  say result
  xline=sublin(result,3);xmax=subwrd(xline,6);xmin=subwrd(xline,4)
  xline=sublin(result,4);ymax=subwrd(xline,6);ymin=subwrd(xline,4)
  dx=(xmax-xmin)/2;dy=(ymax-ymin)/2

  xl=xmin+0.7;xr=xl+dx*0.62;
  yt=ymax-0.5;yb=yt-dy*0.80;

 'set parea 'xl' 'xr' 'yb' 'yt''

*---------------------------------------* 

*fobs = '/work/hydro/carrillo/DATA/prec-temp/prec.livneh.1950-2013.ann.ctl' 
*fobs = '/work/hydro/carrillo/DATA/prec/ldeo/prec.ldeo.1895-2010.ann.ctl'

fobs = '/work/hydro/carrillo/DATA/prec-temp/prec.livneh.1950-2013.ann.ctl'

*--------obs histogram --
if(1=1)
* 'sdfopen 'fobs''
  'open 'fobs''
   v1=0.0;vn = 3.0;'set vrange 'v1' 'vn''
   v1=0.0;vn = 10.0;'set vrange 'v1' 'vn''


   if(xarea=2);v1=0.0;vn = 2.0;endif
   if(xarea=1);v1=0.0;vn = 8.0;endif 
   if(xarea=3);v1=0.0;vn = 8.0;endif 
   if(xarea=4);v1=0.0;vn = 8.0;endif 
   v1=0.0;vn = 4.0;
   'set vrange 'v1' 'vn''

  'set grads off'
  'set grid on 3 1'
  'set frame off'
* 'set xlab off'
  'set xlab on'
  'set ylopts 1 2 0.12 0.12'
  'set xlopts 1 2 0.11 0.12'

* 'set xlabs Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec'
  'set xlabs J|F|M|A|M|J|J|A|S|O|N|D'

  'set lat 37'
  'set lon -103'
  'set t 1 12'
  'set ccolor 0';'set cmark 0';'set cthick 8'
* 'set ccolor 1';'set cmark 0';'set cthick 8'
   scale = 86400 
   scale = 1/30 
   scale = 1 
* 'd ave( ave(prec,lat=30,lat=37.5), lon=-115,lon=-107.5)/30'
* 'd ave( ave('scale'*data,lat=30,lat=37.5), lon=-115,lon=-107.5)/30'

* 'd ave( ave('scale'*prec,lat=37.5,lat=45), lon=-103,lon=-90)'
   if(xarea=2); 
      latdw=37.0;latup=43.0;lonlf=-102;lonrg=-90.0;endif 
* 'd prec'
* 'd ave( ave('scale'*prec,lat='latdw',lat='latup'), lon='lonlf',lon='lonrg')/1.0'
  'd 'scale' * ave( ave(prec,lat='latdw',lat='latup'), lon='lonlf',lon='lonrg')'
   rc=histo(xl,xr,yt,yb,xarea,v1,vn,scale)

*'set xlab on'
*'set ylab on'

*'set ylopts 1 2 0.12 0.12'
*'set xlopts 1 2 0.11 0.12'


'close 1'
endif
*pull stop 
*---- histograma ---------
*-------------------
*-------------------
*-------------------
*xtitle = '[P]: NOAA           [mm/day]'
*xtitle = 'P[A2]: P-NOAA   (1985-2010)' 
xtitle2 = '(1971-2000)'
xtitle2 = ''

if(xarea=1); 
   xtitle = 'P[A1]: WRF-20CR  (1871-2010)' 
   xtitle3 = 'Area 1: [15`aO`n-22.5`aO`nN;105`aO`n-90`aO`nW]'; endif 
if(xarea=2); 
   xtitle = 'P[MW]: Livneh  (1950-2013)' 
   xtitle3 = 'Area MW: [37`aO`n-40`aO`nN;102`aO`n-90`aO`nW]'; endif 
if(xarea=3); 
   xtitle = 'P[A3]: R20C    (1871-2010)' 
   xtitle3 = 'Area 3: [20`aO`n-30`aO`nN;110`aO`n-102.5`aO`nW]'; endif 
if(xarea=4); 
   xtitle = 'P[A4]: R20C    (1871-2010)' 
   xtitle3 = 'Area 4: [32.5`aO`n-42.5`aO`nN;97.5`aO`n-87.5`aO`nW]'; endif 

'set string 1 l 5';'set strsiz 0.115 0.13'
'draw string 'xl' 'yt+0.15' 'xtitle''
'draw string 'xl' 'yt-0.15' 'xtitle2''
'set strsiz 0.09510 0.11'
'draw string 'xl' 'yt-0.15' 'xtitle3''

'set string 1 l 5 90';
'set strsiz 0.11 0.10'
'draw string 'xl-0.44' 'yt-2.60' [mm/day]'
'set string 1 l 5 0';


*****

*----------
'myframe 1 1 2'
*----------
*'mkgm x.gm'
epsfile = 'ann.livneh.A'xarea'.eps' 
pngfile = 'ann.livneh.A'xarea'.png' 

'enable print x.gm'
'print'
'disable print'
'!gxeps -c -i x.gm -o x.eps'
'!convert -density 200 x.eps 'pngfile''
'!mv x.eps 'epsfile''

*-----------
*----------------
function histo(xl,xr,yt,yb,xarea,v1,vn,scale)
  'q dim'
  xline=sublin(result,5);t1=subwrd(xline,11);tn=subwrd(xline,13)
  xline=sublin(result,5);time1=subwrd(xline,6);timen=subwrd(xline,8)
* 
*  say t1' 'tn 
*  pull stop 
  x1=t1;xn=tn
  y1=0;yn=3.0
  y1=v1;yn=vn
* v1=0.0;vn = 3.0;'set vrange 'v1' 'vn''
*
  r=1.0
  np = (tn - t1)/r + 1
  np = math_int(np)

  ry=1.0; npy=(yn-y1)/ry+1; npy=math_int(npy)
  dy = (yt-yb)/(np-1)
  dx = (xr-xl)/(np-1)
**areas**
*  xarea=1 
   if(xarea=1); 
      latdw=15;latup=22.5;lonlf=-105;lonrg=-90;endif 
   if(xarea=2); 
      latdw=37.0;latup=43.0;lonlf=-102;lonrg=-90.0;endif 
   if(xarea=3); 
      latdw=20;latup=30;lonlf=-110;lonrg=-102.5;endif 
   if(xarea=4); 
      latdw=32.5;latup=42.5;lonlf=-97.5;lonrg=-87.5;endif 
*
* y-values / ylab 
* scale = 86400; 
* scale = 1; 
  it=1;while(it<=np)
      xtime = t1+(it-1)*r
      'set t 'xtime''
*     'd ave(ave(data,lat=5,lat=15),lon=-105,lon=-95)'
*     'd ave( ave(prec,lat='latdw',lat='latup'), lon='lonlf',lon='lonrg')/30'
*     'd ave( ave('scale'*data,lat='latdw',lat='latup'), lon='lonlf',lon='lonrg')/30'
*     'd ave( ave('scale'*data,lat='latdw',lat='latup'), lon='lonlf',lon='lonrg')/1.0'

*     'd ave( ave('scale'*prec,lat='latdw',lat='latup'), lon='lonlf',lon='lonrg')/1.0'
      'd ave( ave( 'scale' *prec,lat='latdw',lat='latup'), lon='lonlf',lon='lonrg')'

       say result 
*      get end of line
       len=0;ilen=1;while(len=0)
                      xlen=sublin(result,ilen);xxlen=subwrd(xlen,1);
*                     say xxlen 
                      if(xxlen='Result');len=1;endif 
                      ilen=ilen+1;
*                     pull stop 
                   endwhile
       len=ilen-1;
*      say len 
*      pull stop 
*      xline=sublin(result,17);
       xline=sublin(result,len);
       yvar.it=subwrd(xline,4);
       say yvar.it
*      pull stop 
      it=it+1
      endwhile


* x-labels
  it=1;while(it<=np)
      xtime = t1+(it-1)*r
      'set t 'xtime''
      'q dim'; xline=sublin(result,5);xwrd=subwrd(xline,6)
      dd=substr(xwrd,4,2);dd=dd+0
      mon=substr(xwrd,6,3);
*
      if(mon=JAN);mon=1;endif;if(mon=FEB);mon=2;endif;
      if(mon=MAR);mon=3;endif;if(mon=APR);mon=4;endif;
      if(mon=MAY);mon=5;endif;if(mon=JUN);mon=6;endif;
      if(mon=JUL);mon=7;endif;if(mon=AUG);mon=8;endif;
      if(mon=SEP);mon=9;endif;if(mon=OCT);mon=10;endif;
      if(mon=NOV);mon=11;endif;if(mon=DEC);mon=12;endif;
* 
      say mon
      if(mon=1);smon=Jan;endif;if(mon=2);smon=Feb;endif;
      if(mon=3);smon=Mar;endif;if(mon=4);smon=Apr;endif;
      if(mon=5);smon=May;endif;if(mon=6);smon=Jun;endif;
      if(mon=7);smon=Jul;endif;if(mon=8);smon=Aug;endif;
      if(mon=9);smon=Sep;endif;if(mon=10);smon=Oct;endif;
      if(mon=11);smon=Nov;endif;if(mon=12);smon=Dec;endif;
* 
*     xlab.it= mon%'/'%dd
      xlab.it= smon
*     say xlab.it 
*     pull stop 
      it=it+1
      endwhile

* 
* y-labels
* 
  it=1;while(it<=npy)
           xtmp = y1+(it-1)*ry 
           ylab.it= xtmp
           yy = yb + (xtmp-y1)*(yt-yb)/(yn-y1)
          'set strsiz 0.14';'set string 1 r 2'
*         'draw string 'xl-0.3'  'yy' 'ylab.it''
*         'draw line 'xl-0.2' 'yy' 'xl' 'yy''
           it=it+1
       endwhile
*---------------------------* 
  ip=1;while(ip<=np)
      nn=ip
      nnp=ip-1
      xx=x1+(nn-1)*dx
      xx=x1+(nn-2)*dx
* 
     'set line 1 1 1'
      yy = yb + (yvar.ip-y1)*(yt-yb)/(yn-y1)
*    'draw line 'xx' 'yb' 'xx' 'yy''
      dxrec=0.07
      dxrec=0.10
      dxrec=0.075
     'jaecol'
*    'set line 79'
     'set line 5'
*here 
**    'draw recf  'xx-dxrec' 'yb' 'xx+dxrec' 'yy''
     'draw recf   'xx-2*dxrec' 'yb' 'xx+0*dxrec' 'yy''
*    'draw rec   'xx-2*dxrec' 'yb' 'xx+0*dxrec' 'yy''
     'set ccolor 1'

*   x-label
      'set strsiz 0.15'
      'set string 1 c 3'
*     'draw string 'xx'  'yb-0.25' 'xlab.ip''
*--------------------------------------------* 
** lines
       if(math_mod(nnp,12) = 0 & nnp < 15)
         'set line 1 3 1'
**         'draw line 'xl' 'yy' 'xr' 'yy''
       endif
       if(math_mod(nnp,30) = 0 & nnp < 37)
         'set line 1 3 1'
**       'draw line 'xl' 'yy' 'xr' 'yy''
       endif
*
      ip=ip+1
      endwhile
return
****
