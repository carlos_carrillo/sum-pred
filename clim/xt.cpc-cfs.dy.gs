
function powerlfv(args)
  xl=subwrd(args,1)
  xr=subwrd(args,2)
  yb=subwrd(args,3)
  yt=subwrd(args,4)

  dx=subwrd(args,5)
  dy=subwrd(args,6)

  xcase = 1;*OLAM 
  xcase = 2;*CFS


  'set parea 'xl' 'xr' 'yb' 'yt''
*---------
* wet model 
  'set grads off'
  'set grid off'
  'set frame off'
* 'set xlab on'
  'set xlab off' 
  'set xlint 5'
  'set ylopts 1 2 0.09 0.11' 
  'set xlopts 1 4 0.08 0.11' 

*'open pr_CRCM_cgcm3.81.ctl'
 epsfile='pr_cpc_MW.93.eps'
 pngfile='pr_cpc_MW.93.png'

 precfile='pr-tsrs/prec.olam.MW.cpc.93.nc'

*'open pr_RCM3_cgcm3.81.ctl'
*'open /FoudreD0/data/prec/radars4/prad.24hr.ctl' 
*'open /work/hydro/carrillo/DATA/prec-temp/prec.livneh.1950-2013.dd.ctl' 
 'sdfopen /work/hydro/carrillo/DATA/prec/cpc_us/dy/precip.V1.0.1993.nc' 

 if(xcase=1)
   xtitle = 'P[NW:37`aO`nN-43`aO`nN] CPC, OLAM mm/day'
   epsfile='pr_cpc_olam.MW.93.eps'
   pngfile='pr_cpc_olam.MW.93.png'
 endif 
 if(xcase=2)
   xtitle = 'P[NW:37`aO`nN-43`aO`nN] CPC,  CFS mm/day'
   epsfile='pr_cpc_cfs.MW.93.eps'
   pngfile='pr_cpc_cfs.MW.93.png'
 endif 

*xtitle2 = '30`aO`nN 37.5`aO`nN'
 xtitle2 = ' '
 xtitle2 = '1993'

* dry model 
*open pr_RCM3_cgcm3.81.ctl';
*'open pr_RCM3_cgcm3.86.ctl';
*'set lat 34'
*'set lon 240 260'
*'set lon 245 260'

*'set lat 32'
*'set lon -118 -108'
 'set lat 40'
*'set lon -118 -108'
*'set lon -102  -90'
 'set lon 258  270'

  initime=00Z01Jun2007;endtime=00Z01Oct2007;
  initime=00Z01Jun1993;endtime=00Z01Aug1993;
  initime=00Z01Jul1993;endtime=00Z01Aug1993;
  initime=00Z01Jun1993;endtime=00Z01Aug1993;
  initime=00Z01Jun1993;endtime=00Z01Sep1993;

* initime=00Z01Mar1993;endtime=00Z01Nov1993;
  initime=00Z015Jun1993;endtime=00Z15Sep1993;
* initime=00Z015Jun1993;endtime=00Z01Sep1993;

 'set time 'initime'';'q dim';
  xline=sublin(result,5);itime=subwrd(xline,9);
 'set time 'endtime'';'q dim';
  xline=sublin(result,5);etime=subwrd(xline,9);
  say initime"("itime") "endtime"("etime") "

 'set t 'itime' 'etime''

 'set yflip on'
*'set ylevs 1  2  3  4  5  6  7' 
*'set ylab 1 | 2| 3| 4| 5| 6| 7' 
'set gxout shaded' 

*'yred2blue.gs'
*'set clevs 2 4 6 8 10 12 14 16'
*'set clevs 6 8 10 12 14 16 18 20'
*'set ccols  0 29 27 25 23 21 33 35 37'
*'set ccols  0 29 28 27 26 25 23 22 21'

'yred2blue.gs'
'set clevs 2 4 6 8 10 12 14 16'
'set ccols  0 71 72 73 74 75 76 77 78'

* to convert kg/(m2s) to mm/day 
*scale = 86400

* 'define xvar = ave(apcpsfc,lat=31.5,lat=37)'
* 'define xvar = ave(prec,lat=31.5,lat=37)'
* 'define xvar = ave(prec,lat=37.0,lat=43.0)'
  'define xvar = ave(precip,lat=37.0,lat=43.0)'
  'd xvar' 

*'d 'scale'*smth9(smth9(tloop(ave(ave(pr,lat=30,lat=37.5),t-4,t+4))))'
*'d smth9(smth9(tloop(ave(ave(pr,lat=24,lat=30.0),t-4,t+4))))'
*'d smth9(smth9(tloop(ave(ave(pr,lat=25,lat=30.0),t-1,t+1))))'

  'cbarv 'xr+0.14' 'yt-1.63' 0'
  'set strsiz 0.08 0.10'
* 'draw string 'xr-0.05' 'yt+0.12' [mm/day]'

* 'set gxout contour'
* 'set clevs 6 8 10 12 14 16 18 20'
* 'set clab off';'set cthick 1';'set cstyle 1';'set ccolor 1';
* 'd xvar' 

* x-y label
 'q gxinfo';*the first parea changes when the plot is defined
   xline=sublin(result,3);xr2=subwrd(xline,6);xl2=subwrd(xline,4)
   xline=sublin(result,4);yt2=subwrd(xline,6);yb2=subwrd(xline,4)

   xlab=on
*  if(xlab=on);xvlf=-120;xvrg=-100;xint=5.0;
   if(xlab=on);xvlf=258;xvrg=270;xint=2.0;
             call=myxlabh(xl2,xr2,yt2,yb2,xvlf,xvrg,xint);endif
*  ylab=on
*  if(ylab=on);yvb=-40;yvt=60.0;yint=20.0;
*           call=myylabh(xl2,xr2,yt2,yb2,yvb,yvt,yint);endif

*  pull stop


  'close 2'
  'close 1'
* 'set lat 40'
  'sdfopen 'precfile''
  'set time 'initime'';'q dim';
   xline=sublin(result,5);itime=subwrd(xline,9);
  'set time 'endtime'';'q dim';
   xline=sublin(result,5);etime=subwrd(xline,9);
   say initime"("itime") "endtime"("etime") "

  'set t 'itime' 'etime''
* 'set yflip on'
  'set xyrev on'
  'jaecol.gs'
  'set vrange 0 19'
* 'set cmark 0';'set clab off';'set cstyle 1';'set cthick 4';'set ccolor 8';
  'set cmark 0';'set clab off';'set cstyle 1';'set cthick 5';'set ccolor 1';
* 'set cmark 0';'set clab off';'set cstyle 1';'set cthick 4';'set ccolor 10';
* 'set cmark 0';'set clab off';'set cstyle 1';'set cthick 4';'set ccolor 38';
* 'set cmark 0';'set clab off';'set cstyle 1';'set cthick 4';'set ccolor 6';
  'd data'
*-----------------------
*  xxl = xl - 3.0; 
*  xxr = xr - 3.0; 
* 'set parea 'xxl' 'xxr' 'yb' 'yt''
* initime=00Z01Jun1993;
* endtime=00Z25Jul1993;
  'close 1'
  'sdfopen pr-tsrs/prec.olam.MW.R2F1.I9E1.93.nc'
  'set time 'initime'';'q dim';
   xline=sublin(result,5);itime=subwrd(xline,9);
  'set time 'endtime'';'q dim';
   xline=sublin(result,5);etime=subwrd(xline,9);
   say initime"("itime") "endtime"("etime") "
  'set t 'itime' 'etime''
  'set xyrev on'
  'jaecol.gs'
  'set cmark 0';'set clab off';'set cstyle 1';'set cthick 6';'set ccolor 2';
  if (xcase=1)  
     'd data'
  endif 
*-----------------------
  'close 1'
* 'sdfopen pr-tsrs/prec.olam.MW.R2F1.I8E1.93.nc'
* 'sdfopen pr-tsrs/prec.cfs.MW.1993061512.nc'
* 'sdfopen pr-tsrs/prec.cfs.MW.1993063012.nc'

*'sdfopen pr-tsrs/prec.cfs.MW.1993060512.nc'
*'sdfopen pr-tsrs/prec.cfs.MW.1993061012.nc'
*'sdfopen pr-tsrs/prec.cfs.MW.1993061512.nc'
*'sdfopen pr-tsrs/prec.cfs.MW.1993062012.nc'
*'sdfopen pr-tsrs/prec.cfs.MW.1993062512.nc'
*'sdfopen pr-tsrs/prec.cfs.MW.1993063012.nc';* the best case ?
*'sdfopen pr-tsrs/prec.cfs.MW.1993070512.nc';*also good 
 'sdfopen pr-tsrs/prec.cfs.MW.1993070512.2.nc';*also good 
*'sdfopen pr-tsrs/prec.cfs.MW.1993071012.nc'
*'sdfopen pr-tsrs/prec.cfs.MW.1993071512.nc'
*'sdfopen pr-tsrs/prec.cfs.MW.1993072012.nc';* good case 
*'sdfopen pr-tsrs/prec.cfs.MW.1993072512.nc'
*'sdfopen pr-tsrs/prec.cfs.MW.1993073012.nc'

* 'sdfopen pr-tsrs/prec.cfs.MW.1993071012.nc'
  
  'set time 'initime'';'q dim';
   xline=sublin(result,5);itime=subwrd(xline,9);
  'set time 'endtime'';'q dim';
   xline=sublin(result,5);etime=subwrd(xline,9);
   say initime"("itime") "endtime"("etime") "
  'set t 'itime' 'etime''
  'set xyrev on'
  'jaecol.gs'
* 'set cmark 0';'set clab off';'set cstyle 1';'set cthick 5';'set ccolor 1';
  'set cmark 0';'set clab off';'set cstyle 3';'set cthick 6';'set ccolor 2';
   dxx=1.5
   dxx=1.75
   dxx=2.50
*  dxx=0.0
   if(xcase=2) 
  'd data+0*'dxx''
   endif 
*-----------------------
  'close 1'
  'sdfopen pr-tsrs/prec.olam.MW.R2F1.I7E1.93.nc'
  'set time 'initime'';'q dim';
   xline=sublin(result,5);itime=subwrd(xline,9);
  'set time 'endtime'';'q dim';
   xline=sublin(result,5);etime=subwrd(xline,9);
   say initime"("itime") "endtime"("etime") "
  'set t 'itime' 'etime''
  'set xyrev on'
  'jaecol.gs'
* 'set cmark 0';'set clab off';'set cstyle 1';'set cthick 5';'set ccolor 1';
  'set cmark 0';'set clab off';'set cstyle 1';'set cthick 6';'set ccolor 4';
  if(xcase=1) 
   'd data+3*'dxx''
  endif 
*-----------------------
  'close 1'
* 'sdfopen pr-tsrs/prec.olam.MW.R2F1.I6E1.93.nc'
* 'sdfopen pr-tsrs/prec.cfs.MW.1993070512.nc'
* 'sdfopen pr-tsrs/prec.cfs.MW.1993071012.nc'
  'sdfopen pr-tsrs/prec.cfs.MW.1993071012.2.nc'
*
  'set time 'initime'';'q dim';
   xline=sublin(result,5);itime=subwrd(xline,9);
  'set time 'endtime'';'q dim';
   xline=sublin(result,5);etime=subwrd(xline,9);
   say initime"("itime") "endtime"("etime") "
  'set t 'itime' 'etime''
  'set xyrev on'
  'jaecol.gs'
* 'set cmark 0';'set clab off';'set cstyle 1';'set cthick 5';'set ccolor 1';
  'set cmark 0';'set clab off';'set cstyle 3';'set cthick 6';'set ccolor 4';
* 'd data+15'
  if(xcase=2)
  'd data+3*'dxx''
  endif 
*-----------------------
  'close 1'
  'sdfopen pr-tsrs/prec.olam.MW.R2F1.I5E1.93.nc'
  'set time 'initime'';'q dim';
   xline=sublin(result,5);itime=subwrd(xline,9);
  'set time 'endtime'';'q dim';
   xline=sublin(result,5);etime=subwrd(xline,9);
   say initime"("itime") "endtime"("etime") "
  'set t 'itime' 'etime''
  'set xyrev on'
  'jaecol.gs'
* 'set cmark 0';'set clab off';'set cstyle 1';'set cthick 5';'set ccolor 1';
  'set cmark 0';'set clab off';'set cstyle 1';'set cthick 6';'set ccolor 12';
* 'd data+20'
* 'd data+4*'dxx''
*-----------------------
  'close 1'
  'sdfopen pr-tsrs/prec.olam.MW.R2F1.I4E1.93.nc'
  'set time 'initime'';'q dim';
   xline=sublin(result,5);itime=subwrd(xline,9);
  'set time 'endtime'';'q dim';
   xline=sublin(result,5);etime=subwrd(xline,9);
   say initime"("itime") "endtime"("etime") "
  'set t 'itime' 'etime''
  'set xyrev on'
  'jaecol.gs'
* 'set cmark 0';'set clab off';'set cstyle 1';'set cthick 5';'set ccolor 1';
  'set cmark 0';'set clab off';'set cstyle 2';'set cthick 5';'set ccolor 12';
* 'd data'
* 'd data+5*'dxx''
*-----------------------
  'close 1'
  'sdfopen pr-tsrs/prec.olam.MW.R2F1.I3E1.93.nc'
  'set time 'initime'';'q dim';
   xline=sublin(result,5);itime=subwrd(xline,9);
  'set time 'endtime'';'q dim';
   xline=sublin(result,5);etime=subwrd(xline,9);
   say initime"("itime") "endtime"("etime") "
  'set t 'itime' 'etime''
  'set xyrev on'
  'jaecol.gs'
  'set cmark 0';'set clab off';'set cstyle 1';'set cthick 6';'set ccolor 3';
* 'd data'
* 'd data+6*'dxx''
*-----------------------
  'close 1'
  'sdfopen pr-tsrs/prec.olam.MW.R2F1.I2E1.93.nc'
  'set time 'initime'';'q dim';
   xline=sublin(result,5);itime=subwrd(xline,9);
  'set time 'endtime'';'q dim';
   xline=sublin(result,5);etime=subwrd(xline,9);
   say initime"("itime") "endtime"("etime") "
  'set t 'itime' 'etime''
  'set xyrev on'
  'jaecol.gs'
* 'set cmark 0';'set clab off';'set cstyle 1';'set cthick 5';'set ccolor 1';
  'set cmark 0';'set clab off';'set cstyle 2';'set cthick 5';'set ccolor 3';
* 'd data'
* 'd data+7*'dxx''
*-----------------------
  'close 1'
  'sdfopen pr-tsrs/prec.olam.MW.R2F1.I1E1.93.nc'
  'set time 'initime'';'q dim';
   xline=sublin(result,5);itime=subwrd(xline,9);
  'set time 'endtime'';'q dim';
   xline=sublin(result,5);etime=subwrd(xline,9);
   say initime"("itime") "endtime"("etime") "
  'set t 'itime' 'etime''
  'set xyrev on'
  'jaecol.gs'
  'set cmark 0';'set clab off';'set cstyle 1';'set cthick 5';'set ccolor 1';
* 'd data'
* 'd data+8*'dxx''
*-----------------------




*-----------------------

   'set string 1 l 4'
   'set strsiz 0.10 0.14'
   'draw string 'xl+0.02' 'yt+0.12' 'xtitle''

   'set string 1 c 3'
   'set strsiz 0.10 0.15'
   'draw string 'xl+0.22' 'yb+0.12' 'xtitle2''

*----
   'myframe 1 1 2'
*--------------

   'q dim'
    xline=sublin(result,5);t1=subwrd(xline,11);tn=subwrd(xline,13)
    xline=sublin(result,5);time1=subwrd(xline,6);timen=subwrd(xline,8)

    say result 
    y1=t1;yn=tn

    r=1.0
    np = (tn - t1)/r + 1
    np = math_int(np)

    ry=1.0; npy=(yn-y1)/ry+1; npy=math_int(npy)
    dy = (yt-yb)/(np-1)
*   dx = (xr-xl)/(np-1)
*-------------------
*   plot CAPE-PW days 

    smo.1 ='Jan';smo.2='Feb';smo.3='Mar';smo.4 ='Apr';smo.5='May';smo.6='Jun';
    smo.7 ='Jul';smo.8='Aug';smo.9='Sep';smo.10 ='Oct';smo.11='Nov';smo.12='Dec';

file_in = "pw_cape_dys.2007.txt"
status = 0
while (status = 0)
record = read(file_in)
status = sublin(record,1)
if ( status  = 0)
    infolin  = sublin(record,2)
    stanum  = subwrd(infolin,3)
*
    fyr   = subwrd(infolin,1)
    fmo   = subwrd(infolin,2)
    fdy   = subwrd(infolin,3)
    fcape = subwrd(infolin,4)
    fpw   = subwrd(infolin,5)
    say  fyr"  "fmo"  "fdy

        initime=12Z%fdy%smo.fmo%fyr;
        say initime
       'set time 'initime'';'q dim';
        xline=sublin(result,5);itime=subwrd(xline,9);

        say initime":("itime")"

*       ylab.1=2000 
        ylab.1=itime
*   yy = yb + (yvar.ip-y1)*(yt-yb)/(yn-y1)
*   yy = yb + (ylab.1-y1)*(yt-yb)/(yn-y1)
    yy = yt - (ylab.1-y1)*(yt-yb)/(yn-y1)
 
    'set strsiz 0.10 0.12'
    'set string 1 l 3'
*   'draw string 'xl'  'yy-0.09' 'ylab.1''
*  
    'set line 1 1 2'
    'draw line 'xl' 'yy' 'xr' 'yy''
endif
endwhile 
*-------------------



**  'close 1'
*----
'enable print x.gm'
'print'
'disable print'
'!gxeps -c -i x.gm -o x.eps'
'!convert -density 200  x.eps 'pngfile''
'!mv x.eps 'epsfile''
*------------------
*------------------
*------------------
*------------------

function myxlabh(xl,xr,yt,yb,xvlf,xvrg,xint)
* xvlf=-120;xvrg=-20;xint=20 
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,2);x1=subwrd(xline,6);xn=subwrd(xline,8)
*
  nptos=(xvrg-xvlf)/xint + 1;
* say x1' 'xn 
*
  it=1;while(it<=nptos)
          xx.it = xvlf + (it-1)*xint 
*          say xx.it 
       it=it+1
       endwhile
* lab 
 it=1;while(it<=nptos)
        xreal=xx.it
        say xreal
        xabs=math_abs(xx.it)
        if (xreal<0);
           if (xreal>-180);
               xxlb.it=xabs%"`aO`nW";
           else;  xtmp = 360-xabs
               xxlb.it=xtmp%"`aO`nE";
           endif
        else
           if (xreal> 180);
               xtmp = 360-xabs
               xxlb.it=xtmp%"`aO`nW";
               if (xtmp = 0);
                  xxlb.it=xtmp%"`aO`n";
               endif
           else;
              xxlb.it=xabs%"`aO`nE";
           endif

        endif
       if(xreal=0|xreal=180);xxlb.it=xabs%"`aO`n";endif
*        xxlb.it=math_abs(xx.it) 
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say xxlb.it 
        it=it+1
       endwhile
*

*
  ip=1;while(ip<=nptos)
         xx = xx.ip
         xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         'set line 1 1 1'
         'draw line 'xs' 'yb' 'xs' 'yb-0.075''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
         y = yt
*        'draw line 'xs' 'yb' 'xs' 'yt''
         'set strsiz 0.080 0.10'
         'set string 1 c 3'
         'draw string 'xs'  'yb-0.175' 'xxlb.ip''
*         say xs' 'xx 
      ip=ip+1
      endwhile
return
*----------


*------------------------
function myylabh(xl,xr,yt,yb,yvb,yvt,yint)
* yvb=-40;yvt=40;yint=10;
*
* to log scale set splot=log
* to linear scale set splot=anything
* Jan 21, 2009
*
  splot=log
  splot=nolog
*
  'q dim'
  xline=sublin(result,3);y1=subwrd(xline,6);yn=subwrd(xline,8)
  nptos=(yvt-yvb)/yint + 1;
*
  it=1;while(it<=nptos)
          yy.it = yvb  + (it-1)*yint 
*          say yy.it 
       it=it+1
       endwhile
* lab 

  it=1;while(it<=nptos)
        yreal=yy.it
        yabs=math_abs(yy.it)
        if(yreal=0);yylb.it="EQ";endif
        if(yreal<0);yylb.it=yabs%"`aO`nS";endif
        if(yreal>0);yylb.it=yabs%"`aO`nN";endif
*        if(splot=log);tmp=math_log(xx.it);xx.it=tmp;endif
*        say yylb.it 
        it=it+1
       endwhile
*
  ip=1;while(ip<=nptos)
         yy = yy.ip
*        xs = xl+(xx-x1)*(xr-xl)/(xn-x1)
         ys = yb+(yy-y1)*(yt-yb)/(yn-y1)
         'set line 1 1 1'
         'draw line 'xl-0.075' 'ys' 'xl' 'ys''
         'set line 1 3 1'
*         'draw line 'xs' 'yb' 'xs' 'yt*2.45''
*         'draw line 'xs' 'yb' 'xs' 'y''
*        'draw line 'xr' 'ys' 'xl' 'ys''
*        'set strsiz 0.12'
         'set strsiz 0.09 0.11'
         'set string 1 r 3'
*         'draw string 'xs'  'yb-0.2' 'xxlb.ip''
         'draw string 'xl-0.10'  'ys' 'yylb.ip''
      ip=ip+1
      endwhile
return
*----------------------------------------------



