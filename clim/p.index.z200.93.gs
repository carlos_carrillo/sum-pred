 
'reinit'

*CFS = 1993060512
*CFS = 1993092812

*infile = '/work/hydro/carrillo/2015/backup_tusker/work/MODELS/OLAM/runs/'OLAM'/NC/pr_olam.ctl'
*outfile = 'pr-tsrs/prec.olam.MW.'OLAM'.93.txt'

*infile = '/work/hydro/carrillo/DATA/CFS/9mo/prate.'CFS'.ctl'
infile = '/home/hydro/carrillo/2015/sum-pred/eof/reco-gph.mod35.nc'

outfile = 'pr-tsrs/z200.narr.MW.1993.txt'
outfile = 'pr-tsrs/z200.narr.MW.1993.v2.txt'


 'sdfopen 'infile''
*'open 'infile''
*------WRITE FILE--------
*  outfile = 'sst.nino3.1854-2013.na.AA.txt'
*  outfile = 'prec.SE-MX.1854-2013.na.AA.txt'
*--SE-MX (MidWest) 
* 'define xvar3 = ave(ave(data,lat=40.0,lat=50.0),lon=260,lon=280)'
   ltdw = 40.0; ltup = 50.0;
   lnlf = 260.; lnrg = 280.; 
*----ENP (eastern North Pacific) 
*  outfile = 'sst.ENP.1854-2013.na.AA.txt'
*  ltdw = 35.0; ltup = 50.0;
*  lnlf = -150.0; lnrg = -125.0; 
*----CNP (central North Pacific) 
*  outfile = 'sst.CNP.1854-2013.na.AA.txt'
*  ltdw = 26.0; ltup = 36.0;
*  lnlf = 177.0; lnrg = 196.0; 

*  yrini=1854;
*  tn = 160; 
*  yrini=1870;
*  tn = 141; 
*  yrini=1950;
*  tn = 21; 
* 'q file';xline = sublin(result,5);tn=subwrd(xline,12);
  '!rm -rf 'outfile''
*-----
  'set t 1';'q dim';xline=sublin(result,5);xtime=subwrd(xline,6);initime=00Z%substr(xtime,4,12);
  initime=00Z01Jun1993;
  endtime=00Z01Sep1993;
 'set time 'initime'';'q dim';xline=sublin(result,5);itime=subwrd(xline,9);
 'set time 'endtime'';'q dim';xline=sublin(result,5);etime=subwrd(xline,9);
 
  say initime"("itime") "endtime"("etime") "
*
   it=itime;
*  it=1;
*  it=2;
   tn=etime;
   scale = 86400; 
   scale = 1; 
   while (it <= tn) 
        et=it+3;
        'set t 'it''
        'q dims';xline = sublin(result,5);xtime=subwrd(xline,6);
*
*       'd ave( ave(ave('scale'*PRATEsfc,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg'), t='it',t='et')'

        'd  ave(ave('scale'*data,lat='ltdw',lat='ltup'),lon='lnlf',lon='lnrg')'

*        say result
         flag = 'off';count=1
         while (flag != 'Result')
             xline = sublin(result,count);xvar=subwrd(xline,4);flag = subwrd(xline,1);
             count=count+1; 
         endwhile
*        pull stop 
*        xline = sublin(result,14);xvar=subwrd(xline,4) 
*        yr = yrini+it-1 
         say xtime'  'xvar
         tofile = xtime'  'xvar
         tofile = it'  'xvar
*        tofile = yr'  'xvar
*        pull stop 
         call = write(outfile,tofile) 
       it=it+1 
*      it=it+4 
   endwhile 
* 'set t 1 'tn''
*--------------
